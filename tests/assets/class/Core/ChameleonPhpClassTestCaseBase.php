<?php
/**
 * Aids Class Manager testing procedures, provides basic tools and settings
 *
 */
namespace Gaad\ChameleonTesting\Core;

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Gaad\Chameleon\Service\Schema\PhpClassWriter;

class ChameleonPhpClassTestCaseBase extends AbstractChameleonTestCaseBase
{
	private PhpClassWriter $ClassWriter;


	/**
	 * Class code reading operations class
	 *
	 * @var PhpClassManager|object|null
	 */
	public PhpClassManager $ClassManager;

	/**
	 * Keeps the classes templates under indexes corresponding with test methods names
	 * If some testing procedures will be more efficient on smaller testing class You can declare one here
	 * and load it after the test starts.
	 *
	 * @var array|string[]
	 */
	public array $testClassPath = [
		'setUp' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/TestClasses.php',
		'test_loadingTestClassForSpecificTesting' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/TestClasses.php',
		'test_findNearestSubstring' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Other/ConstantsAndProperties.php',
		'test_findNearestSemiColon' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Other/ConstantsAndProperties.php',
		'test_findNearestDocBlockPosition' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Other/ConstantsAndProperties.php',
		'test_isLineInsideDocBlockDocBlock' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Other/ConstantsAndProperties.php',
		'test_findElementsAfterLine' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/TestClasses.php',
		'test_findAllLinesWithSubstring' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/TestClasses.php',
		'test_findUseSection' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/TestClasses.php',
		'test_findClassUseSection' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/TestClasses.php',
		'test_findClassHeader' => __CEBOARD_CORE_DIR__ . '/tests/assets/class/Some/Name/ClassHeaderTesting.php',
	];

	public function setUp()
	{
		$this->kernel = $GLOBALS['kernel'];
		$this->ClassManager = $this->kernel->getService('ClassManager');
		$this->ClassManager->loadFromFile($this->getCorrespondingTestClassPath(__FUNCTION__));

		$this->ClassWriter = $this->kernel->getService('PhpClassWriter');
		$this->ClassWriter->loadFromFile($this->getCorrespondingTestClassPath(__FUNCTION__));
	}

	/**
	 * If applicable checks and returns the testing class accordingly to test method name
	 * @return void
	 */
	function getCorrespondingTestClassPath(string $testMethodName): string
	{
		$testClassPath = $this->getTestClassPath();
		$found = !empty( $testClassPath[$testMethodName]);

		return $found ? $testClassPath[$testMethodName] : $testClassPath['setUp'];
	}

	public function getClassManager()
	{
		return $this->ClassManager;
	}

	public function getTestClassPath(): array
	{
		return $this->testClassPath;
	}

	/**
	 * @return PhpClassWriter
	 */
	public function getClassWriter(): PhpClassWriter
	{
		return $this->ClassWriter;
	}

	/**
	 * Executes the testing class switch in class manager
	 *
	 * @param string $functionName
	 * @return string
	 */
	protected function loadCorrespondingTestClass(string $functionName): string
	{
		$cm = $this->getClassManager();
		$path = $this->getCorrespondingTestClassPath($functionName);
		try {
			$cm->loadFromFile($path);
		} catch (ClassException $e) {
			die(); // @TODO Handle it in more civilized way
		}

		return $path;
	}

}
