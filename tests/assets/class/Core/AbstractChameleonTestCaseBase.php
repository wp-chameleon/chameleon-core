<?php

namespace Gaad\ChameleonTesting\Core;

use Gaad\Chameleon\AppKernel;

abstract class AbstractChameleonTestCaseBase extends \WP_UnitTestCase
{

	public static string $env = 'dev';

	public ?AppKernel $kernel;

	/**
	 * Because we are doing more of a hybrid than strict unit testing the dependency container is available.
	 * Most convenient way is to have it accessible globally
	 *
	 * @return void
	 */
	public static function setUpBeforeClass(): void
	{
		$GLOBALS['kernel'] = $GLOBALS['kernel'] instanceof AppKernel ? $GLOBALS['kernel'] : new AppKernel('dev');
	}

	/**
	 * @return string
	 */
	public static function getEnv(): string
	{
		return self::$env;
	}

	/**
	 * @param string $env
	 */
	public static function setEnv(string $env): void
	{
		self::$env = $env;
	}

	/**
	 * @return AppKernel|null
	 */
	public function getKernel(): ?AppKernel
	{
		return $this->kernel;
	}

	/**
	 * @param AppKernel|null $kernel
	 */
	public function setKernel(?AppKernel $kernel): void
	{
		$this->kernel = $kernel;
	}



}
