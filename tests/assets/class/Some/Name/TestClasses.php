<?php

namespace Gaad\ChameleonTesting\Some\Name;

use Gaad\Chameleon\Annotation\ChController;
use Gaad\Chameleon\Annotation\WPHookPriority;
use Gaad\ChameleonTesting\Some\Name\Parent\TestClassesParent;
use Gaad\ChameleonTesting\Some\Traits\AwesomeTrait;
use Gaad\ChameleonTesting\Some\Traits\ShittyTrait;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @ChController()
 */
class TestClasses
extends TestClassesParent
{

	use ShittyTrait;
	use AwesomeTrait;

	const VERSION = '12';


	/** @var string */
	protected const SUBVERSION = [
		'whiskey',
		'bourbon',
		'tequila'
	];

	/** @var string */
	private string $a = 'abc';
	/**
	 * @var array|string[]
	 */
	public array $b = [
		'very',
		'important',
		'info'
	];

	/**
	 * @Route("/some", name="some_place")
	 * @WPHookPriority(100)
	 *
	 * @return void
	 */
	function someAction()
	{

	}

	/**
	 * @Route("/some2", name="some_place")
	 * @WPHookPriority(100)
	 *
	 * @return void
	 */
	function some2Action()
	{

	}

}
