<?php
/**
 * Lorem ipsum dolor sit amet, consectet.
 *
 * version 3.1
 */
namespace Gaad\ChameleonTesting\Some\Name;

use Gaad\Chameleon\Annotation\ChController;
use Gaad\ChameleonTesting\Some\FakeInter\Interface1;
use Gaad\ChameleonTesting\Some\FakeInter\Interface2;
use Gaad\ChameleonTesting\Some\Name\Parent\TestClassesParent;

/**
 * @ChController()
 */
class ClassHeaderTesting
extends TestClassesParent
implements Interface1, Interface2
{
}
