<?php
/**
 * Testing playground for constants, properties
 *
 * tests:
 * - finding constant line
 * - finding constant DocBlock line
 * - finding multiline constants declarations
 * - checking if setter/getter exists
 *
 *
 * */

namespace Gaad\ChameleonTesting\Some\Other;

use Gaad\Chameleon\Annotation\ChController;

/**
 * @ChController()
 */
class ConstantsAndProperties
{
	/**
	 * this is DocBlock for constant
	 *
	 * it tells you what the constant is for
	 */
	const VERSION = '12';

	private $anyTimeDinner = '';

	/**
	 * I'm a cool description
	 *
	 * @var string[]
	 */
	private $maybeAnotherRedundantPropertyHere = [
		'some' => 'data',
		'some2' => 'data2',
		'some3' => 'data3'
	];

	const VERSION_OF_NOTHING_IMPORTANT = 12.4;


	/** @var string */
	protected const SUBVERSION = [
		'another',
		'story',
		'to',
		'tell', // I like commas at the end of arrays, please leave it here
	];

	/** @var string */
	private string $a = 'abc';
	/**
	 * @var array|string[]
	 */
	public array $b = [
		'no', 'comma', 'array'
	];

	/**
	 * @return string
	 */
	public function getAnyTimeDinner(): string
	{
		return $this->anyTimeDinner;
	}

	/**
	 * @param string $anyTimeDinner
	 */
	public function setAnyTimeDinner(string $anyTimeDinner): void
	{
		$this->anyTimeDinner = $anyTimeDinner;
	}

	/**
	 * @return string[]
	 */
	public function getMaybeAnotherRedundantPropertyHere(): array
	{
		return $this->maybeAnotherRedundantPropertyHere;
	}

	/**
	 * @param string[] $maybeAnotherRedundantPropertyHere
	 */
	public function setMaybeAnotherRedundantPropertyHere(array $maybeAnotherRedundantPropertyHere): void
	{
		$this->maybeAnotherRedundantPropertyHere = $maybeAnotherRedundantPropertyHere;
	}

	/**
	 * @return string
	 */
	public function getA(): string
	{
		return $this->a;
	}

	/**
	 * @param string $a
	 */
	public function setA(string $a): void
	{
		$this->a = $a;
	}

	/** Somebody commented me out and left it like this, probably for some testing purposes
	 * @return array|string[]

	public function getB(): array
	{
		return $this->b;
	}*/

	/**
	 * @param array|string[] $b
	 */
	public function setB(array $b): void
	{
		$this->b = $b;
	}


}
