<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\Annotation\ChController;
use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Exception\ClassPartException;
use Gaad\Chameleon\Schema\Php\DocBlockClassPart;
use Gaad\Chameleon\Schema\Php\MethodArgumentClassPart;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @property AppKernel $app
 */
class MethodArgumentClassPartTest extends ChameleonPhpClassTestCaseBase
{

	public MethodArgumentClassPart $argumentClassPart1;
	public MethodArgumentClassPart $invalidArgumentClassPart1;
	public MethodArgumentClassPart $argumentClassPartBasic;

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function setUp()
	{
		parent::setUp();
		$cm = $this->getClassManager();

		$this->argumentClassPartBasic = new MethodArgumentClassPart($cm, [
			'methodName' => 'kickAssMethod',
			'name' => 'abc'
		]);

		$this->argumentClassPartDocBlock = new MethodArgumentClassPart($cm, [
			'methodName' => 'kickAssMethod',
			'name' => 'abc'
		]);

		$this->argumentClassPart1 = new MethodArgumentClassPart($cm, [
			'methodName' => 'kickAssMethod',
			'name' => 'abc',
			'argumentType' => 'string',
			'defaultValue' => null,
			'isNull' => false,
			'canBeNull' => false,
		]);
		$this->invalidArgumentClassPart1 = new MethodArgumentClassPart($cm, [
			'name' => 'abc',
			'argumentType' => 'string',
			'defaultValue' => null,
			'isNull' => false,
			'canBeNull' => false,
		]);
	}


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_classMethodClassPartExists()
	{
		$classPart = new MethodArgumentClassPart($this->getClassManager());
		$this->assertInstanceOf(MethodArgumentClassPart::class, $classPart);
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_validate()
	{
		$cm = $this->getClassManager();
		$this->assertTrue(method_exists($this->argumentClassPart1, 'validate'));

		$this->assertTrue($this->argumentClassPart1->validate());

		$this->assertFalse($this->invalidArgumentClassPart1->validate());

		$MinimumSetupArgumentClassPart = new MethodArgumentClassPart($cm, [
			'name' => 'abc',
			'methodName' => 'kickAssMethod'
		]);
		$this->assertTrue($MinimumSetupArgumentClassPart->validate());

	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_setArgumentType()
	{
		$cm = $this->getClassManager();
		$this->assertTrue(method_exists($this->argumentClassPart1, 'setArgumentType'));

		//@Todo do it!

	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_generateCodeNoDocBlock()
	{
		$cm = $this->getClassManager();
		$this->assertTrue(method_exists($this->argumentClassPart1, 'generateCode'));

		$expectedCode = '$abc';
		$generatedCode = implode("", $this->argumentClassPartBasic->generateCode());
		$this->assertEquals($expectedCode, $generatedCode);

		$expectedCode = 'array $abc';
		$this->argumentClassPart1->setArgumentType('array');
		$generatedCode = implode("", $this->argumentClassPart1->generateCode());
		$this->assertEquals($expectedCode, $generatedCode);

		$expectedCode = '?array $abc';
		$this->argumentClassPart1->setCanBeNull(true);
		$generatedCode = implode("", $this->argumentClassPart1->generateCode());
		$this->assertEquals($expectedCode, $generatedCode);

		$expectedCode = '?array $abc = null';
		$this->argumentClassPart1->setIsNull(true);
		$generatedCode = implode("", $this->argumentClassPart1->generateCode());
		$this->assertEquals($expectedCode, $generatedCode);

		$expectedCode = '?PropertyClassPart $abc = null';
		$this->argumentClassPart1->setArgumentType('Gaad\Chameleon\Schema\Php\PropertyClassPart');
		$generatedCode = implode("", $this->argumentClassPart1->generateCode());
		$this->assertEquals($expectedCode, $generatedCode);

		//setting non-existing custom type
		try {
			$this->argumentClassPart1->setArgumentType('Gaad');
		} catch (ClassPartException $e) {
		}
		$this->assertInstanceOf(ClassPartException::class, $e);
	}

}
