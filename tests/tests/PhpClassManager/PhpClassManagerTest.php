<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Schema\Php\ConstantClassPart;
use Gaad\Chameleon\Schema\Php\DocBlockClassPart;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;


/**
 * @property AppKernel $app
 */
class PhpClassManagerTest extends ChameleonPhpClassTestCaseBase
{

	public static array $expectedClassParts = [
		'namespace',
		'useSection',
		'classHeader',
		'classUseSection',
		'constSection',
		'propertiesSection',
		'methodSection'
	];


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_class_manager_exists()
	{
		$this->assertInstanceOf(PhpClassManager::class, $this->getClassManager());
	}

	public function test_loadingTestClassForSpecificTesting()
	{
		$cm = $this->getClassManager();
		$path = $this->loadCorrespondingTestClass(__FUNCTION__);

		$this->assertTrue($cm->isInitialized()); //loading went fine
		$this->assertEquals($cm->getPath(), $path);
	}

	public function test_findNearestSubstring()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);

		$cm = $this->getClassManager();
		$this->assertTrue(method_exists($cm, 'findNearestSubstring'));
		//going down
		$this->assertEquals(32, $cm->findNearestSubstring('/**', 25));
		$this->assertEquals(79, $cm->findNearestSubstring('/**', 77));
		$this->assertEquals(79, $cm->findNearestSubstring('/**', 79));
		$this->assertEquals(80, $cm->findNearestSubstring('@return string[]', 77));

		//going up
		$this->assertEquals(33, $cm->findNearestSubstring('	 * I\'m a cool description', 77, false));
		$this->assertEquals(1, $cm->findNearestSubstring('<?php', 77, false));
		$this->assertEquals(105, $cm->findNearestSubstring('*/', 115, false));
		$this->assertEquals(105, $cm->findNearestSubstring('*/', 105, false));
	}

	public function test_findNearestSemiColon()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findNearestSemiColon'));

		//going down
		$this->assertEquals(null, $cm->findNearestSemiColon(125));
		$this->assertEquals(43, $cm->findNearestSemiColon(43));
		$this->assertEquals(61, $cm->findNearestSemiColon(57));

		//going up
		$this->assertEquals(43, $cm->findNearestSemiColon(43, false));
		$this->assertEquals(55, $cm->findNearestSemiColon(57, false));
		$this->assertEquals(124, $cm->findNearestSemiColon(124, false));
	}

	public function test_isLineInsideDocBlockDocBlock()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'isLineInsideDocBlockDocBlock'));
		$this->assertFalse($cm->isLineInsideDocBlockDocBlock(38));
		$this->assertTrue($cm->isLineInsideDocBlockDocBlock(25));
		$this->assertTrue($cm->isLineInsideDocBlockDocBlock(46)); // single line
		$this->assertTrue($cm->isLineInsideDocBlockDocBlock(32));
		$this->assertTrue($cm->isLineInsideDocBlockDocBlock(65));
	}

	public function test_findNearestDocBlockPosition()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findNearestDocBlockPosition'));
		$this->assertEqualsCanonicalizing(['begin' => 2, 'end' => 12], $cm->findNearestDocBlockPosition(10));
		$this->assertEqualsCanonicalizing(['begin' => 103, 'end' => 105], $cm->findNearestDocBlockPosition(100));
		$this->assertEqualsCanonicalizing(['begin' => 56, 'end' => 58], $cm->findNearestDocBlockPosition(56));
	}

	public function test_findAllLinesWithSubstring()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findAllLinesWithSubstring'));
		$this->assertEqualsCanonicalizing([22, 26], $cm->findAllLinesWithSubstring('const', 12));
		$this->assertEqualsCanonicalizing([44, 55], $cm->findAllLinesWithSubstring('@Route', 12));
		$this->assertEqualsCanonicalizing([44], $cm->findAllLinesWithSubstring('@Route', 54, false));

	}

	public function test_findElementsAfterLinePattern()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findElementsAfterLinePattern'));
		$this->assertEqualsCanonicalizing([19, 20], $cm->findElementsAfterLinePattern('use', 'class'));
		$this->assertEqualsCanonicalizing([5, 6, 7, 8, 9, 10], $cm->findElementsAfterLinePattern('use', 'class', false));
		$this->assertEqualsCanonicalizing([3, 44, 55], $cm->findElementsAfterLinePattern('name', '<?php'));

	}

	public function test_findUseSection()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$useArray = [
			"use Gaad\\Chameleon\\Annotation\\ChController;\n",
			"use Gaad\\Chameleon\\Annotation\\WPHookPriority;\n",
			"use Gaad\\ChameleonTesting\\Some\\Name\\Parent\\TestClassesParent;\n",
			"use Gaad\\ChameleonTesting\\Some\\Traits\\AwesomeTrait;\n",
			"use Gaad\\ChameleonTesting\\Some\\Traits\\ShittyTrait;\n",
			"use Symfony\\Component\\Routing\\Annotation\\Route;\n"
		];

		$this->assertEqualsCanonicalizing($useArray, $cm->getUse());
	}

	public function test_findClassUseSection()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$useArray = [
			"\tuse AwesomeTrait;\n",
			"\tuse ShittyTrait;\n"
		];

		$this->assertEqualsCanonicalizing($useArray, $cm->getClassUse());
	}

	public function test_getClassSlice()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'getClassSlice'));

		$classSlice = $cm->getClassSlice(26, 49);
		$this->assertEquals("	protected const SUBVERSION = [\n", $classSlice[0]);
		$this->assertEquals("	function someAction()\n", $classSlice[array_key_last($classSlice)]);

		$classSlice = $cm->getClassSlice(33, 33);
		$this->assertEquals("	private string \$a = 'abc';\n", $classSlice[0]);
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassPartException
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_getConstantDetails()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'getConstantDetails'));
		$this->assertEquals(null, $cm->getConstantDetails('nonExisting'));

		$this->assertInstanceOf(ConstantClassPart::class, $cm->getConstantDetails('SUBVERSION'));

		/** @var ConstantClassPart $claasPart */
		$claasPart = $cm->getConstantDetails('SUBVERSION');
		$this->assertInstanceOf(DocBlockClassPart::class, $claasPart->getDocBlock());

		$this->assertEquals(26, $claasPart->getBegin());
		$this->assertEquals(30, $claasPart->getEnd());

		$constantCode = [
			"\t/** @var string */\n",
			"\tprotected const SUBVERSION = [\n",
			"\t\t'whiskey',\n",
			"\t\t'bourbon',\n",
			"\t\t'tequila'\n",
			"\t];\n"
		];
		$code = $claasPart->findCode();
		$this->assertEqualsCanonicalizing($constantCode, $code);
	}

	public function test_findClassHeader()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findClassHeader'));
		$headerCode = [
			"/**\n",
			" * @ChController()\n",
			" */\n",
			"class ClassHeaderTesting\n",
			"extends TestClassesParent\n",
			"implements Interface1, Interface2\n"
		];

		$code = $cm->getClassHeader()->getCode();
		$this->assertEqualsCanonicalizing($headerCode, $code);
	}

	public function test_findMethods()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findMethods'));
		$this->assertCount(2, $cm->getMethods());

	}

	public function test_findProperties()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findProperties'));
		$this->assertCount(2, $cm->getProperties());
	}


	public function test_fetchClassParts()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'fetchClassParts'));
		$classParts = $cm->fetchClassParts();
		$this->assertNotEmpty($classParts);

		$this->assertEqualsCanonicalizing(self::$expectedClassParts, array_keys($classParts));
	}


	public function test_isPartOfFile()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'isPartOfFile'));
		$this->assertTrue($cm->isPartOfFile('const VERSION'));
		$this->assertFalse($cm->isPartOfFile('const NOT_HERE'));


	}

	public function test_findConstants()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'findConstants'));
		$this->assertCount(2, $cm->getConstants());
	}

	public function test_getConstant()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'getConstant'));
		$this->assertInstanceOf (ConstantClassPart::class, $cm->getConstant('VERSION'));
		$this->assertInstanceOf (ConstantClassPart::class, $cm->getConstant('SUBVERSION'));
		$this->assertNull ($cm->getConstant('SUB'));
		$this->assertNull($cm->getConstant('VERSION_NO'));
	}

	public function test_methodExists()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'methodExists'));

		$this->assertTrue($cm->methodExists('someAction'));
		$this->assertFalse($cm->methodExists('someActionThatDoNotExists'));
	}

	public function test_addUseSectionElement()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();

		$this->assertTrue(method_exists($cm, 'addUseSectionElement'));
		$qualifiedName = 'A\\B\\C';
		$unqualifiedName = 'SomeController';
		$cm->addUseSectionElement($qualifiedName . "\\" . $unqualifiedName);

		$useArray = [
			"use Gaad\\Chameleon\\Annotation\\ChController;\n",
			"use Gaad\\Chameleon\\Annotation\\WPHookPriority;\n",
			"use Gaad\\ChameleonTesting\\Some\\Name\\Parent\\TestClassesParent;\n",
			"use Gaad\\ChameleonTesting\\Some\\Traits\\AwesomeTrait;\n",
			"use Gaad\\ChameleonTesting\\Some\\Traits\\ShittyTrait;\n",
			"use Symfony\\Component\\Routing\\Annotation\\Route;\n"
		];
		$useArray[] = "use ". $qualifiedName ."\\".$unqualifiedName . ";\n";
		$this->assertEqualsCanonicalizing($useArray, $cm->getUse());

	}

}
