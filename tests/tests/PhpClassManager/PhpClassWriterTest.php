<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Exception\ClassWriterException;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;
use Gaad\Chameleon\Service\Schema\PhpClassWriter;

/**
 * @property AppKernel $app
 */
class PhpClassWriterTest extends ChameleonPhpClassTestCaseBase
{
	public string $classCodeExpected= <<<  EOF
<?php
namespace Gaad\ChameleonTesting\Some\Name;
use Gaad\Chameleon\Annotation\ChController;
use Gaad\Chameleon\Annotation\WPHookPriority;
use Gaad\ChameleonTesting\Some\Name\Parent\TestClassesParent;
use Gaad\ChameleonTesting\Some\Traits\AwesomeTrait;
use Gaad\ChameleonTesting\Some\Traits\ShittyTrait;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @ChController()
 */
class TestClasses
extends TestClassesParent
{
	use AwesomeTrait;
	use ShittyTrait;

	const VERSION = '12';
	/** @var string */
	protected const SUBVERSION = [
		'whiskey',
		'bourbon',
		'tequila'
	];

	/** @var string */
	private string \$a = 'abc';
	/**
	 * @var array|string[]
	 */
	public array \$b = [
		'very',
		'important',
		'info'
	];

	/**
	 * @Route("/some", name="some_place")
	 * @WPHookPriority(100)
	 *
	 * @return void
	 */
	function someAction()
	{
	}
	/**
	 * @Route("/some2", name="some_place")
	 * @WPHookPriority(100)
	 *
	 * @return void
	 */
	function some2Action()
	{
	}

}

EOF;


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_classWriterExists()
	{
		$this->assertInstanceOf(PhpClassWriter::class, $this->getClassWriter());
	}

	public function test_extractClassCode()
	{
		$cw = $this->getClassWriter();
		$this->assertTrue(method_exists($cw, 'extractClassCodeParts'));

		$classCodeExtracted = $cw->extractClassCodeParts();
		$this->assertEqualsCanonicalizing(PhpClassManagerTest::$expectedClassParts, array_keys($classCodeExtracted));

		$this->assertEquals('Gaad\ChameleonTesting\Some\Name', $classCodeExtracted['namespace']);

		$useSectionExpected = [
			"use Gaad\Chameleon\Annotation\ChController;\n",
			"use Gaad\Chameleon\Annotation\WPHookPriority;\n",
			"use Gaad\ChameleonTesting\Some\Name\Parent\TestClassesParent;\n",
			"use Gaad\ChameleonTesting\Some\Traits\AwesomeTrait;\n",
			"use Gaad\ChameleonTesting\Some\Traits\ShittyTrait;\n",
			"use Symfony\Component\Routing\Annotation\Route;\n"
		];
		$this->assertEqualsCanonicalizing($useSectionExpected, $classCodeExtracted['useSection']);
		//var_export([ 123], true)

		$classHeaderExpected = [
			"/**\n",
			" * @ChController()\n",
			" */\n",
			"class TestClasses\n",
			"extends TestClassesParent\n"
		];
		$this->assertEqualsCanonicalizing($classHeaderExpected, $classCodeExtracted['classHeader']);

		$classUseSectionExpected = [
			"\tuse AwesomeTrait;\n",
			"\tuse ShittyTrait;\n"
		];
		$this->assertEqualsCanonicalizing($classUseSectionExpected, $classCodeExtracted['classUseSection']);

		$constSectionExpected = [
			'VERSION' => [
				"\tconst VERSION = '12';\n",
				"\n"
			],
			'SUBVERSION' => [
				"\t/** @var string */\n",
				"\tprotected const SUBVERSION = [\n",
				"\t\t'whiskey',\n",
				"\t\t'bourbon',\n",
				"\t\t'tequila'\n",
				"\t];\n",
				"\n"
			]
		];
		$this->assertEqualsCanonicalizing($constSectionExpected, $classCodeExtracted['constSection']);

		$propertiesSectionExpected = [
			'a' => [
				"\t/** @var string */\n",
				"\tprivate string \$a = 'abc';\n",
				"\n"
			],
			'b' => [
				"\t/**\n",
				"\t * @var array|string[]\n",
				"\t */\n",
				"\tpublic array \$b = [\n",
				"\t\t'very',\n",
				"\t\t'important',\n",
				"\t\t'info'\n",
				"\t];\n",
				"\n"
			]
		];
		$this->assertEqualsCanonicalizing($propertiesSectionExpected, $classCodeExtracted['propertiesSection']);

		$methodsSectionExpected = [
			'someAction' => [
				"\t/**\n",
				"\t * @Route(\"/some\", name=\"some_place\")\n",
				"\t * @WPHookPriority(100)\n",
				"\t *\n",
				"\t * @return void\n",
				"\t */\n",
				"\tfunction someAction()\n",
				"\t{\n",
				"\n",
				"\t}\n",
				"\n",
			],
			'some2Action' => [
				"\t/**\n",
				"\t * @Route(\"/some2\", name=\"some_place\")\n",
				"\t * @WPHookPriority(100)\n",
				"\t *\n",
				"\t * @return void\n",
				"\t */\n",
				"\tfunction some2Action()\n",
				"\t{\n",
				"\n",
				"\t}\n",
				"\n"
			]
		];

		$this->assertEqualsCanonicalizing($methodsSectionExpected, $classCodeExtracted['methodSection']);
	}

	public function test_prepareClassCode()
	{
		$cw = $this->getClassWriter();
		$this->assertTrue(method_exists($cw, 'prepareClassCode'));

		$cw->prepareClassCode();

		$this->assertEquals($this->classCodeExpected, $cw->getClassCode());

	}

	public function test_prepareDirectoryBeforeSave()
	{
		$cw = $this->getClassWriter();
		$this->assertTrue(method_exists($cw, 'prepareClassCode'));

		$path = '/tmp/some/dir/someClass.php';
		try {
			$cw->prepareDirectoryBeforeSave($path);
		} catch (ClassWriterException $e) {
		}

		$this->assertDirectoryExists(dirname($path));
	}

	public function test_saveToFile()
	{
		$cw = $this->getClassWriter();
		$this->assertTrue(method_exists($cw, 'saveToFile'));

		$path = '/tmp/some/dir/someClass.php';
		try {
			$cw->saveToFile($path);
		} catch (ClassWriterException $e) {
		}

		$this->assertFileExists($path);
		$this->assertEquals($this->classCodeExpected, file_get_contents($path));
	}


}
