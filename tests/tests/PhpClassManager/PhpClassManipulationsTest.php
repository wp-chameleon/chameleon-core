<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Exception\ClassPartException;
use Gaad\Chameleon\Schema\Php\ConstantClassPart;
use Gaad\Chameleon\Schema\Php\DocBlockClassPart;
use Gaad\Chameleon\Schema\Php\MethodClassPart;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Gaad\Chameleon\Service\Schema\PhpClassManipulator;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;


/**
 * @property AppKernel $app
 */
class PhpClassManipulationsTest extends ChameleonPhpClassTestCaseBase
{


	public function test_classManipulatorAvailable()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);

		$cm = $this->getClassManager();
		$this->assertInstanceOf(PhpClassManipulator::class, $cm->getPhpClassManipulator());

	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 * @throws \Gaad\Chameleon\Exception\ClassPartException
	 */
	public function test_addMethod()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);
		$cm = $this->getClassManager();
		$methodClassPart = new MethodClassPart($cm, [
			'name' => 'dynamicallyAddedMethod'
		]);

		$this->assertTrue(method_exists($cm, 'addMethod'));

		$this->assertFalse($cm->methodExists('dynamicallyAddedMethod'));

		$cm->addMethod($methodClassPart);
		$this->assertTrue($cm->methodExists('dynamicallyAddedMethod'));
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 * @throws \Gaad\Chameleon\Exception\ClassPartException
	 */
	public function test_removeMethod()
	{
		$this->loadCorrespondingTestClass(__FUNCTION__);

		$cm = $this->getClassManager();
		$this->assertTrue(method_exists($cm, 'addMethod'));

		$this->assertTrue($cm->methodExists('someAction'));

		$cm->removeMethod('someAction');
		$this->assertFalse($cm->methodExists('someAction'));

		//removing not existing methods
		try {
			$cm->removeMethod('someNotExistingAction');
		} catch (\Exception $e) {
		}
		$this->assertInstanceOf(ClassPartException::class, $e, 'Method can be deleted event if doesnt exists');

	}

}
