<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Schema\Php\ConstantClassPart;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;

/**
 * @property AppKernel $app
 */
class ConstantClassPartTest extends ChameleonPhpClassTestCaseBase
{


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_classConstantClassPartExists()
	{
		$classPart = new ConstantClassPart($this->getClassManager());
		$this->assertInstanceOf(ConstantClassPart::class, $classPart);
	}


	public function test_getName()
	{
		$cm = $this->getClassManager();
		$classPart = new ConstantClassPart($cm, ['name' => 'newconst']);


		$this->assertEquals('newconst', $classPart->getName());

		$constants = $cm->getConstants();
		$this->assertEquals('VERSION', $constants[0]->getName());
		$this->assertNotEquals('SUB', $constants[1]->getName());

	}


}
