<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Schema\Php\Annotations\RouteAnnotationClassPart;
use Gaad\Chameleon\Schema\Php\DocBlockClassPart;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;

/**
 * @property AppKernel $app
 */
class DocBlockClassPartTest extends ChameleonPhpClassTestCaseBase
{
	private $docBlockClassPart;

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function setUp()
	{
		parent::setUp();
		$cm = $this->getClassManager();

		$this->docBlockClassPartRoute = new DocBlockClassPart($cm, [
			'annotations' => [
				new RouteAnnotationClassPart($cm, ['path' => 'locations/heaven/','name' => 'heaven']),
				new RouteAnnotationClassPart($cm, ['path' => 'locations/hell/','name' => 'hell'])
			]
		]);

	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_classDocBlockClassPartExists()
	{
		$cm = $this->getClassManager();
		$classPart = new DocBlockClassPart($cm, []);

		$this->assertInstanceOf(DocBlockClassPart::class, $classPart);
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_annotationsExists()
	{
		$this->assertNotEmpty($this->docBlockClassPartRoute->getAnnotations());
		$this->assertCount(2, $this->docBlockClassPartRoute->getAnnotations());
	}


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_generateCode()
	{
		$generatedCodeExpected = <<< EOF
/**
 * @Route("locations/heaven/", name="heaven")
 *
 * @Route("locations/hell/", name="hell")
 *
 */
EOF;

		$generateCode = $this->docBlockClassPartRoute->generateCode();
		$this->assertEquals($generatedCodeExpected, $generateCode[0]);
	}


}
