<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Schema\Php\Annotations\RouteAnnotationClassPart;
use Gaad\Chameleon\Schema\Php\DocBlockClassPart;
use Gaad\Chameleon\Schema\Php\MethodArgumentClassPart;
use Gaad\Chameleon\Schema\Php\MethodClassPart;
use Gaad\ChameleonTesting\Core\ChameleonPhpClassTestCaseBase;

/**
 * @property AppKernel $app
 */
class MethodClassPartTest extends ChameleonPhpClassTestCaseBase
{
	private $argumentClassPart1;

	public function setUp()
	{
		parent::setUp();
		$cm = $this->getClassManager();

		$this->argumentClassPart1 = new MethodArgumentClassPart($cm, [
			'methodName' => 'kickAssMethod',
			'name' => 'abc',
			'argumentType' => 'string',
			'defaultValue' => null,
			'isNull' => false,
			'canBeNull' => false,
		]);

	}


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_classMethodClassPartExists()
	{
		$classPart = new MethodClassPart($this->getClassManager());
		$this->assertInstanceOf(MethodClassPart::class, $classPart);
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_generateCodeEssential()
	{
		$cm = $this->getClassManager();
		$classPart = new MethodClassPart($cm, ['name' => 'abc']);

		$this->assertTrue(method_exists($classPart, 'generateCode'));

		$generatedCodeExpected = [
			"\n",
			"\n",
			"public  function abc(): void\n",
			"{\n",
			"\n",
			"\tretrun ;\n",
			"}\n",
			"\n"
		];
		$this->assertEqualsCanonicalizing($generatedCodeExpected, $classPart->generateCode());

	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_validate()
	{
		$cm = $this->getClassManager();
		$classPart = new MethodClassPart($cm);
		$this->assertTrue(method_exists($classPart, 'validate'));
		$this->assertTrue($classPart->validate());
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_getArgumentsCode()
	{
		$cm = $this->getClassManager();

		$args = [
			'name' => 'kickAssMethod',
			'arguments' => [$this->argumentClassPart1]
		];
		$methodClassPart = new MethodClassPart($cm, $args);
		$this->assertTrue(method_exists($methodClassPart, 'getArgumentsCode'));

		$argumentsLineExpected = 'string $abc';
		$generatedCode = $methodClassPart->getArgumentsCode();
		$this->assertEquals($argumentsLineExpected, $generatedCode);
	}


	/**
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	public function test_generateCodeArgumentsBasic()
	{
		$cm = $this->getClassManager();


		$args = [
			'name' => 'kickAssMethod',
			'arguments' => [$this->argumentClassPart1]
		];
		$classPart = new MethodClassPart($cm, $args);

		$this->assertTrue(method_exists($classPart, 'generateCode'));

		$generatedCodeExpected = [
			"\n",
			"\n",
			"public  function kickAssMethod(string \$abc): void\n",
			"{\n",
			"\n",
			"\tretrun ;\n",
			"}\n",
			"\n"
		];
		$this->assertEqualsCanonicalizing($generatedCodeExpected, $classPart->generateCode());

	}


	public function test_generateDocBlock()
	{
		$cm = $this->getClassManager();
		$docBlock = new DocBlockClassPart($cm, [
			'annotations' => [
				new RouteAnnotationClassPart($cm, ['path' => 'locations/heaven/', 'name' => 'heaven'])
			]
		]);

		$args = [
			'name' => 'kickAssMethod',
			'docBlock' => $docBlock
		];
		$classPart = new MethodClassPart($cm, $args);
		$this->assertTrue(method_exists($classPart, 'generateDocBlockCode'));


		$expectedDocBlockCode = <<< EOF
/**
 * @Route("locations/heaven/", name="heaven")
 *
 */
EOF;

		$generateDocBlockCode = $classPart->generateDocBlockCode();
		$this->assertEquals($expectedDocBlockCode, $generateDocBlockCode);

	}

	public function test_generateMethodWithDocBlock()
	{
		$cm = $this->getClassManager();
		$docBlock = new DocBlockClassPart($cm, [
			'annotations' => [
				new RouteAnnotationClassPart($cm, ['path' => 'locations/heaven/', 'name' => 'heaven'])
			]
		]);

		$args = [
			'name' => 'kickAssMethod',
			'docBlock' => $docBlock
		];
		$classPart = new MethodClassPart($cm, $args);
		$this->assertTrue(method_exists($classPart, 'generateDocBlockCode'));

		$generatedCode = [
			"\n",
			"/**\n",
			" * @Route(&quot;locations/heaven/&quot;, name=&quot;heaven&quot;)\n",
			" *\n",
			" */\n",
			"public  function kickAssMethod(): void\n",
			"{\n",
			"\n",
			"\tretrun ;\n",
			"}\n",
			"\n"
		];

		$classPart->generateCode();
		$this->assertEqualsCanonicalizing($generatedCode, $generatedCode);

	}

}
