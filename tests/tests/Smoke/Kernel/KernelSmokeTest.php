<?php

namespace Gaad\Chameleon\Tests\Smoke;

use Gaad\Chameleon\AppKernel;
use WP_UnitTestCase;


/**
 * @property AppKernel $app
 */
class KernelSmokeTest extends WP_UnitTestCase
{

	public static string $env = 'dev';
	private ?AppKernel $kernel;

	public static function setUpBeforeClass(): void
	{
		$GLOBALS['kernel'] = $GLOBALS['kernel'] instanceof AppKernel ? $GLOBALS['kernel'] : new AppKernel('dev');
	}

	public function setUp()
	{
		$this->kernel = $GLOBALS['kernel'];
	}

	public function test_app_kernel_exists()
	{
		$this->assertTrue($this->kernel instanceof AppKernel);
	}

	public function test_get_service_ids()
	{
		$this->assertNotEmpty($this->kernel->getServiceIdsList());
	}

	public function test_core_services_exists()
	{
		$serviceIdsList = $this->kernel->getServiceIdsList();

		$this->assertTrue(in_array('config',$serviceIdsList));
		$this->assertTrue(in_array('filesystem',$serviceIdsList));
		$this->assertTrue(in_array('mustache',$serviceIdsList));
		$this->assertTrue(in_array('extensions.manager',$serviceIdsList));
		$this->assertTrue(in_array('orm',$serviceIdsList));
		$this->assertTrue(in_array('twig',$serviceIdsList));
		$this->assertTrue(in_array('extension.service.list',$serviceIdsList));
	}

}
