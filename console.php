<?php
use Symfony\Component\Console\Application;

require_once("../../../wp-load.php");

$application = apply_filters('Chameleon.Console.Commands.Add', new Application());
$application->run();
