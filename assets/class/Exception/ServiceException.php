<?php

namespace Gaad\Chameleon\Exception;

use Exception;

class ServiceException extends Exception
{

	public static function notExistsError(string $name): ServiceException
	{
		return new self( 'Service `'.$name.'` do not exists', 500);
	}

}
