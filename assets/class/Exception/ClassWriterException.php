<?php

namespace Gaad\Chameleon\Exception;

use Exception;

class ClassWriterException extends Exception
{

	public static function notInitialized(): ClassWriterException
	{
		return new self( 'Initialization failed.', 500);
	}


}
