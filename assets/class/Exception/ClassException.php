<?php

namespace Gaad\Chameleon\Exception;

use Exception;

class ClassException extends Exception
{

	public static function notExists(?string $name): ClassException
	{
		return new self( 'Class `'.(string)$name.'` do not exists', 500);
	}

	public static function propertyNotExists(string $className, string $propertyName): ClassException
	{
		return new self( 'Class `'.(string)$className.'` do not have essential property: `'.(string)$propertyName.'`', 500);
	}

	public static function essentialPropertyEmpty(string $className, string $propertyName): ClassException
	{
		return new self( 'Class `'.(string)$className.'` do have essential property: `'.(string)$propertyName.'` but its empty.', 500);
	}

	public static function dynamicMethodNotExists(string $className, string $methodName): ClassException
	{
		return new self( 'Method `'.(string)$className.'::'.(string)$methodName.'` not exists or it is not have public access.', 500);
	}

	public static function classPartBeginNull(): ClassException
	{
		return new self( 'Php class part needs to have begin of the code as integer, null given.', 500);
	}

}
