<?php

namespace Gaad\Chameleon\Exception;

use Exception;

class ConfigException extends Exception
{

	public static function parameterMissing( string $parameterMissingName): ConfigException
	{
		return new self( 'Configuration cannot be parsed because the `'.(string)$parameterMissingName.'` parameter is missing.', 500);
	}

	public static function unknownPath(string $optionPath)
	{
		return new self( 'Configuration value path `'.(string)$optionPath.'` is incorrect.', 500);
	}

}
