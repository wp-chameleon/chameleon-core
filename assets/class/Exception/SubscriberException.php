<?php

namespace Gaad\Chameleon\Exception;

use Exception;

class SubscriberException extends Exception
{

	public static function alreadyExistsError(string $name): self
	{
		return new self( 'Subscriber `'.$name.'` already exists', 500);
	}

	public static function notFoundError(string $name): self
	{
		return new self( 'Subscriber `'.$name.'` doesn\'t exists', 500);
	}

}
