<?php

namespace Gaad\Chameleon\Exception;

use Exception;

class ClassPartException extends Exception
{


	public static function classMethodExists($name): ClassPartException
	{
		return new self( 'Class already has mathod `'.$name.'`.', 500);
	}

	public static function classMethodDoNotExists($name): ClassPartException
	{
		return new self( 'Class method `'.$name.'` do not exists.', 500);
	}

	public static function classPartBeginNull(): ClassPartException
	{
		return new self( 'Php class part needs to have begin of the code as integer, null given.', 500);
	}

	public static function searchForCodeBeforeInit(): ClassPartException
	{
		return new self( 'Php class part needs to have begin and end set up before code search begins.', 500);
	}


}
