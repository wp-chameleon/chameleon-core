<?php

namespace Gaad\Chameleon\Config;

interface ConfigurationManagerInterface
{
    function getConfiguration();

}