<?php

namespace Gaad\Chameleon\Config;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class GeneralConfigurationsManager implements ConfigurationManagerInterface
{

    public function __construct()
    {
    }

    /**
     * @return TreeBuilder
     */
    function getConfiguration(): ConfigurationInterface
    {
        return apply_filters('Chameleon.Config.Configuration.Filter', new GeneralConfiguration());
    }
}