<?php

namespace Gaad\Chameleon\Config;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

abstract class ExtensionConfigurationBase implements ConfigurationInterface
{
    public TreeBuilder $treeBuilder;
	protected string $rootNodeName = 'general';

    public function __construct()
    {
		$rootNodeName = $this->getRootNodeName();
		$this->setTreeBuilder(new TreeBuilder($rootNodeName));
    }


    public function getConfigTreeBuilder()
    {
        return $this->treeBuilder;
    }

    public function applyExtensionManifestSupport(ArrayNodeDefinition $node)
    {
		$node
			->children()
				->arrayNode('manifest')
					->children()
						->scalarNode('name')->end()
					->end()
				->end()
			->end();
    }

	/**
	 * @return TreeBuilder
	 */
	public function getTreeBuilder(): TreeBuilder
	{
		return $this->treeBuilder;
	}

	/**
	 * @param TreeBuilder $treeBuilder
	 */
	public function setTreeBuilder(TreeBuilder $treeBuilder): void
	{
		$this->treeBuilder = $treeBuilder;
	}

	/**
	 * @return string
	 */
	public function getRootNodeName(): string
	{
		return $this->rootNodeName;
	}

	/**
	 * @param string $rootNodeName
	 */
	public function setRootNodeName(string $rootNodeName): void
	{
		$this->rootNodeName = $rootNodeName;
	}

}
