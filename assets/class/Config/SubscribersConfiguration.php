<?php

namespace Gaad\Chameleon\Config;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class SubscribersConfiguration implements ConfigurationInterface
{
    public TreeBuilder $treeBuilder;

    public function __construct()
    {
        $this->treeBuilder = new TreeBuilder('general');
    }


    public function getConfigTreeBuilder()
    {
        $rootNode = $this->treeBuilder->getRootNode();




		$rootNode
            ->children()
                ->arrayNode('doctrine')
                    ->children()
						->arrayNode('orm')
							->children()
								->scalarNode('proxy_dir')->end()
								->arrayNode('directories')
								->end()
						->end()
					->end()
                ->end()
            ->end();

		$rootNode
            ->children()
                ->arrayNode('database')
                    ->children()
						->arrayNode('connections')
							->useAttributeAsKey('name')
							->arrayPrototype()
							->children()
								->scalarNode('driver')->end()
								->scalarNode('host')->end()
								->scalarNode('dbname')->end()
								->scalarNode('user')->end()
								->scalarNode('password')->end()
						->end()
					->end()
                ->end()
            ->end();

		$rootNode
            ->children()
                ->arrayNode('naming')
					->useAttributeAsKey('name')
					->arrayPrototype()
					->children()
						->scalarNode('namespace')->end()
				->end()
            ->end();

		$rootNode
			->children()
				->arrayNode('cache')
						->children()
							->scalarNode('directory')->end()
					->end()
				->end()
			->end();

        return $this->treeBuilder;
    }

}
