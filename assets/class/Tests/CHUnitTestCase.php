<?php
/**
 * This is based on too high for WP version of PHP unit, downgrade to 7.5 before usage
 */
namespace Gaad\Chameleon\Tests;

use Gaad\Chameleon\Service\Extensions\ExtensionsManager;
use Gaad\Chameleon\Service\Schema\ElementsNames;
use Gaad\Chameleon\Traits\CHUnitTestKernel;
use ReflectionClass;

/**
 * @property ElementsNames $elementsNames
 */
class CHUnitTestCase extends \WP_UnitTestCase
{
	use CHUnitTestKernel;

	public ?string $extensionName = null;
	public static string $pluginsDirectory = '/var/www/html/wp-content/plugins';

	protected function setUp(): void
	{
		parent::setUp();

		$this->kernel = $GLOBALS['kernel'];
		$this->elementsNames = $this->kernel->getService('chameleon-core.schema.elements.names');

		$extensionName = $this->getExtensionName();
		if (!empty($extensionName)) $this->createExtension($extensionName);
	}

	public function createExtension(string $extensionName): void
	{
		/** @var ExtensionsManager $extensionsManager */
		$extensionsManager = $this->kernel->getService('extensions.manager');
		$extensionsManager->setPluginsDirectory(self::$pluginsDirectory);
		$extensionsManager->prepareData($extensionName);
		$extensionExists = $extensionsManager->extensionExists($extensionName);
		if (!$extensionExists) {
			$extensionsManager->createExtension($extensionName, false);
			$this->kernel->clearCache();
		}
	}

	protected static function getMethod($class, $name) {
		$class = new ReflectionClass($class);
		$method = $class->getMethod($name);
		$method->setAccessible(true);
		return $method;
	}

	/**
	 * @return string|null
	 */
	public function getExtensionName(): ?string
	{
		return $this->extensionName;
	}

	/**
	 * @param string|null $extensionName
	 */
	public function setExtensionName(?string $extensionName): void
	{
		$this->extensionName = $extensionName;
	}

	/**
	 * @return string
	 */
	public static function getPluginsDirectory(): string
	{
		return self::$pluginsDirectory;
	}

	/**
	 * @return ElementsNames
	 */
	public function getElementsNames(): ElementsNames
	{
		return $this->elementsNames;
	}


}
