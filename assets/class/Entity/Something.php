<?php

namespace Gaad\Chameleon\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="someting")
 * @ORM\Entity(repositoryClass="Gaad\Chameleon\Repository\SomethingRepository")
 * @ORM\MappedSuperclass
 */
class Something
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected int $id;

}
