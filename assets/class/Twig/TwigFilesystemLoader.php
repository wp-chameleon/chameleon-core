<?php
namespace Gaad\Chameleon\Twig;

use Twig\Loader\FilesystemLoader;

class TwigFilesystemLoader
{

	public function getLoader(): FilesystemLoader
	{
		return new FilesystemLoader(
			apply_filters('Chameleon.Twig.Directories.Filter', [
				__CEBOARD_CORE_DIR__ . '/templates'
			])
		);
	}

}
