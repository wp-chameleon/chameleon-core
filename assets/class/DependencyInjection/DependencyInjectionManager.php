<?php

namespace Gaad\Chameleon\DependencyInjection;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Controller\ControllersManager;
use Gaad\Chameleon\Service\Event\EventsManager;
use Gaad\Chameleon\Service\Routing\RoutesManager;
use Gaad\Chameleon\Service\WP\ModsManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;


class DependencyInjectionManager
{
	const DI_CONTAINER_DUMP_FILE_NAME = 'DIContainer.php';
	const DI_CONTAINER_CLASS = 'DIContainer';

	private string $servicesFile = 'services.yaml';
	private $container = null;
	private string $dumpFile = '/tmp/services.yaml';
	private array $configDirectories = [];
	private AppKernel $kernel;

	public function __construct(AppKernel $kernel, ?bool $forceSetup = false)
	{
		/**
		 * @var EventsManager $EventsManager
		 * @var ControllersManager $EventsManager
		 * @var ModsManager $EventsManager
		 * @var RoutesManager $EventsManager
		 */

		$this->kernel = $kernel;
		$this->configDirectories = apply_filters('Chameleon.Services.File.Filter', [__CEBOARD_CORE_DIR__ . '/config',]);
		$this->setContainer(new ContainerBuilder());
		$this->setDumpFile();
		$this->setupContainer($forceSetup);
		$this->dumpContainer();

		$containerBuilder = $this->getContainer();

		$containerBuilder->get('Assetic');

		$EventsManager = $containerBuilder->get('EventsManager');
		$EventsManager->registerSubscribers();

		$containerBuilder->get('ControllersManager');


		$containerBuilder->get('ModsManager');

		$RoutesManager = $containerBuilder->get('RoutesManager');
		add_filter('init', [$RoutesManager,'saveRewriteCache']); //this one is redundanta fter dev
		//add_filter('init', [$RoutesManager,'applyRoutes']);
	}

	public function setupContainer(?bool $force = false)
	{
		if ($this->containerDumpExists() && ! $force) {
			$this->getDumpedContainer();
			return;
		}

		$containerBuilder = $this->getContainer();
		$containerBuilder->addCompilerPass(new MergeServicesFromPluginsCompilerPass());
		$containerBuilder->addCompilerPass(new RegisterListenersPass(), PassConfig::TYPE_BEFORE_REMOVING);

		$loader = $this->getServicesFileLoader();
		$loader->load($this->servicesFile);

		$this->setGlobalParameters($containerBuilder);

		$containerBuilder->compile(true);
	}

	/**
	 * @return ContainerBuilder
	 */
	public function getContainer()
	{
		return $this->container;
	}

	/**
	 * @param ContainerBuilder $container
	 */
	public function setContainer(ContainerBuilder $container): void
	{
		$this->container = $container;
	}

	private function dumpContainer()
	{
		if ($this->containerDumpExists()) return;

		$dumper = new PhpDumper($this->getContainer());
		file_put_contents(
			$this->getDumpFile(),
			$dumper->dump(['class' => self::DI_CONTAINER_CLASS])
		);
	}

	private function setDumpFile()
	{
		$this->dumpFile = $this->getDumpFileDirectory() . '/' . self::DI_CONTAINER_DUMP_FILE_NAME;
	}

	/**
	 * @return string
	 */
	public function getDumpFile(): string
	{
		return $this->dumpFile;
	}

	/**
	 * @return string
	 */
	private function getDumpFileDirectory(): string
	{
		$dir = __CEBOARD_CORE_DIR__ . '/cache/' . $this->getKernel()->getEnv() . '/DependencyInjection/';
		@mkdir($dir, 0775, true);
		return $dir;
	}

	private function setGlobalParameters(ContainerBuilder $containerBuilder)
	{
		$containerBuilder->setParameter('env', $this->getKernel()->getEnv());
	}

	private function containerDumpExists()
	{
		$dumpFile = $this->getDumpFile();
		return is_readable($dumpFile) && !$this->isDirty($dumpFile);
	}

	private function getDumpedContainer()
	{
		include($this->getDumpFile());
		$className = self::DI_CONTAINER_CLASS;
		if (class_exists($className))
			$this->container = new $className();
	}

	/**
	 * @return AppKernel
	 */
	public function getKernel(): AppKernel
	{
		return $this->kernel;
	}

	/**
	 * @return YamlFileLoader
	 */
	private function getServicesFileLoader(): ?YamlFileLoader
	{
		$containerBuilder = $this->getContainer();
		if (!$containerBuilder instanceof ContainerBuilder) return null;
		$locator = new FileLocator($this->configDirectories);

		return new YamlFileLoader($containerBuilder, $locator);
	}

	/**
	 * Checks if container needs recompiling
	 *
	 * @param string $dumpFile
	 * @return bool
	 */
	private function isDirty(string $dumpFile)
	{
		$newest = 0;
		foreach ($this->configDirectories as $dir) {
			$filePath = $dir . '/' . $this->servicesFile;
			if (is_readable($filePath)) {
				$ftime = \filemtime($filePath);
				$newest = $ftime > $newest ? $ftime : $newest;
			}
		}

		return \filemtime($this->getDumpFile()) < $newest;
	}
}
