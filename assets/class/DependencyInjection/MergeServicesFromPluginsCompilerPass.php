<?php
namespace Gaad\Chameleon\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Yaml;

class MergeServicesFromPluginsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $configDirectories = apply_filters('Chameleon.Services.File.Filter', [
            __CEBOARD_CORE_DIR__ . '/config',
        ]);

        $servicesFiles = [];
        foreach ($configDirectories as $dir) {
            $file = $dir . '/services.yaml';
            if (is_file($file)) $servicesFiles[] = str_replace('//', '/', $file);
        }

        $content = [];
        foreach ($servicesFiles as $servicesFile) {
            $nextContent = Yaml::parse(file_get_contents($servicesFile));

            if (!empty($nextContent))
                foreach ($nextContent as $k => $v)
                    if (isset($content[$k]))
                        $content[$k] = array_merge($content[$k], $v);
                    else $content[$k] = $v;
        }

        $tmpfname = tempnam("/tmp", "services");
        file_put_contents($tmpfname, Yaml::dump($content, 50));


        $loader = new YamlFileLoader($container, new FileLocator(['/tmp']));
        $loader->load($tmpfname);

        return $container;
    }
}
