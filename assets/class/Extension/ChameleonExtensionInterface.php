<?php

namespace Gaad\Chameleon\Extension;

interface ChameleonExtensionInterface
{
	const defaultHeaders = [
		'Name' => 'Plugin Name',
		'PluginURI' => 'Plugin URI',
		'Version' => 'Version',
		'Description' => 'Description',
		'Author' => 'Author',
		'AuthorURI' => 'Author URI',
		'TextDomain' => 'Text Domain',
		'DomainPath' => 'Domain Path',
		'Network' => 'Network',
		'RequiresWP' => 'Requires at least',
		'RequiresPHP' => 'Requires PHP',
		'UpdateURI' => 'Update URI'
	];

	public function getName(): string;

	public function getAbsDir(): string;
}
