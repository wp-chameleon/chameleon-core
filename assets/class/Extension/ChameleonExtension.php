<?php

namespace Gaad\Chameleon\Extension;

use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Annotations\AnnotationReader;
use Exception;
use Gaad\Chameleon\Annotation\ChController;
use Gaad\Chameleon\Controller\ControllerReflectionManager;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use ReflectionClass;
use Symfony\Component\Yaml\Yaml;

class ChameleonExtension implements ChameleonExtensionInterface
{
	private string $name = '';
	private string $absDir = '';
	private string $relControllersDir = '/assets/class/Controller';
	private array $headers = [];
	private string $servicesFilePath;
	private array $servicesDefinitions;
	private array $serviceBlackList = ['_defaults', 'imports', 'extension.service.list'];
	private array $servicesIds;
	private array $controllers = [];
	private PhpClassManager $classManager;

	/**
	 * @throws Exception
	 */
	public function __construct(string $name)
	{
		$this->name = $name;
		$this->absDir = $this->getAbsDir();
		$this->servicesFilePath = $this->getServicesFilePath();
		$this->validate();

		$this->servicesDefinitions = $this->getServicesDefinitions();
		$this->servicesIds = $this->getServicesIds();
		$this->headers = $this->fetchHeaders();
	}

	function recursiveControllerSearch($dir, &$results = []): array
	{
		$ls = glob($dir);

		if (empty($ls)) return $results;

		foreach ($ls as $item) {
			if (is_dir($item))
				$this->recursiveControllerSearch($item . '/*', $results);

			if (is_file($item) && $this->isControllerClass($item))
				$results['c_'.md5($item)] = new ControllerReflectionManager($item, $this->getClassManager());
		}

		return $results;
	}

	public function fetchControllers(): void
	{
		$results = [];
		$query = $this->getAbsDir() . $this->relControllersDir . '/*';
		$this->setControllers($this->recursiveControllerSearch($query, $results));
	}

	public function getAbsDir(): string
	{
		return WP_PLUGIN_DIR . '/' . $this->name;
	}

	public function setAbsDir(string $absDir): void
	{
		$this->absDir = $absDir;
	}

	public function getName(): string
	{
		return $this->name;
	}

	private function fetchHeaders()
	{
		return get_file_data($this->getAbsDir() . '/' . $this->getName() . '.php', self::defaultHeaders);
	}

	private function getServicesFilePath(): string
	{
		return $this->getAbsDir() . '/config/services.yaml';
	}

	public function getServicesDefinitions(): array
	{
		$services = Yaml::parse(file_get_contents($this->getServicesFilePath()));
		return !empty($services) ? $this->filterServicesDefinitions($services['services']) : [];
	}

	private function filterServicesDefinitions(array $services): array
	{
		$serviceBlackList = $this->getServiceBlackList();
		$services_ = [];
		foreach ($services as $k => $v) {
			if (in_array($k, $serviceBlackList)) continue;
			if (!is_array($v)) continue;
			if (!in_array('public', $v)) continue;
			if (!filter_var($v['public'], FILTER_VALIDATE_BOOLEAN)) continue;

			$services_[$k] = $v;
		}

		return $services_;
	}

	public function getServiceBlackList(): array
	{
		return $this->serviceBlackList;
	}

	/**
	 * @param mixed $serviceBlackList
	 */
	public function setServiceBlackList(array $serviceBlackList): void
	{
		$this->serviceBlackList = $serviceBlackList;
	}

	private function getServicesIds(): array
	{
		return array_keys($this->getServicesDefinitions());
	}

	/**
	 * @return array|string[]
	 */
	public function getHeaders(): array
	{
		return $this->headers;
	}

	/**
	 * @throws Exception
	 */
	private function validate()
	{
		if (
			!is_dir($this->getAbsDir())
			|| !is_file($this->getServicesFilePath())
		)
			throw new Exception("Extension " . $this->getName() . " doesnt exists.");
	}

	/**
	 * @return array
	 */
	public function getControllers(): array
	{
		return $this->controllers;
	}

	/**
	 * @param array $controllers
	 */
	public function setControllers(array $controllers): void
	{
		$this->controllers = $controllers;
	}

	/**
	 * @throws \ReflectionException
	 */
	private function isControllerClass($item)
	{
		$controllerNamespace = $this->getClassManager()->getClassNamespaceFromPath($item);

		$reflection = new ReflectionClass($controllerNamespace);
		$annotationReader = new AnnotationReader();
		$classAnnotations = array_filter($annotationReader->getClassAnnotations($reflection),
			function (Annotation $item) {
				return $item instanceof ChController;
			});

		if (empty($classAnnotations)) return false;

		return true;
	}


	public function getClassManager(): ?PhpClassManager
	{
		return $this->classManager;
	}


	public function setClassManager(PhpClassManager $classManager): void
	{
		$this->classManager = $classManager;
	}


}
