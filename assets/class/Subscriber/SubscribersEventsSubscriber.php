<?php
namespace Gaad\Chameleon\Subscriber;

use Gaad\Chameleon\Traits\UpdateExtensionPatchVersion;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class SubscribersEventsSubscriber implements EventSubscriberInterface
{

	use UpdateExtensionPatchVersion;

	public static function getSubscribedEvents()
	{
		return [
			'kernel.subscriber.created' => 'updateExtensionVersion',
			'kernel.subscriber.removed' => 'updateExtensionVersion'
		];
	}

}
