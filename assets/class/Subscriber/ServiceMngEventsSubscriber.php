<?php
namespace Gaad\Chameleon\Subscriber;

use Gaad\Chameleon\Traits\UpdateExtensionPatchVersion;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ServiceMngEventsSubscriber implements EventSubscriberInterface
{
	use UpdateExtensionPatchVersion;

	public static function getSubscribedEvents()
	{
		return [
			'kernel.service.dependency.modification' => 'updateExtensionVersion',
			'kernel.service.removed' => 'updateExtensionVersion',
			'kernel.service.created' => 'updateExtensionVersion',
		];
	}

}
