<?php
namespace Gaad\Chameleon\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\Event;

class KernelEventsSubscriber implements EventSubscriberInterface
{

	const CONTROLLERS_REGISTERED = 'kernel.controllers.registered';
	const ROUTES_REGISTERED = 'kernel.routes.registered';

	public static function getSubscribedEvents()
	{
		return [
			self::CONTROLLERS_REGISTERED => 'fooAction',
			self::ROUTES_REGISTERED => 'fooAction2',
		];
	}

	public function fooAction(Event $event)
	{
		$r=1;
	}

	public function fooAction2(Event $event)
	{
		$r=1;
	}

}
