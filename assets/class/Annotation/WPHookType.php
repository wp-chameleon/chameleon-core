<?php
/*
 * Describes WordPress hook type: action or filter
 */
namespace Gaad\Chameleon\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class WPHookType
{
	public string $value;

	/**
	 * @throws \Exception
	 */
	public function __construct(array $args)
	{
		if(!in_array($args['value'], array('action', 'filter')))
		throw new \Exception('Wordpress hook type can be either action or filter.');

		$this->value = $args['value'];
	}

	public function getValue(): string
	{
		return $this->value;
	}

}
