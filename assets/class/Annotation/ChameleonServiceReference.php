<?php

namespace Gaad\Chameleon\Annotation;

/**
 * @Annotation
 * @Target("CLASS")
 */
class ChameleonServiceReference
{
	public string $referenceName;

	/**
	 * @param string $referenceName
	 */
	public function __construct(string $referenceName)
	{
		$this->referenceName = $referenceName;
	}

	/**
	 * @return string
	 */
	public function getReferenceName(): string
	{
		return $this->referenceName;
	}


}
