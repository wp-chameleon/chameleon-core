<?php
/*
 * Describes WordPress hook type: action or filter
 */

namespace Gaad\Chameleon\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
class ChController extends Annotation
{
	public string $name;


	public function getName(): string
	{
		return $this->name;
	}


	public function setName(string $name): void
	{
		$this->name = $name;
	}


}
