<?php
/*
 * Describes WordPress hook tag that method should be applied to
 */
namespace Gaad\Chameleon\Annotation;

/**
 * @Annotation
 * @Target({"METHOD"})
 */
class WPHookTag
{
	public string $value;

	public function __construct(array $args)
	{
		$this->value = $args['value'];
	}

	public function getValue(): string
	{
		return $this->value;
	}

}
