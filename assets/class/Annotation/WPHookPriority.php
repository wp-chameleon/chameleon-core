<?php

namespace Gaad\Chameleon\Annotation;

/**
 * @Annotation
 * @Target("METHOD")
 */
class WPHookPriority
{
	public string $value;

	public function __construct(array $args)
	{
		$this->value = $args['value'];
	}


	public function getValue(): string
	{
		return $this->value;
	}


}
