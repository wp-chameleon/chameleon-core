<?php
namespace Gaad\Chameleon\Service\Routing;

use Gaad\Chameleon\Service\Routing\Parent\RoutesListParent;


class RoutesList extends RoutesListParent
{

	public function getRoutes()
	{
		$routesManager = $this->getRoutesManager();
		return $routesManager->getRoutes();
	}
}
