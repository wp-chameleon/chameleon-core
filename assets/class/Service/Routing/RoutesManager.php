<?php

namespace Gaad\Chameleon\Service\Routing;

use Gaad\Chameleon\Controller\ControllerReflectionManager;
use Gaad\Chameleon\Event\RoutesRegisteredEvent;
use Gaad\Chameleon\Service\Routing\Parent\RoutesManagerParent;
use Gaad\Chameleon\Subscriber\KernelEventsSubscriber;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Annotation\Route as AnnotationRoute;
use SplFileInfo;


class RoutesManager extends RoutesManagerParent
{
	const description = 'MVC Routing management';
	const CONTROLLER_QUERY_VAR = 'ch_controller';
	const ACTION_QUERY_VAR = 'ch_action';

	protected RouteCollection $routesCollection;
	protected string $configField = 'routes';
	protected string $resourcesTag = 'MVCRoutes';
	protected string $resourceFileName = 'routes.yaml';
	protected string $parametersFileName = 'parameters.yaml';
	protected string $cacheFormat = 'xml';
	private ?array $matchedRoute;

	/**
	 * Returns matched route
	 *
	 * @return array|null
	 */
	public function matchRoute(): ?array
	{
		$matchedRoute = null;
		$context = new RequestContext();
		$context->fromRequest($this->getRequest());
		$matcher = new UrlMatcher($this->getRoutesCollection(), $context);

		try {
			$pathInfo = $this->getRequest()->getPathInfo();
			$matchedRoute = $matcher->match($pathInfo);
		} catch (\Exception $e) {
		}

		$this->setMatchedRoute($matchedRoute);

		return $matchedRoute;
	}

	/**
	 * Lads cached data into the object
	 *
	 * @return bool
	 */
	protected function loadCache(): bool
	{
		parent::loadCache();

		$file = str_replace($this->getCacheFormat(), 'php', $this->getCacheFilePath());
		$cacheExists = is_readable($file);
		if ($cacheExists) {
			require_once $file;
			//@TODO here loading other partial resources should be triggered when not in production mode

		}

		!$cacheExists ?: $this->setInitialized(true);
		return $cacheExists;
	}

	/**
	 * Hooked:
	 * - query vars extension filter for controller support
	 * - template advanced loading method for controller support
	 *
	 * @return void
	 */
	public function onInitComplete(): void
	{
		$this->setRoutesCollection(new RouteCollection());
		$this->addRoutesToCollection();

		add_filter('query_vars', [$this, 'queryVarsFilter']);
		add_filter('template_include', [$this, 'getTemplate']);
	}

	/**
	 * Updates RoutesCollection for route matching
	 *
	 * @return void
	 */
	public function addRoutesToCollection(): void
	{
		foreach ($this->getConfigFieldValue() as $details)
			foreach ($details['routes'] as $route) {
				//@Todo check why sometimes the routes are empty and the add command throw mismatched type error
				if (null === $route['name']) continue;

				$path = $route['path'];
				$path = '/' !== $path[strlen($path) - 1] ? $path . '/' : $path[0];
				$routeComponent = new Route($path, $route);

				$this->getRoutesCollection()->add($route['name'], $routeComponent);
			}
	}

	/**
	 * Rebuild routing mechanism cache procedure
	 *
	 * @throws \Exception
	 */
	protected function refreshCachedResource(): void
	{
		$this->registerRoutes();
		$this->saveCache();
		$this->saveRewriteCache();
	}

	/**
	 * Generate code for registering rewrite rules in WordPress system.
	 * Method computes registration of all routes scanned from controllers and save it as php file.
	 *
	 * @return void
	 */
	function saveRewriteCache()
	{
		$rules = $this->generateRulesApplierInstructionsCode();
		$this->saveRulesApplierCache($rules);
		flush_rewrite_rules(true);
	}

	/**
	 * Scan available controllers and register routes bound methods
	 *
	 * @throws \Exception
	 */
	function registerRoutes()
	{
		$controllers = $this->getControllersManager()->getControllers(); //dynamic config resource getter
		$controllers_ = [];
		$routes = [];

		array_walk_recursive($controllers, function ($item) use (&$controllers_) {
			$controllers_[] = $item;
		});

		array_map(function (ControllerReflectionManager $controller) use (&$routes) {
			$controller->fetchRoutes();
			$routes[$controller->getName()] = $controller->getRoutes();
		}, $controllers_);

		$this->setConfigFieldValue($routes);
		$this->getEventDispatcher()->dispatch(new RoutesRegisteredEvent(), KernelEventsSubscriber::ROUTES_REGISTERED);
	}

	/**
	 * Parse route path
	 *
	 * @param $route
	 * @return string
	 */
	private function wordpressRoutePathCompatibilityFilter($route): string
	{
		if (is_null($route)) return '';
		$path = '';

		if ($route instanceof AnnotationRoute)
			$path = $route->getPath();

		if (is_array($route))
			$path = $route['path'];

		//compatibility with Symfony Routing, pass variables in curly brackets as (.*)
		$pathParts = array_map(function (string $part) {
			if ('{' === $part[0] && '}' === $part[strlen($part) - 1])
				return '(.*)';
			return $part;
		}, explode('/', $path));

		return implode('/', $pathParts) . '/?$';
	}

	/**
	 * @inheritDoc
	 *
	 * @return string
	 */
	public function getCacheFormat(): string
	{
		return $this->cacheFormat;
	}

	private function saveRulesApplierCache(array $rules): void
	{
		$file = str_replace($this->getCacheFormat(), 'php', $this->getCacheFilePath());
		$cache = new ConfigCache($file, true);
		$template = file_get_contents(__CEBOARD_CORE_DIR__ . '/assets/tpl/cache/' . basename($file) . '.tpl');
		$arguments = [
			'name' => $this->getResourcesTag(),
			'constructorContent' => "\n\t\t\t" . implode("\n\t\t\t", $rules)
		];
		$content = $this->getMustache()->render($template, $arguments);

		$cache->write($content, []);
	}

	function queryVarsFilter($query_vars): array
	{
		$query_vars[] = self::CONTROLLER_QUERY_VAR;
		$query_vars[] = self::ACTION_QUERY_VAR;

		return $query_vars;
	}

	/**
	 * Return Request object
	 *
	 * @return Request
	 */
	private function getRequest(): Request
	{
		return new Request($GLOBALS["_GET"], $GLOBALS["_POST"], (array)$_SERVER['PATH_INFO'], $GLOBALS["_COOKIE"], $GLOBALS["_FILES"], $GLOBALS["_SERVER"]);
	}

	/**
	 * Returns dedicated view for controller originated content
	 *
	 * @return string
	 */
	public function getControllerView(): string
	{
		return $this->getControllersManager()->getControllerView($GLOBALS["_SERVER"]["REQUEST_URI"]);
	}

	/**
	 * Return path for main render template
	 *
	 * @param $path
	 * @return string
	 */
	public function getTemplate($path): string
	{
		return $this->matchRoute() ? $this->getControllerView() : $path;
	}

	/**
	 * @return RouteCollection
	 */
	public function getRoutesCollection(): RouteCollection
	{
		return $this->routesCollection;
	}

	/**
	 * @param RouteCollection $routesCollection
	 */
	public function setRoutesCollection(RouteCollection $routesCollection): void
	{
		$this->routesCollection = $routesCollection;
	}

	/**
	 * Generate WordPress compatible PHP code for applying a rewrite rules
	 *
	 * @return array|void
	 */
	protected function generateRulesApplierInstructionsCode()
	{
		$configFieldValue = $this->getConfigFieldValue();
		if (empty($configFieldValue)) return;

		$rules = [];
		foreach ($configFieldValue as $details)
			foreach ($details['routes'] as $methodName => $route) {
				$controllerInfo = new SplFileInfo($details['controller']);
				$extension = $controllerInfo->getExtension();
				$controllerNamespace = 'Gaad\Chameleon\Controller\\' . str_replace(['.' . $extension, '/'], ['', '\\'], explode('/Controller/', $controllerInfo->getPathname())[1]);
				$controllerName = str_replace('', '', trim($controllerNamespace));
				$action = str_replace('Action', '', $methodName);
				$pathPage = $this->wordpressRoutePathCompatibilityFilter($route);
				$pathPage = '/' === $pathPage[0] ? substr($pathPage, 1) : $pathPage;
				$rewriteRule = 'index.php?ch_controller=' . $controllerName . '&ch_action=' . $action;

				$rules[] = 'add_rewrite_rule("' . $pathPage . '", "' . $rewriteRule . '", "top");';
			}
		return $rules;
	}

	public function getMatchedRoute(): ?array
	{
		return $this->matchedRoute;
	}

	public function setMatchedRoute(?array $matchedRoute): void
	{
		$this->matchedRoute = $matchedRoute;
	}
}
