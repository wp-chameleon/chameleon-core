<?php

namespace Gaad\Chameleon\Service\Routing\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Routing\RoutesManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class RoutesListParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference RoutesManager
	*/
	private $routesManager;


	public function __construct(
		RoutesManager $routesManager)
	{

			$this->routesManager = $routesManager;


		parent::__construct();
	}


	public function getRoutesManager(): RoutesManager
	{
		return $this->routesManager;
	}



}
