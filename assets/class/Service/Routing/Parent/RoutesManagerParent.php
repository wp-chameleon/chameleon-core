<?php

namespace Gaad\Chameleon\Service\Routing\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Extensions\Parent\ResourceAwareParent;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Gaad\Chameleon\Service\Controller\ControllersManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;
use Symfony\Component\EventDispatcher\EventDispatcher;

class RoutesManagerParent extends ResourceAwareParent
{
	const description = '';


	/**
	 * @ChameleonServiceReference mustache
	 */
	private $mustache;


	/**
	 * @ChameleonServiceReference ControllersManager
	 */
	private $controllersManager;


	/**
	 * @ChameleonServiceReference event_dispatcher
	 */
	private $eventDispatcher;


	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}

	public function __construct(
		MustacheManager    $mustache,
		ControllersManager $controllersManager,
		EventDispatcher    $eventDispatcher)
	{

		$this->mustache = $mustache;
		$this->controllersManager = $controllersManager;
		$this->eventDispatcher = $eventDispatcher;

		parent::__construct();
	}


	public function getMustache(): MustacheManager
	{
		return $this->mustache;
	}


	public function getControllersManager(): ControllersManager
	{
		return $this->controllersManager;
	}


}
