<?php

namespace Gaad\Chameleon\Service\Templating;

use Gaad\Chameleon\Traits\EnvironmentAware;
use Gaad\Chameleon\Traits\InitAware;
use Gaad\Chameleon\Twig\TwigFilesystemLoader;


class TwigManager
{
	use EnvironmentAware;
	use InitAware;

	const description = 'Frontend templates rendering engine';

	private TwigFilesystemLoader $filesystemLoader;
	private \Twig\Environment $twig;

	public function __construct(TwigFilesystemLoader $filesystemLoader)
	{
		$this->filesystemLoader = $filesystemLoader;
	}

	/**
	 * Twig template rendering
	 *
	 * @param string $template
	 * @param array|null $attributes
	 * @return string
	 * @throws \Twig\Error\LoaderError
	 * @throws \Twig\Error\RuntimeError
	 * @throws \Twig\Error\SyntaxError
	 */
	function renderTwigTemplate(string $template, ?array $attributes)
	{
		if (! $this->isInitialized()) $this->init();

		return $this->twig->render($template, $attributes);
	}

	/**
	 * Twig setup options
	 *
	 * @return array
	 */
	private function getEnvironmentOptions(): array
	{
		$options = [];

		if ('dev' !== $this->getEnv())
			$options['cache'] = __CEBOARD_CORE_DIR__ . '/cache/' . $this->getEnv() . '/Twig';

		return $options;
	}

	/**
	 * Twig Environment initialization
	 *
	 * @return void
	 */
	private function onInitComplete(): void
	{
		$this->twig = new \Twig\Environment(
			$this->filesystemLoader->getLoader(),
			$this->getEnvironmentOptions()
		);
		$this->initialized = true;
	}

}
