<?php

namespace Gaad\Chameleon\Service\Templating;

use Mustache_Engine;
use Symfony\Component\Filesystem\Filesystem;


class MustacheManager
{
	const description = 'Simple templates rendering engine';

	private Filesystem $filesystem;
	private Mustache_Engine $engine;

	public function __construct(Filesystem $filesystem)
	{
		$this->filesystem = $filesystem;
		$this->engine = new Mustache_Engine();


		$this->engine->addHelper('case', [
			'lcfirst' => function($value) { return lcfirst((string) $value); },
			'ucfirst' => function($value) { return ucfirst((string) $value); },
			'lower' => function($value) {return strtolower((string) $value); },
			'upper' => function($value) { return strtoupper((string) $value); },
		]);

		$this->engine->addHelper('sanitize', [
			'namespace' => function($value) { return implode('\\', array_filter( explode('\\', (string) $value))); },
			'rmnewline' => function($value) { return str_replace("\n", '', $value); }

		]);
	}

	function render(string $template, ?array $arguments)
	{
		return $this->engine->render($template, $arguments);
	}

}
