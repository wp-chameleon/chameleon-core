<?php

namespace Gaad\Chameleon\Service\Event\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Symfony\Component\Filesystem\Filesystem;
use Gaad\Chameleon\Service\Event\EventSubscribersManager;
use Gaad\Chameleon\Service\Schema\ElementsNames;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class EventSubscriberCreatorParent extends AbstractParent
{
	const description = '';

	
	

	/**
	* @ChameleonServiceReference config
	*/
	private $config;
	

	/**
	* @ChameleonServiceReference mustache
	*/
	private $mustache;
	

	/**
	* @ChameleonServiceReference filesystem
	*/
	private $filesystem;
	

	/**
	* @ChameleonServiceReference eventSubscribersManager
	*/
	private $eventSubscribersManager;
	

	/**
	* @ChameleonServiceReference elementsNames
	*/
	private $elementsNames;
	

	/**
	* @ChameleonServiceReference event_dispatcher
	*/
	private $eventDispatcher;
	

	public function __construct(
		ConfigManager $config, 
		MustacheManager $mustache, 
		Filesystem $filesystem, 
		EventSubscribersManager $eventSubscribersManager, 
		ElementsNames $elementsNames, 
		EventDispatcher $eventDispatcher)
	{
		
			$this->config = $config;
			$this->mustache = $mustache;
			$this->filesystem = $filesystem;
			$this->eventSubscribersManager = $eventSubscribersManager;
			$this->elementsNames = $elementsNames;
			$this->eventDispatcher = $eventDispatcher;
			
	}

	
	public function getConfig(): ConfigManager
	{
		return $this->config;
	}



	public function getMustache(): MustacheManager
	{
		return $this->mustache;
	}



	public function getFilesystem(): Filesystem
	{
		return $this->filesystem;
	}



	public function getEventSubscribersManager(): EventSubscribersManager
	{
		return $this->eventSubscribersManager;
	}



	public function getElementsNames(): ElementsNames
	{
		return $this->elementsNames;
	}



	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}


	
}
