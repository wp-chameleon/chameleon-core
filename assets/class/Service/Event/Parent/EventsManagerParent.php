<?php

namespace Gaad\Chameleon\Service\Event\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Event\EventSubscribersManager;
use Gaad\Chameleon\Service\WP\ActionsManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class EventsManagerParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference eventSubscribersManager
	*/
	private $eventSubscribersManager;




	public function __construct(
		EventSubscribersManager $eventSubscribersManager)
	{

			$this->eventSubscribersManager = $eventSubscribersManager;

	}


	public function getEventSubscribersManager(): EventSubscribersManager
	{
		return $this->eventSubscribersManager;
	}






}
