<?php

namespace Gaad\Chameleon\Service\Event\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Gaad\Chameleon\Service\Schema\ElementsNames;
use Symfony\Component\Filesystem\Filesystem;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class EventSubscribersManagerParent extends AbstractParent
{
	const description = '';


	/**
	 * @ChameleonServiceReference eventDispatcher
	 */
	private $eventDispatcher;


	/**
	 * @ChameleonServiceReference elementsNames
	 */
	private $elementsNames;


	/**
	 * @ChameleonServiceReference filesystem
	 */
	private $filesystem;
	private Processor $processor;


	public function __construct(
		EventDispatcher $eventDispatcher,
		ElementsNames   $elementsNames,
		Filesystem      $filesystem,
		Processor       $processor
	)
	{
		$this->eventDispatcher = $eventDispatcher;
		$this->elementsNames = $elementsNames;
		$this->filesystem = $filesystem;
		$this->processor = $processor;
	}


	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}


	public function getElementsNames(): ElementsNames
	{
		return $this->elementsNames;
	}


	public function getFilesystem(): Filesystem
	{
		return $this->filesystem;
	}


	/**
	 * @return Processor
	 */
	public function getProcessor(): Processor
	{
		return $this->processor;
	}

	/**
	 * @param Processor $processor
	 */
	public function setProcessor(Processor $processor): void
	{
		$this->processor = $processor;
	}

}
