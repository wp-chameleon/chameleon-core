<?php

namespace Gaad\Chameleon\Service\Event;

use Exception;
use Gaad\Chameleon\Config\GeneralConfigurationsManager;
use Gaad\Chameleon\Config\SubscribersConfiguration;
use Gaad\Chameleon\Exception\SubscriberException;
use Gaad\Chameleon\Service\Event\Parent\EventSubscribersManagerParent;
use Gaad\Chameleon\Traits\ConfigResourcesAware;
use Gaad\Chameleon\Traits\InitAware;
use Gaad\Chameleon\Traits\PluginsDirectoryAware;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Yaml\Yaml;


class EventSubscribersManager extends EventSubscribersManagerParent
{
	use InitAware;
	use PluginsDirectoryAware;
	use ConfigResourcesAware;

	protected array $subscribers = [];
	protected string $configField = 'subscribers';
	protected string $resourcesTag = 'EventSubscribers';
	protected string $resourceFileName = 'subscribers.yaml';
	protected string $parametersFileName = 'parameters.yaml';
	protected string $cacheFormat = 'xml';


	/**
	 * Register subscribers in dispatcher
	 *
	 * @throws Exception
	 */
	public function registerSubscribers()
	{
		if (!$this->isInitialized()) $this->handleResource();
		$subscribers = $this->getSubscribers();

		foreach ($subscribers as $subscriberDefinitionsGroup)
			if (!empty($subscriberDefinitionsGroup))
				foreach ($subscriberDefinitionsGroup as $subscriberDefinition)
					$this->registerSubscriber($subscriberDefinition);
	}

	/**
	 * @throws Exception
	 */
	private function registerSubscriber(array $subscriberDefinition)
	{
		$subscriberClass = str_replace('\\\\', '\\', $subscriberDefinition['class']);
		$classExists = class_exists(str_replace('\\\\', '\\', $subscriberClass));
		if (!$classExists)
			throw new Exception('Subscriber class `' . $subscriberClass . '` doesn\'t exists');

		$subscriber = new $subscriberClass();
		$this->getEventDispatcher()->addSubscriber($subscriber);
	}

	public function getSubscribers(): array
	{
		return !empty($this->subscribers) ? $this->subscribers : [];
	}

	public function subscriberExists(string $subscriberName): bool
	{
		$subscribers = $this->getSubscribers();
		foreach ($subscribers as $subscriberDefinitions) {
			if (empty($subscriberDefinitions)) continue;
			foreach ($subscriberDefinitions as $subscriberReferenceName => $subscriberDefinition) {
				$trimmedSubscriberName1 = preg_replace('/Subscriber$/m', '', $subscriberDefinition['name']);
				$trimmedSubscriberName2 = preg_replace('/Subscriber$/m', '', ucfirst($subscriberName));

				if ($trimmedSubscriberName1 === $trimmedSubscriberName2)
					return true;
			}
		}
		return false;
	}

	/**
	 * @param array $subscribers
	 */
	public function setSubscribers(array $subscribers): void
	{
		$this->subscribers = $subscribers;
	}

	public function getCachedCodeDataFilter(array $subscribers): array
	{
		// add / escape to not break json formatting
		foreach ($subscribers as $i => $subscriberGroup)
			foreach ($subscriberGroup as $subscriberReferenceName => $subscriberData) {
				$subscribers[$i][$subscriberReferenceName]['class'] = str_replace('\\', '\\\\', $subscriberData['class']);
				$subscribers[$i][$subscriberReferenceName]['namespace'] = str_replace('\\', '\\\\', $subscriberData['namespace']);
			}

		return $subscribers;
	}

	public function updateSubscribers(array $newSubscriberData)
	{
		$extensionName = $newSubscriberData['extensionName'];
		$subscribersFilePath = $this->fetchSubscribersFilePath($extensionName);
		$index = $this->fetchSubscribersIndexByExtension($extensionName);
		$generateSubscriberReferenceName = $this->getElementsNames()->generateSubscriberReferenceName($newSubscriberData["name"]);

		if (is_int($index))
			$this->subscribers[$index] = $save = array_merge((array)$this->subscribers[$index], [$generateSubscriberReferenceName => $newSubscriberData]);
		else
			$this->subscribers[] = $save = [$generateSubscriberReferenceName => $newSubscriberData];

		$yaml = "---\n" . Yaml::dump($save, 50, 2);
		file_put_contents($subscribersFilePath, $yaml);

		return $this->subscribers;
	}

	private function fetchSubscribersFilePath(string $extensionName): ?string
	{
		foreach ($this->resources as $path)
			if (explode('/', explode('/plugins/', $path)[1])[0] === $extensionName)
				return $path;

		return $this->getPluginsDirectory() . '/' . $extensionName . '/config/' . $this->getResourceFileName();
	}

	function fetchSubscribersFromExtension(string $extensionName): array
	{
		foreach ($this->resources as $i => $path)
			if (explode('/', explode('/plugins/', $path)[1])[0] === $extensionName)
				return null === $this->subscribers[$i] ? [] : $this->subscribers[$i];

		return [];
	}

	function fetchSubscribersIndexByExtension(string $extensionName): ?int
	{
		foreach ($this->resources as $i => $path)
			if (explode('/', explode('/plugins/', $path)[1])[0] === $extensionName)
				return $i;

		return null;
	}

	/**
	 * @return string
	 */
	public function getCacheFormat(): string
	{
		return $this->cacheFormat;
	}

	/**
	 * @return string
	 */
	public function getConfigField(): string
	{
		return $this->configField;
	}

	/**
	 * @return string
	 */
	public function getResourcesTag(): string
	{
		return $this->resourcesTag;
	}

	/**
	 * @return string
	 */
	public function getResourceFileName(): string
	{
		return $this->resourceFileName;
	}

	/**
	 * @return string
	 */
	public function getParametersFileName(): string
	{
		return $this->parametersFileName;
	}

}
