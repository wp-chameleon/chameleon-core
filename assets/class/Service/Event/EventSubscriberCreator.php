<?php

namespace Gaad\Chameleon\Service\Event;

use Gaad\Chameleon\Event\KernelExtensionEvent;
use Gaad\Chameleon\Exception\SubscriberException;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\Service\Event\Parent\EventSubscriberCreatorParent;
use Gaad\Chameleon\Traits\ChameleonExtensionAware;
use Gaad\Chameleon\Traits\InitAware;
use Symfony\Component\Yaml\Yaml;


class EventSubscriberCreator extends EventSubscriberCreatorParent
{

	use InitAware;
	use ChameleonExtensionAware;

	protected array $subscriberData = [];
	protected string $classTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/subscriber';
	protected string $subscriberName = '';
	protected string $subscriberClassName = '';
	protected string $subscriberClassNamespace = '';

	/**
	 * @throws SubscriberException
	 */
	public function removeSubscriber(string $subscriberName, string $extensionName, ?bool $save = false)
	{
		$this->prepareData($subscriberName, $extensionName);

		if (!$this->getEventSubscribersManager()->subscriberExists($subscriberName))
			throw SubscriberException::notFoundError($subscriberName);

		$this->generateSubscriberData($extensionName);

		if ($save) $this->deleteSubscriber();
	}

	/**
	 * @throws SubscriberException
	 */
	public function createSubscriber(string $subscriberName, string $extensionName, ?bool $save = false)
	{
		$this->prepareData($subscriberName, $extensionName);

		if ($this->getEventSubscribersManager()->subscriberExists($subscriberName))
			throw SubscriberException::alreadyExistsError($subscriberName);

		$this->generateSubscriberData($extensionName);

		if ($save) $this->saveSubscriber();
	}

	private function saveSubscriberClass()
	{
		$subscriberData = $this->getSubscriberData();
		$subscriberData['namespace'] = str_replace('\\\\','\\', $subscriberData['namespace']);
		$subscriberData['class'] = str_replace('\\\\','\\', $subscriberData['class']);

		$filePath = $subscriberData["filePath"];
		$filesystem = $this->getFilesystem();
		$filesystem->mkdir(dirname($filePath));
		$filesystem->dumpFile($filePath, $this->generateClassCode());
	}

	private function deleteSubscriberClass()
	{
		$subscriberData = $this->getSubscriberData();
		$filesystem = $this->getFilesystem();
		$filesystem->remove($subscriberData["filePath"]);
	}

	/**
	 * @throws SubscriberException
	 * @throws \Exception
	 */
	public function saveSubscriber()
	{
		$this->saveSubscriberClass();
		$this->saveSubscriberDefinition();

		$event = new KernelExtensionEvent(new ChameleonExtension($this->getChExtensionName()), $this->getSubscriberData());
		$this->getEventDispatcher()->dispatch($event, 'kernel.subscriber.created');
	}

	/**
	 * @throws SubscriberException
	 */
	public function deleteSubscriber(): void
	{
		$this->deleteSubscriberClass();
		$this->deleteSubscriberDefinition();

		$event = new KernelExtensionEvent(new ChameleonExtension($this->getChExtensionName()), $this->getSubscriberData());
		$this->getEventDispatcher()->dispatch($event, 'kernel.subscriber.removed');
	}

	private function prepareData(string $subscriberName, string $chExtensionName)
	{
		$isInitialized = $this->isInitialized();
		$subscriberData = $this->getSubscriberData();

		if (empty($subscriberData))
			$isInitialized = false;

		if (!empty($subscriberData) && $subscriberData['name'] !== $subscriberName)
			$isInitialized = false;

		$subscriberName = !$isInitialized ? $subscriberName : $this->getSubscriberName();

		if (!$isInitialized) {
			$subscriberClassName = $this->getElementsNames()->generateSubscriberClassName($chExtensionName, $subscriberName);
			$subscriberClassNamespace = $this->getElementsNames()->generateSubscriberClassNamespace($chExtensionName);

			$this->init(compact('subscriberName', 'chExtensionName', 'subscriberClassName', 'subscriberClassNamespace'));
		}
	}

	/**
	 * @return array
	 */
	public function getSubscriberData(): array
	{
		return $this->subscriberData;
	}

	/**
	 * @param array $subscriberData
	 */
	public function setSubscriberData(array $subscriberData): void
	{
		$this->subscriberData = $subscriberData;
	}

	/**
	 * @return string
	 */
	public function getSubscriberName(): string
	{
		return $this->subscriberName;
	}

	/**
	 * @param string $subscriberName
	 */
	public function setSubscriberName(string $subscriberName): void
	{
		$this->subscriberName = $subscriberName;
	}

	public function generateClassCode(): string
	{
		$template = file_get_contents($this->getClassTplDir() . '/Subscriber.php.tpl');

		$arguments = array_merge($this->getSubscriberData(), [
			'useSection' => "use Symfony\Contracts\EventDispatcher\Event;"
		]);
		return $this->getMustache()->render($template, $arguments);
	}

	/**
	 * @return string
	 */
	public function getClassTplDir(): string
	{
		return $this->classTplDir;
	}

	/**
	 * @param string $classTplDir
	 */
	public function setClassTplDir(string $classTplDir): void
	{
		$this->classTplDir = $classTplDir;
	}

	/**
	 * @return string
	 */
	public function getFilePath(?string $className = null): string
	{
		$className = null === $className ? $this->getSubscriberName() : $className;
		return $this->getChExtensionDirectory() . '/assets/class/Subscriber/' . $className . '.php';
	}

	/**
	 * @param string $fileName
	 */
	public function setFileName(string $fileName): void
	{
		$this->fileName = $fileName;
	}

	/**
	 * @throws SubscriberException
	 */
	private function saveSubscriberDefinition(): void
	{
		$eventSubscribersManager = $this->getEventSubscribersManager();
		$eventSubscribersManager->setPluginsDirectory($this->getPluginsDirectory());
		$eventSubscribersManager->updateSubscribers($this->getSubscriberData());
	}

	/**
	 * @throws SubscriberException
	 */
	private function deleteSubscriberDefinition(): void
	{
		$filePath = $this->getChExtensionDirectory() . '/config/' . $this->getEventSubscribersManager()->getResourceFileName();
		$definitions = YAML::parse(file_get_contents($filePath));
		$subscriberName = $this->getSubscriberName();

		if (empty($definitions)) {
			throw SubscriberException::notFoundError($subscriberName);
		}

		$definitions = array_filter($definitions, function (array $subscriberDefinition) use ($subscriberName) {
			$trimmedSubscriberName1 = preg_replace('/Subscriber$/m', '', $subscriberDefinition['name']);
			$trimmedSubscriberName2 = preg_replace('/Subscriber$/m', '', ucfirst($subscriberName));

			return $trimmedSubscriberName1 !== $trimmedSubscriberName2;
		});

		$yaml = Yaml::dump($definitions, 50, 2);
		file_put_contents($filePath, $yaml);
	}

	/**
	 * @return string
	 */
	public function getSubscriberClassName(): string
	{
		return $this->subscriberClassName;
	}

	/**
	 * @param string $subscriberClassName
	 */
	public function setSubscriberClassName(string $subscriberClassName): void
	{
		$this->subscriberClassName = $subscriberClassName;
	}

	/**
	 * @return string
	 */
	public function getSubscriberClassNamespace(): string
	{
		return $this->subscriberClassNamespace;
	}

	/**
	 * @param string $subscriberClassNamespace
	 */
	public function setSubscriberClassNamespace(string $subscriberClassNamespace): void
	{
		$this->subscriberClassNamespace = $subscriberClassNamespace;
	}

	private function generateSubscriberData(string $extensionName): void
	{
		$subscriberClassName = $this->subscriberClassName;
		$namespace = str_replace('\\','\\\\', $this->subscriberClassNamespace);
		$this->setSubscriberData([
			'name' => $subscriberClassName,
			'extensionName' => $extensionName,
			'filePath' => $this->getFilePath($subscriberClassName),
			'namespace' => $namespace,
			'class' => $namespace . '\\' . $subscriberClassName
		]);
	}

}
