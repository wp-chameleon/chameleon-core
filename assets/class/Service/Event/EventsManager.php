<?php
/*
 * Manages Kernel, events related initializations.
 * Subscribers, WP actions, WP filters are supported
 * */

namespace Gaad\Chameleon\Service\Event;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Event\Parent\EventsManagerParent;


class EventsManager extends EventsManagerParent
{
	private AppKernel $kernel;

	/**
	 * @throws \Exception
	 */
	public function registerSubscribers()
	{
		$this->getEventSubscribersManager()->registerSubscribers();
	}

	public function registerWPFilters()
	{
	//	$this->getFiltersManager()->registerFilters();
	}

	/**
	 * @return AppKernel
	 */
	public function getKernel(): AppKernel
	{
		return $this->kernel;
	}

	/**
	 * @param AppKernel $kernel
	 */
	public function setKernel(AppKernel $kernel): void
	{
		$this->kernel = $kernel;
	}



}
