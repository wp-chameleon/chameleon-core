<?php
namespace Gaad\Chameleon\Service\Controller;

use Gaad\Chameleon\Service\Controller\Parent\ControllersListParent;



class ControllersList extends ControllersListParent
{
	public function getControllers(): array
	{
		return $this->getControllersManager()->getControllers();
	}
}
