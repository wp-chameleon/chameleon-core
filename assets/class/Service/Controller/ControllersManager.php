<?php

namespace Gaad\Chameleon\Service\Controller;

use Gaad\Chameleon\Event\ControllersRegisteredEvent;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\Service\Controller\Parent\ControllersManagerParent;
use Gaad\Chameleon\Subscriber\KernelEventsSubscriber;


class ControllersManager extends ControllersManagerParent
{

	const description = 'MVC Controllers management';

	protected string $configField = 'controllers';
	protected string $resourcesTag = 'MVCControllers';
	protected string $resourceFileName = 'controllers.yaml';
	protected string $cacheFormat = 'xml';

	public function getControllerView(?string $path = null): string
	{
		return __CEBOARD_CORE_DIR__ . '/assets/view/chameleon-view.php';
	}

	/**
	 * @throws \Exception
	 */
	protected function refreshCachedResource(): void
	{
		$this->registerControllers();
		$this->getEventDispatcher()->dispatch(new ControllersRegisteredEvent(), KernelEventsSubscriber::CONTROLLERS_REGISTERED);
	}

	/**
	 * @throws \Exception
	 */
	function registerControllers()
	{
		$controllers = [];
		$currentExtensionName = $this->fetchExtensionNameFromPath(__DIR__);
		$extensions = apply_filters('Chameleon.Extension.Register', [new ChameleonExtension($currentExtensionName)]);
		array_map(function (ChameleonExtension $extension) use (&$controllers) {
			$extension->setClassManager($this->getPhpClassManager());
			$extension->fetchControllers();
			$controllers[$extension->getName()] = $extension->getControllers();
		}, (array)$extensions);

		$this->setConfigFieldValue($controllers);
	}
}
