<?php

namespace Gaad\Chameleon\Service\Controller\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\ResourceAwareParent;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ControllersManagerParent extends ResourceAwareParent
{
	const description = '';




	/**
	* @ChameleonServiceReference phpClassManager
	*/
	private $phpClassManager;


	/**
	* @ChameleonServiceReference event_dispatcher
	*/
	private $eventDispatcher;


	public function __construct(
		PhpClassManager $phpClassManager,
		EventDispatcher $eventDispatcher)
	{

			$this->phpClassManager = $phpClassManager;
			$this->eventDispatcher = $eventDispatcher;


		parent::__construct();
	}


	public function getPhpClassManager(): PhpClassManager
	{
		return $this->phpClassManager;
	}



	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}



}
