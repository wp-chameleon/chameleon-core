<?php

namespace Gaad\Chameleon\Service\Controller\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Controller\ControllersManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ControllersListParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference ControllersManager
	*/
	private $controllersManager;


	public function __construct(
		ControllersManager $controllersManager)
	{

			$this->controllersManager = $controllersManager;


		parent::__construct();
	}


	public function getControllersManager(): ControllersManager
	{
		return $this->controllersManager;
	}



}
