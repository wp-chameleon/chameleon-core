<?php

namespace Gaad\Chameleon\Service\Schema;

use Gaad\Chameleon\Exception\ClassWriterException;
use Gaad\Chameleon\Interfaces\PhpClassPartInterface;
use Gaad\Chameleon\Service\Schema\Parent\PhpClassWriterParent;


class PhpClassWriter extends PhpClassWriterParent
{
	private string $path;
	protected string $classTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/phpclass';
	private string $classCode;


	public function loadFromFile(string $path)
	{
		$this->path = $path;
		$phpClassManager = $this->getClassManager();
		$phpClassManager->loadFromFile($path);
		$this->setInitialized($phpClassManager->isInitialized());
	}

	/**
	 * @throws ClassWriterException
	 */
	public function saveToFile(string $path)
	{
		$this->prepareDirectoryBeforeSave($path);

		file_put_contents($path, $this->getClassCode());
	}

	/**
	 * @return string
	 */
	public function getPath(): string
	{
		return $this->path;
	}

	/**
	 * @param string $path
	 */
	public function setPath(string $path): void
	{
		$this->path = $path;
	}

	public function extractClassCodeParts(): array
	{
		$code = [];
		$classParts = $this->getClassManager()->fetchClassParts();

		/** @var PhpClassPartInterface $handler */
		foreach ($classParts as $partName => $handler) {
			if (is_string($handler))
				$code[$partName] = $handler;

			if (is_array($handler)) {
				if ($handler[0] instanceof PhpClassPartInterface) {
					$classPartCode = [];
					array_map(function (PhpClassPartInterface $classPart) use (&$classPartCode) {
						$classPartCodeRaw = $classPart->getCode();
						$classPartCodeRaw[] = "\n";

						$classPartCode[$classPart->getName()] = !empty($classPartCode[$classPart->getName()]) ?
							array_merge($classPartCode[$classPart->getName()], $classPartCodeRaw)
							: $classPartCodeRaw;
					}, $handler);

					$code[$partName] = $classPartCode;
				}
				if (is_string($handler[0]))
					$code[$partName] = $handler;
			}

			if ($handler instanceof PhpClassPartInterface)
				$code[$partName] = $handler->getCode();
		}

		return $code;
	}

	public function prepareClassCode(): void
	{
		$template = file_get_contents($this->getClassTplDir() . '/Class.php.tpl');
		$classCodeParts = array_map(function ($code) {
			if(is_array($code) && is_array($code[array_key_first($code)]))
				$code = array_map(function ($code) { return implode('', $code); }, $code);

			return is_array($code) ? implode('', $code) : $code;
		}, $this->extractClassCodeParts());
		$classCode = $this->getMustache()->render($template, $classCodeParts);
		$classCode = htmlspecialchars_decode($classCode);
		$classCode = preg_replace('/\n\n/m', "\n", $classCode);

		$this->setClassCode($classCode);
	}

	public function getClassConstantsCode(): string
	{
		$code = [];
		$constants = $this->getClassManager()->getConstants();
		foreach ($constants as $name => $value) {
			$const = 'const ' . $name . ' = ';

			if (is_numeric($value)) {
				$const .= $value . ';';
			} elseif (is_string($value)) {
				$const .= '\'' . $value . '\';';
			}

			if (is_array($value)) {
				$isAssoc = 0 === array_keys($value);
				$const .= '[' . "\n";
				foreach ($value as $k => $v) {
					if ($isAssoc) $const .= $k . ' => ';
					$const .= '\'' . $v . '\',';
				}
				$const .= '];';
			}

			$code[] = $const;
		}


		return implode("\n", $code);
	}

	public function getClassPropertiesCode(): string
	{
		$code = [];
		$methods = $this->getClassManager()->getProperties();
		array_map(function (array $propertyDetails) use (&$code) {
			$code[] = $propertyDetails['docBlock'] . "\n" . $propertyDetails['code'];
		}, $methods);

		return implode("\n", $code);
	}

	public function getClassMethodsCode(): string
	{
		$code = [];
		$methods = $this->getClassManager()->getMethods();
		array_map(function (array $methodDetails) use (&$code) {
			$code[] = $methodDetails['docBlock'] . "\n" . $methodDetails['code'];
		}, $methods);

		return implode("\n", $code);
	}

	public function getClassTplDir(): string
	{
		return $this->classTplDir;
	}

	/**
	 * @return string
	 */
	public function getClassCode(): string
	{
		return $this->classCode;
	}

	/**
	 * @param string $classCode
	 */
	public function setClassCode(string $classCode): void
	{
		$this->classCode = $classCode;
	}

	public function prepareDirectoryBeforeSave(string $path): void
	{
		if (!$this->isInitialized())
			throw ClassWriterException::notInitialized();

		$filesystem = $this->getFilesystem();
		$this->prepareClassCode();

		$filesystem->mkdir(dirname($path));
	}


}
