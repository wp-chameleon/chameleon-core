<?php

namespace Gaad\Chameleon\Service\Schema;

use Doctrine\Common\Annotations\AnnotationReader;
use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Interfaces\PhpClassPartInterface;
use Gaad\Chameleon\Schema\Php\ClassHeaderClassPart;
use Gaad\Chameleon\Schema\Php\ConstantClassPart;
use Gaad\Chameleon\Schema\Php\MethodClassPart;
use Gaad\Chameleon\Schema\Php\PhpClassPart;
use Gaad\Chameleon\Schema\Php\PropertyClassPart;
use Gaad\Chameleon\Service\Schema\Parent\PhpClassManagerParent;

use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;
use SplFileInfo;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @property array $methods
 * @property array $properties
 * @property array $use
 * @property array $const
 * @property array $classHeader
 */
class PhpClassManager extends PhpClassManagerParent
{
	const description = 'PHP class editor and builder';

	private ReflectionClass $ref;
	public string $namespace;
	public array $use = [];
	public ClassHeaderClassPart $classHeader;
	public array $classUse = [];
	public array $const = [];
	public array $properties = [];
	public array $methods = [];
	private string $path = '';

	/**
	 * @throws ClassException
	 */
	function loadFromFile(string $path)
	{
		$this->setInitialized(false);
		$this->path = $path;
		$classNamespace = $this->getClassNamespaceFromPath($path);

		if (!class_exists($classNamespace))
			throw ClassException::notExists($classNamespace);

		$this->setRef(new ReflectionClass($classNamespace));
		$this->findNamespace();
		$this->findUseSection();
		$this->findClassUseSection();
		$this->findConstants();
		$this->findClassHeader();
		$this->findMethods();
		$this->findProperties();

		$this->setInitialized(true);
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassPartException
	 */
	public function addMethod(MethodClassPart $method): void
	{
		$this->getPhpClassManipulator()->addMethod($this, $method);
		$this->methods[] = $method;
	}

	/**
	 * @throws \Gaad\Chameleon\Exception\ClassPartException
	 */
	public function removeMethod(string $name): void
	{
		$this->getPhpClassManipulator()->removeMethod($this, $name);
		$this->methods = array_filter($this->methods, function (PhpClassPartInterface $method) use ($name) {
			return $name !== $method->getName();
		});
	}

	private function normalizeForRegExp(string $searchString): string
	{
		$from = ['/', '*', '[', ']', '	', '$'];
		$to = ['\/', '\*', '\[', '\]', '\t', '\$'];

		return str_replace($from, $to, $searchString);
	}

	function fetchClassParts(): array
	{
		return [
			'namespace' => $this->getNamespace(),
			'useSection' => $this->getUse(),
			'classHeader' => $this->getClassHeader(),
			'classUseSection' => $this->getClassUse(),
			'constSection' => $this->getConstants(),
			'propertiesSection' => $this->getProperties(),
			'methodSection' => $this->getMethods()
		];
	}

	/**
	 * Check if given line is inside DocBlock
	 *
	 * @param int|null $lineNumber
	 * @return bool
	 */
	function isLineInsideDocBlockDocBlock(int $lineNumber = null): bool
	{
		$beginD = $this->findNearestSubstring('/**', $lineNumber);
		$endD = $this->findNearestSubstring('*/', $lineNumber);
		$beginU = $this->findNearestSubstring('/**', $lineNumber, false);
		$endU = $this->findNearestSubstring('*/', $lineNumber, false);

		//no DocBlock below line and end of Dock block before line
		if ($beginD === null && $endD === null && $lineNumber > $endU) return false;

		if (null === $endU && null === $beginU) return false;

		$outside =
			$lineNumber > $beginU && $lineNumber > $endU
			&& $beginD > $lineNumber && $endD > $lineNumber
			&& $beginD <= $endD
			&& $beginU <= $endU;

		return !$outside;
	}

	/**
	 * Returns an array with begin and end line of the nearest DocBlock element in given direction
	 *
	 * @param int|null $lineNumber
	 * @param bool|null $directionDown
	 * @return array|null
	 */
	function findNearestDocBlockPosition(int $lineNumber = null, ?bool $directionDown = true): ?array
	{
		$directionDown = null === $directionDown ? true : $directionDown;
		$insideDocBlock = $this->isLineInsideDocBlockDocBlock($lineNumber);

		if ($insideDocBlock && $directionDown) {
			$begin = $this->findNearestSubstring('/**', $lineNumber, false);
			$end = $this->findNearestSubstring('*/', $lineNumber);
		}
		if ($insideDocBlock && !$directionDown) {
			$begin = $this->findNearestSubstring('/**', $lineNumber, false);
			$end = $this->findNearestSubstring('*/', $lineNumber);
		}

		if (!$insideDocBlock && $directionDown) {
			$begin = $this->findNearestSubstring('/**', $lineNumber);
			$end = $this->findNearestSubstring('*/', $lineNumber);
		}

		if (!$insideDocBlock && !$directionDown) {
			$begin = $this->findNearestSubstring('/**', $lineNumber, false);
			$end = $this->findNearestSubstring('*/', $lineNumber, false);
		}

		return compact('begin', 'end');
	}

	/**
	 * Returns line number if given patter is found in class file.
	 * Search can be done in both directions.
	 *
	 * @param string $searchString
	 * @param int|null $lineNumber
	 * @param bool|null $directionDown
	 * @return int|null
	 */
	function findNearestSubstring(string $searchString, ?int $lineNumber = null, ?bool $directionDown = true): ?int
	{
		$matched = $this->findAllLinesWithSubstring($searchString, $lineNumber, $directionDown);

		return !empty($matched) ? $matched[0] : null;
	}


	function findAllLinesWithSubstring(string $searchString, ?int $lineNumber = null, ?bool $directionDown = true): array
	{
		$directionDown = $directionDown ?? false;
		$matchedLineNumbers = [];
		$file = $this->getClassFileArray();

		if (count($file) < $lineNumber) return []; //escape

		$max = count($file);
		$i = $lineNumber - 1 >= 0 ? $lineNumber - 1 : 0;
		$indexChanger = $directionDown ? $this->getIncrementer($max) : $this->getDecrementer($max);
		$searchString = $this->normalizeForRegExp($searchString);

		if (preg_match('/' . $searchString . '/m', $file[$lineNumber - 1])) {
			$matchedLineNumbers[] = $i + 1;
		} else {
			while ($i > -1 && $i <= $max) {
				if (preg_match('/' . $searchString . '/m', $file[$i])) {
					$matchedLineNumbers[] = $i + 1;
				}
				$indexChanger($i);
			}
		}

		return $matchedLineNumbers;
	}


	/**
	 * Returns incrementing mechanism closure for used in class file walkers
	 *
	 * @param int $max
	 * @return \Closure
	 */
	protected function getIncrementer(int $max): \Closure
	{
		return function (int &$i) use ($max) {
			return $i <= $max ? $i++ : $max;
		};
	}

	/**
	 * Returns decrementing mechanism closure for used in class file walkers
	 *
	 * @param int $max
	 * @return \Closure
	 */
	protected function getDecrementer(int $max): \Closure
	{
		return function (int &$i) use ($max) {
			return $i >= 0 ? $i-- : 0;
		};
	}

	/**
	 * Returns an array of loaded class contents.
	 * Parameters is an array contains lines numbers.
	 *
	 * @param array $useMatchedLines
	 * @return array
	 */
	public function fetchMultipleLinesContents(array $useMatchedLines): array
	{
		return array_map(function (int $lineNumber) {
			return $this->getLineContents($lineNumber);
		}, array_reverse($useMatchedLines));
	}

	public function findNearestSemiColon(int $lineNumber, ?bool $directionDown = true): ?int
	{
		return $this->findNearestSubstring(';', $lineNumber, $directionDown);
	}

	/**
	 * Returns a range of lines of loaded class
	 *
	 * @param int $from
	 * @param int|null $to
	 * @return array
	 */
	public function getClassSlice(int $from, ?int $to = null): array
	{
		$to = !empty($to) ? $to : $this->getMaxLInes();
		$length = $to - $from + 1 < 0 ? 1 : $to - $from + 1;
		$from -= 1;

		$array_slice = array_slice($this->getClassFileArray(), $from, $length);
		return $array_slice;
	}

	/**
	 * Fetch named constant definition
	 *
	 * @param string $name
	 * @return array|int[]|null
	 * @throws ClassException
	 */
	function getConstantDetails(string $name): ?PhpClassPartInterface
	{
		$details = null;
		$constMatchedLines = $this->findElementsAfterLinePattern('const ' . $name, 'class');
		if (!empty($constMatchedLines)) {
			$constMatchedLines = array_shift($constMatchedLines);
			$details = new ConstantClassPart($this, ['begin' => $constMatchedLines]);
		}

		return $details;
	}

	/**
	 * Fetch all class constants definitions
	 *
	 * @return void
	 */
	private function findConstants(): void
	{
		$this->setConst(array_map(function (string $name) {
			return $this->getConstantDetails($name);
		}, array_keys($this->getRef()->getConstants())));
	}

	/**
	 * Iterate trough class methods and fetch either one wanted or all annotations
	 *
	 * @param string|null $type
	 * @return array|mixed|object[]|null
	 */
	function getMethodsAnnotations(?string $type = null)
	{
		$annotations = [];
		array_map(function (ReflectionMethod $method) use (&$annotations, $type) {
			$annotationReader = new AnnotationReader();
			$methodAnnotations = $annotationReader->getMethodAnnotations($method);

			if ($type)
				$methodAnnotations = array_filter($methodAnnotations, function ($annotation) use ($type) {
					return get_class($annotation) === $type;
				});

			$annotations[$method->getname()] = 1 === count($methodAnnotations) ? $methodAnnotations[0] : $methodAnnotations;
		}, $this->getRef()->getMethods());

		return $annotations;
	}


	static function getClassNamespaceFromPath($item): string
	{
		$info = new SplFileInfo($item);
		$namespaceBase = !strstr($item, '/tests/') ? 'Gaad\Chameleon' : 'Gaad\ChameleonTesting';

		return $namespaceBase . str_replace(['.' . $info->getExtension(), '/'], ['', '\\'], explode('class', $item)[1]);
	}

	/**
	 * Locates and saves in classHeader property the loaded class declaration
	 *
	 * @throws ClassException
	 */
	function findClassHeader(): void
	{

		$begin = $this->findNearestSubstring('class ', 0);
		$end = $this->findNearestSubstring('{', $begin) - 1;

		$this->getClassSlice($begin, $end);

		$this->setClassHeader(new ClassHeaderClassPart($this, compact('begin', 'end')));
	}

	/**
	 * Finds all methods that belong to loaded class file.
	 *
	 * @throws ClassException
	 */
	private function findMethods()
	{
		$methods = [];
		$reflectionMethods = $this->getRef()->getMethods();
		foreach ($reflectionMethods as $method) {
			$name = $method->getName();
			$isPartOfFile = $this->IsPartOfFile('function ' . $name);
			if ($isPartOfFile)
				$methods[] = new MethodClassPart($this, [
					'begin' => $method->getStartLine(),
					'end' => $method->getEndLine()
				]);
		}

		$this->setMethods($methods);
	}

	/**
	 * Locates all loaded class properties and saves it in $properties property.
	 *
	 * @throws ClassException
	 */
	private function findProperties()
	{
		$properties_ = [];
		/** @var ReflectionProperty $property */
		$properties = $this->getRef()->getProperties();
		foreach ($properties as $property) {
			list($file, $i, $matches) = $this->propertyPartOfFile($property);
			if ($matches) {
				$propertyLine = $this->findAllLinesWithSubstring($matches[0][0])[0];
				$properties_[] = new PropertyClassPart($this, [
					'begin' => $propertyLine
				]);
			}
		}

		$this->setProperties($properties_);
	}

	/**
	 * Finds all elements that match the given element pattern and occurs after given start line patter in both directions.
	 *
	 * @param string $elementPattern
	 * @param string $searchOriginLine
	 * @param bool|null $directionDown
	 * @return array
	 */
	function findElementsAfterLinePattern(string $elementPattern, string $searchOriginLine, ?bool $directionDown = true): array
	{
		//first find the search origin line
		$searchOriginLineNumber = $directionDown ?
			$this->findNearestSubstring($searchOriginLine, 0)
			: $this->findNearestSubstring($searchOriginLine, $this->getMaxLInes(), false);

		/*
		 * no search origin, no search start.
		 * Search from begin or end in opposite direction can return unexpected matches
		 */
		if (empty($searchOriginLineNumber)) return [];

		return $this->findAllLinesWithSubstring($elementPattern, $searchOriginLineNumber, $directionDown);
	}

	/**
	 * Returns the contents of the given line
	 *
	 * @param int $lineNumber
	 * @return mixed|string
	 */
	function getLineContents(int $lineNumber)
	{
		$lineNumber--; // contents array is indexed from 0
		if ($lineNumber < 0 || $lineNumber > $this->getMaxLInes()) return '';

		return $this->getClassFileArray()[$lineNumber];
	}

	/**
	 * Setup use section od the loaded class
	 *
	 * @return void
	 */
	function findUseSection()
	{
		$useMatchedLines = $this->findElementsAfterLinePattern('use', 'class', false);
		$this->setUse($this->fetchMultipleLinesContents($useMatchedLines));
	}

	/**
	 * Setup use section inside the loaded class
	 *
	 * @return void
	 */
	function findClassUseSection()
	{
		$useMatchedLines = $this->findElementsAfterLinePattern('use', 'class');
		$this->setClassUse($this->fetchMultipleLinesContents($useMatchedLines));
	}

	private function findNamespace(): void
	{
		$this->namespace = $this->getRef()->getNamespaceName();
	}

	public function methodExists(string $name): bool
	{
		return !empty(array_values(array_filter($this->getMethods(), function (PhpClassPartInterface $classPart) use ($name) {
			return $classPart->getName() == $name;
		})));
	}

	/**
	 * Adds use section record
	 *
	 * @param string $useLine
	 * @return void
	 */
	public function addUseSectionElement(string $useLine)
    {
		if(empty($useLine)) return;

		$use = $this->getUse();
		$useLine = str_replace( ["\n", ';'],'', trim($useLine)) . ";\n";
		$useKeywordExists = strstr($useLine, 'use ');

		if (!$useKeywordExists)  $useLine = 'use '. $useLine;
		if(!in_array($useLine, $use)) $use[] = $useLine;

		$this->setUse($use);
    }

    private function getLineIndex(string $anchorLine): int
	{
		$i = -1;
		$file = $this->getClassFileArray();
		foreach ($file as $i => $lineContents)
			if ($anchorLine === $lineContents) return $i;

		return $i;
	}

	/**
	 * Returns loaded class code as array
	 *
	 * @return array|false
	 */
	function getClassFileArray()
	{
		return file($this->getRef()->getFileName());
	}

	/**
	 * Generates property access lever part of the decration from given ReflectionProperty
	 *
	 * @param ReflectionProperty $property
	 * @return string
	 */
	private function getPropertyAccessLevel(ReflectionProperty $property): string
	{
		$accessLevel = '';
		$accessLevel .= $property->isPrivate() ? 'private ' : '';
		$accessLevel .= $property->isProtected() ? 'protected ' : '';
		$accessLevel .= $property->isPublic() ? 'public ' : '';
		$accessLevel .= $property->isStatic() ? 'static ' : '';

		return $accessLevel;
	}

	/**
	 * Checks if given string is physically a part of loaded file
	 * Used for checking elements exists in loaded class, not its parents.
	 *
	 * @param string $searchString
	 * @return bool
	 */
	function isPartOfFile(string $searchString): bool
	{
		$file = $this->getClassFileArray();
		foreach ($file as $i => $lineContents) {
			$matches = [];
			preg_match_all('/' . $this->normalizeForRegExp($searchString) . '/m', $lineContents, $matches, PREG_SET_ORDER, 0);
			if ($matches) return true;
		}

		return false;
	}

	/**
	 * Checks if given property is physically a part of loaded file and not inherited one.
	 *
	 * @param ReflectionProperty $property
	 * @return array
	 */
	private function propertyPartOfFile(ReflectionProperty $property): array
	{
		$file = $this->getClassFileArray();
		$accessLevel = $this->getPropertyAccessLevel($property);
		$type = $this->getPropertyType($property);
		$name = '\$' . $property->getName() . ' ';
		$re = '/' . $accessLevel . $type . $name . '/m';

		foreach ($file as $i => $lineContents) {
			$matches = [];
			preg_match_all($re, $lineContents, $matches, PREG_SET_ORDER, 0);
			if ($matches) break;
		}

		return array($file, $i, $matches);
	}

	/**
	 * @return string
	 */
	public function getPath(): string
	{
		return $this->path;
	}

	/**
	 * @param string $path
	 */
	public function setPath(string $path): void
	{
		$this->path = $path;
	}

	public function getPropertyType(ReflectionProperty $property): string
	{
		$reflectionNamedType = $property->getType();
		$type = !empty($reflectionNamedType) ? $reflectionNamedType->getName() . ' ' : '';

		return $type;
	}

	private function getMaxLInes(): int
	{
		return count($this->getClassFileArray());
	}

	/**
	 * @return ReflectionClass
	 */
	public function getRef(): ReflectionClass
	{
		return $this->ref;
	}

	/**
	 * @param ReflectionClass $ref
	 */
	public function setRef(ReflectionClass $ref): void
	{
		$this->ref = $ref;
	}

	/**
	 * @return array
	 */
	public function getClassUse(): array
	{
		return $this->classUse;
	}

	/**
	 * @param array $classUse
	 */
	public function setClassUse(array $classUse): void
	{
		$this->classUse = $classUse;
	}

	public function getMethods(): array
	{
		return $this->methods;
	}

	public function setMethods(array $methods): void
	{
		$this->methods = $methods;
	}


	public function getProperties(): array
	{
		return $this->properties;
	}

	public function setProperties(array $properties): void
	{
		$this->properties = $properties;
	}


	public function getUse(): array
	{
		return $this->use;
	}

	public function setUse(array $use): void
	{
		$this->use = $use;
	}


	public function getConstants(): array
	{
		return $this->const;
	}


	public function setConst(array $constants): void
	{
		$this->const = $constants;
	}


	public function getNamespace(): string
	{
		return $this->namespace;
	}

	public function setNamespace(string $namespace): void
	{
		$this->namespace = $namespace;
	}

	public function getClassHeader(): ClassHeaderClassPart
	{
		return $this->classHeader;
	}

	public function setClassHeader(ClassHeaderClassPart $classHeader): void
	{
		$this->classHeader = $classHeader;
	}

	function getConstant(string $name): ?PhpClassPartInterface
	{
		$constant = array_values(array_filter($this->getConstants(), function (PhpClassPartInterface $const) use ($name) {
			return $const->getName() == $name;
		}));

		return !empty($constant) && $constant[0] instanceof PhpClassPartInterface ? $constant[0] : null;
	}

}
