<?php

namespace Gaad\Chameleon\Service\Schema;

use Gaad\Chameleon\Service\Schema\Parent\ElementsNamesParent;


class ElementsNames extends ElementsNamesParent
{
	public function generateSubscriberClassName(string $extensionName, string $subscriberName): string
	{
		return ucfirst($this->camelize($subscriberName) . 'Subscriber');
	}

	public function generateSubscriberClassNamespace(string $extensionName, ?string $suffix = ''): string
	{
		$subscriberNamingRules = $this->getConfig()->getResource('naming.Subscriber');
		$namespaceBase = !empty($subscriberNamingRules['namespace']) ? $subscriberNamingRules['namespace'] : 'Gaad\\Chameleon\\Subscriber';

		return $namespaceBase . (string)$suffix;
	}

	public function generateSubscriberReferenceName(string $subscriberName): string
	{
		return $this->dotize($subscriberName);
	}

	static function camelize(string $input, ?array $separators = []): string
	{
		$defaultSeparators = ['.', '_', '-'];
		array_map(function (string $separator) use (&$input) {
			$input = str_replace($separator, '', ucwords($input, $separator));
		}, array_merge($defaultSeparators, (array)$separators));

		return lcfirst($input);
	}

	static function dotize(string $input, ?string $separator = '.'): string
	{
		$separator = $separator ? $separator : '.';
		return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1' . $separator, $input));
	}

}
