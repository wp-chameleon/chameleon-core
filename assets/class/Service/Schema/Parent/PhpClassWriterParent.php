<?php

namespace Gaad\Chameleon\Service\Schema\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\InitAwareParent;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Symfony\Component\Filesystem\Filesystem;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class PhpClassWriterParent extends InitAwareParent
{
	const description = 'Php class file manipulations';



	/**
	* @ChameleonServiceReference phpClassManager
	*/
	private $classManager;


	/**
	* @ChameleonServiceReference filesystem
	*/
	private $filesystem;


	/**
	* @ChameleonServiceReference mustache
	*/
	private $mustache;


	public function __construct(
		PhpClassManager $classManager,
		Filesystem $filesystem,
		MustacheManager $mustache)
	{

			$this->classManager = $classManager;
			$this->filesystem = $filesystem;
			$this->mustache = $mustache;


		parent::__construct();
	}


	public function getClassManager(): PhpClassManager
	{
		return $this->classManager;
	}



	public function getFilesystem(): Filesystem
	{
		return $this->filesystem;
	}



	public function getMustache(): MustacheManager
	{
		return $this->mustache;
	}



}
