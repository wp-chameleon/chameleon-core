<?php

namespace Gaad\Chameleon\Service\Schema\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Schema\PhpClassManipulator;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;


class PhpClassManagerParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference PhpClassManipulator
	*/
	private $phpClassManipulator;


	public function __construct(
		PhpClassManipulator $phpClassManipulator)
	{

			$this->phpClassManipulator = $phpClassManipulator;


		parent::__construct();
	}


	public function getPhpClassManipulator(): PhpClassManipulator
	{
		return $this->phpClassManipulator;
	}



}
