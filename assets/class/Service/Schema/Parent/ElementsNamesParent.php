<?php

namespace Gaad\Chameleon\Service\Schema\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ElementsNamesParent extends AbstractParent
{
	const description = '';

	
	

	/**
	* @ChameleonServiceReference config
	*/
	private $config;
	

	/**
	* @ChameleonServiceReference eventDispatcher
	*/
	private $eventDispatcher;
	

	public function __construct(
		ConfigManager $config, 
		EventDispatcher $eventDispatcher)
	{
		
			$this->config = $config;
			$this->eventDispatcher = $eventDispatcher;
			
	}

	
	public function getConfig(): ConfigManager
	{
		return $this->config;
	}



	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}


	
}
