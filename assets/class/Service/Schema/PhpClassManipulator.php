<?php
namespace Gaad\Chameleon\Service\Schema;

use Gaad\Chameleon\Exception\ClassPartException;
use Gaad\Chameleon\Schema\Php\MethodClassPart;
use Gaad\Chameleon\Service\Schema\Parent\PhpClassManipulatorParent;



class PhpClassManipulator extends PhpClassManipulatorParent
{
	/**
	 * @throws ClassPartException
	 */
	public function addMethod(PhpClassManager $classManager, MethodClassPart $method): bool
	{
		$name = $method->getName();
		if($classManager->methodExists($name))
			throw (new ClassPartException())->classMethodExists($name);


		return true;
	}

	/**
	 * @throws ClassPartException
	 */
	public function removeMethod(PhpClassManager $classManager, string $name): bool
	{
		if(!$classManager->methodExists($name))
			throw (new ClassPartException())->classMethodDoNotExists($name);


		return true;
	}
}
