<?php

namespace Gaad\Chameleon\Service\WP;

require_once __CEBOARD_CORE_DIR__ . '/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/AnnotationReader.php';

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Service\WP\Parent\ModsManagerParent;
use Gaad\Chameleon\Traits\ConfigResourcesAware;
use Symfony\Component\Config\ConfigCache;


class ModsManager extends ModsManagerParent
{
	const description = 'Wordpress hooks modifications manager';

	protected string $configField = 'wpMods';
	protected string $resourcesTag = 'WPModifications';
	protected string $resourceFileName = 'wp-modifications.yaml';
	protected string $parametersFileName = 'parameters.yaml';
	protected string $cacheFormat = 'xml';

	private array $hookedMethods = [];

	use ConfigResourcesAware {
		getCacheContentString as getCacheString;
		loadCache as loadCache;
	}

	/**
	 * Rebuild routing mechanism cache procedure
	 *
	 * @throws \Exception
	 */
	protected function refreshCachedResource(): void
	{
		$this->locateFiles();
		$this->fetchConfigFiles();
		$this->setInitialized(true);
		$this->applyPlaceholdersValues();
		$this->saveCache();
		$this->saveModsCache();
	}


	protected function loadCache(): bool
	{
		$cached = parent::loadCache();

		$file = str_replace($this->getCacheFormat(), 'php', $this->getCacheFilePath());
		$cacheExists = is_readable($file);
		if ($cacheExists) {
			require_once $file;
			//@TODO here loading other partial resources should be triggered when not in production mode

		}

		return $cached;
	}


	private function createCacheClass(string $content, string $use): string
	{
		$args = [
			'name' => $this->getResourcesTag(),
			'constructorContent' => $content,
			'useSection' => $use,
		];
		$extDir = str_replace('//', '/', __CEBOARD_CORE_DIR__ . '/assets/tpl/cache/');
		$template = file_get_contents($extDir . 'WPModificationsCache.php.tpl');

		return $this->getMustache()->render($template, $args);
	}

	protected function saveModsCache(): void
	{
		$use = [];
		$code = array_map(function ($hooked) use (&$use) {
			$applierClassNamespace = str_replace('\\\\', '\\', $hooked['method'][0]);
			$alias = in_array($applierClassNamespace, array_keys($use)) ? $use[$applierClassNamespace][1] : 'HookApplier' . uniqid();

			if (!in_array($applierClassNamespace, array_keys($use)))
				$use[$applierClassNamespace] = [$applierClassNamespace, $alias];

			$code = $hooked['applier'] . '(\"' . $hooked['hook'] . '\", [ $' . $alias . 'Obj, \"' . $hooked['method'][1] . '\"], ' . $hooked['priority'] . ', ' . $hooked['args'] . ' );';
			return str_replace('\"', '"', $code);
		}, $this->hookedMethods);


		$useSection = array_map(function (array $alias) {
			return 'use ' . $alias[0] . ' as '. $alias[1] .';';
		}, $use);

		$objects = array_map(function (array $alias) {
			return '$'. $alias[1] . 'Obj = new ' . $alias[1] . '();';
		}, $use);

		$objects = "\n\t\t" . implode("\n\t\t", $objects);
		$content =
			$objects
			. "\n\n\t\t" . implode("\n\t\t", $code);

		$cacheClass = $this->createCacheClass($content, implode("\n", array_unique($useSection)));

		$file = str_replace($this->getCacheFormat(), 'php', $this->getCacheFilePath());
		$cache = new ConfigCache($file, true);
		$cache->write($cacheClass, []);
	}


	public function beforeInitComplete(bool $initialized)
	{
		if (!$this->isInitialized()) $this->fetchHookedMethod();
	}

	/**
	 * @throws ClassException
	 */
	private function fetchHookedMethod()
	{
		foreach ($this->getWpMods() as $extensionReferenceName => $mods)
			foreach ((array)$mods as $modReferenceName => $modDetails) {
				$className = $modDetails['class'];
				if (!class_exists($className))
					throw (new ClassException())->notExists($className);

				$modificationApplier = new $className();
				$this->hookedMethods = array_merge($this->hookedMethods, $modificationApplier->getMethods());
			}
	}

	/**
	 * @return mixed
	 */
	public function getHookedMethods()
	{
		return $this->hookedMethods;
	}

	/**
	 * @param mixed $hookedMethods
	 */
	public function setHookedMethods($hookedMethods): void
	{
		$this->hookedMethods = $hookedMethods;
	}

}


