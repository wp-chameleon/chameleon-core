<?php

namespace Gaad\Chameleon\Service\WP\Modifications;

use Gaad\Chameleon\Service\WP\Modifications\Parent\ModsListParent;


class ModsList extends ModsListParent
{
	public function getMods()
	{
		return $this->getModsManager()->getWpMods();
	}
}
