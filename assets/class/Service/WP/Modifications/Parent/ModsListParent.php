<?php

namespace Gaad\Chameleon\Service\WP\Modifications\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\WP\ModsManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ModsListParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference ModsManager
	*/
	private $modsManager;


	public function __construct(
		ModsManager $modsManager)
	{

			$this->modsManager = $modsManager;


		parent::__construct();
	}


	public function getModsManager(): ModsManager
	{
		return $this->modsManager;
	}

}
