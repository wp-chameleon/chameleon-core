<?php

namespace Gaad\Chameleon\Service\WP\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\ResourceAwareParent;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ModsManagerParent extends ResourceAwareParent
{
	const description = '';


	/**
	* @ChameleonServiceReference mustache
	*/
	private MustacheManager $mustache;


	public function __construct(
		MustacheManager $mustache)
	{
		$this->mustache = $mustache;
		parent::__construct();


	}






	public function getMustache(): MustacheManager
	{
		return $this->mustache;
	}

	public function setMustache(): MustacheManager
	{
		return $this->mustache;
	}



}
