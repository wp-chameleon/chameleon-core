<?php

namespace Gaad\Chameleon\Service\WP\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\WP\ModsManager;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ModCreatorParent extends AbstractParent
{
	const description = '';

	
	

	/**
	* @ChameleonServiceReference modsManager
	*/
	private $modsManager;
	

	/**
	* @ChameleonServiceReference ClassManager
	*/
	private $classManager;
	

	public function __construct(
		ModsManager $modsManager, 
		PhpClassManager $classManager)
	{
		
			$this->modsManager = $modsManager;
			$this->classManager = $classManager;
			

		parent::__construct();
	}

	
	public function getModsManager(): ModsManager
	{
		return $this->modsManager;
	}



	public function getClassManager(): PhpClassManager
	{
		return $this->classManager;
	}


	
}
