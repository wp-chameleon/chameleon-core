<?php
namespace Gaad\Chameleon\Service\WP;

use Gaad\Chameleon\Service\WP\Parent\ModCreatorParent;



class ModCreator extends ModCreatorParent
{
	const description = 'Wordpress modifications classes creator';

}
