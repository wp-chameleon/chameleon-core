<?php

namespace Gaad\Chameleon\Service\Extensions;

use Doctrine\DBAL\Exception;
use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Event\KernelServiceEvent;
use Gaad\Chameleon\Event\KernelExtensionEvent;
use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\Service\Extensions\Parent\ExtensionServiceCreatorParent;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Symfony\Component\Yaml\Yaml;

class ExtensionServiceCreator extends ExtensionServiceCreatorParent
{
	const description = 'Chameleon extensions services generator';

	private string $serviceClassDir = '';
	private string $serviceName = '';
	private string $extensionName = '';
	private string $servicesPath = '';
	private string $extDir = '';
	private ?array $args;
	private array $dependencies = [];
	private string $serviceNamespace;
	private array $serviceDefinition = [];
	private array $namespaceBlacklist = ['and', 'or'];
	private bool $serviceNameOverridden = false;
	private string $serviceNameOverride = '';
	private string $parentClassBase = 'AbstractParent';


	/**
	 * @throws \Exception
	 */
	function removeService(string $serviceName, string $extensionName, ?array $args = [])
	{
		global $AppKernel;

		$this->prepareData([
			'extensionName' => $extensionName,
			'serviceName' => $serviceName,
			'args' => $args
		]);

		if (!$this->serviceExists())
			throw new \Exception('The `' . $this->serviceName . '` service do not exists under `' . $this->extensionName . '` extension.');

		$this->removeServiceClass();
		$this->removeServiceDefinition();
		$AppKernel->clearCache();

		$event = new KernelExtensionEvent(new ChameleonExtension($extensionName), $this->getServiceDefinition());
		$this->getEventDispatcher()->dispatch($event, 'kernel.service.removed');
	}

	/**
	 * @throws \Exception
	 */
	function createService(string $serviceName, string $extensionName, ?array $args = [])
	{
		$this->prepareData([
			'extensionName' => $extensionName,
			'serviceName' => $serviceName,
			'args' => $args
		]);
		$this->createServiceValidation();
		$this->createServiceDefinition();
		$this->createServiceParentClass();
		$this->createServiceClass();
		$this->appendServiceDefinition();

		$event = new KernelExtensionEvent(new ChameleonExtension($extensionName), $this->getServiceDefinition());
		$this->getEventDispatcher()->dispatch($event, 'kernel.service.created');
	}

	public function extensionExists(): bool
	{
		return $this->getFilesystem()->exists($this->extDir);
	}

	/**
	 * @throws Exception
	 */
	public function serviceExists(): bool
	{
		global $AppKernel;

		$serviceName = $this->getServiceReferenceDotizedName();
		$serviceExists = $AppKernel->serviceExists($serviceName);

		$serviceClassPath = $this->getServiceClassPath();
		$serviceClassExists = is_readable($serviceClassPath);
		if (!$serviceExists && $serviceClassExists)
			throw new Exception('Service definition do not exist but there is a class that seems to be matching the name. Operation can\'t continue automatically.'
				. "\n" . 'class found: ' . $serviceClassPath);

		return $serviceExists && $serviceClassExists;
	}

	private function createServiceParentClass()
	{
		$argsDefaults = [
			'name' => $this->serviceName,
			'parentClassBaseName' => $this->getParentClassBaseClassName()
		];

		$serviceNamespace = $this->getServiceNamespace();
		$argsDefaults['serviceNamespace'] = str_replace('\\\\', '\\', 'Gaad\Chameleon\Service\\' . $serviceNamespace);
		$argsDefaults['serviceNamespace'] = implode('\\', array_filter(explode('\\', $argsDefaults['serviceNamespace'])));

		$extDir = str_replace('//', '/', __CEBOARD_CORE_DIR__ . '/assets/tpl/service/');
		$fs = $this->getFilesystem();
		$fs->mkdir($this->serviceClassDir);
		$argsDefaults = $this->fetchParentClassDetails($argsDefaults);

		$template = file_get_contents($extDir . 'ServiceParent.php.tpl');
		$content = $this->getMustache()->render($template, array_merge($argsDefaults, $this->args));
		$this->getFilesystem()->dumpFile($this->getServiceParentClassPath(), $content);
	}

	private function createServiceClass()
	{

		$serviceNamespace = str_replace('\\\\', '\\', 'Gaad\Chameleon\Service\\' . $this->getServiceNamespace());

		$argsDefaults = [
			'name' => $this->serviceName,
			'serviceNamespace' => rtrim($serviceNamespace, '\\')
		];

		$extDir = str_replace('//', '/', __CEBOARD_CORE_DIR__ . '/assets/tpl/service/');
		$fs = $this->getFilesystem();
		$fs->mkdir($this->serviceClassDir);

		$template = file_get_contents($extDir . 'Service.php.tpl');
		$content = $this->getMustache()->render($template, array_merge($argsDefaults, $this->args));
		$this->getFilesystem()->dumpFile($this->getServiceClassPath(), $content);
	}

	/**
	 * @return string
	 */
	private function getServiceClassPath(): string
	{
		global /** @var AppKernel $AppKernel */
		$AppKernel;
		$rawServiceName = $this->dotize($this->serviceName);
		$serviceName = strtolower($this->getExtensionName()) . '.' . $rawServiceName;
		try {
			$service = $AppKernel->getService($serviceName);
		} catch (\Exception $e) {
		}

		if ($service) {
			$classFile = str_replace('//', '/', $this->serviceClassDir . str_replace('\\', '/', explode('Service', get_class($service))[1]) . '.php');
			if (is_file($classFile)) return $classFile;
		}

		$str = $this->serviceClassDir . '/' . $this->serviceName . '.php';
		return $str;
	}

	private function getServiceParentClassPath(): string
	{
		$serviceClassPath = $this->getServiceClassPath();
		$basename = basename(str_replace('.php', 'Parent.php', $serviceClassPath));

		return dirname($serviceClassPath) . '/Parent/' . $basename;
	}

	private function createServiceDefinition(): void
	{
		$definition = [];
		$serviceName = $this->getServiceReferenceDotizedName();
		$serviceReferenceNamePrv = $this->getServiceReferencePrvDotizedName();
		$serviceNamespace = $this->getServiceNamespace();
		$namespacedService = $this->getPhpNamespaceString($serviceNamespace);

		$prvData = [
			'class' => $namespacedService
		];

		$pubData = [
			'alias' => $serviceReferenceNamePrv,
			'public' => true
		];

		$definition[$serviceName] = $pubData;
		$definition[$serviceReferenceNamePrv] = $prvData;
		$definition[$namespacedService] = '@' . $serviceName;

		$this->serviceDefinition = $definition;
	}

	public function getServiceName(): string
	{
		return $this->serviceName;
	}

	public function setServiceName(string $serviceName): void
	{
		$serviceName = $this->getServiceClassNameFromDotizedName($serviceName);

		$rawServiceName = $this->extractRawServiceName($serviceName);
		$this->serviceName = $this->sanitizeServiceName($rawServiceName);
	}

	/**
	 * @return string
	 */
	public function getExtensionName(): string
	{
		return $this->extensionName;
	}

	/**
	 * @param string $extensionName
	 */
	public function setExtensionName(string $extensionName): void
	{
		$this->extensionName = $extensionName;
	}

	private function appendServiceDefinition(?array $serviceDefinition = null)
	{
		$servicesDefinitions = Yaml::parse(file_get_contents($this->servicesPath));
		$serviceDefinition1 = !empty($serviceDefinition) ? $serviceDefinition : $this->getServiceDefinition();
		$servicesDefinitions['services'] = array_merge($servicesDefinitions['services'], $serviceDefinition1);
		$dump = "---\n\n" . Yaml::dump($servicesDefinitions, 50);
		file_put_contents($this->servicesPath, $dump);
	}

	/**
	 * @throws Exception
	 */
	private function setupDependencies(): void
	{
		global $AppKernel;

		if (!isset($this->args['dependencies']))
			return;

		$dependencies_ = [];
		$dependencies = is_string($this->args['dependencies']) ? explode(',', $this->args['dependencies']) : $this->args['dependencies'];
		array_map(function (string $serviceName) use ($AppKernel, &$dependencies_) {
			$serviceExists = $AppKernel->serviceExists($serviceName);
			if ($serviceExists) $dependencies_[$serviceName] = $AppKernel->getService($serviceName);
			else throw new Exception("Service " . $serviceName . " doesn't exists.");
		}, array_filter($dependencies));

		$this->setDependencies($dependencies_);
	}

	private function camelize(string $serviceName): string
	{
		$parts = explode('.', $serviceName);
		$parts_ = array_map(function (string $part) use (&$parts) {
			return ucfirst($part);
		}, $parts);

		return 1 === count($parts) ? $parts[0] : lcfirst(implode('', $parts_));
	}

	private function dotize(string $serviceName): string
	{
		$str = strtolower(preg_replace('/(?<!^)[A-Z]/', '.$0', $serviceName));
		$arr = explode('.', $str);
		$arr2 = array_map(function ($a) {
			return sanitize_key($a);
		}, $arr);
		return implode('.', $arr2);
	}

	private function generateClassGetter($dependency, $serviceName): string
	{
		list($depNamespaced, $depClass, $depFieldName, $depArgumentName) = $this->getDependencyImplementationDetails($dependency, $serviceName);
		$code = ["\t", 'public function get' . ucfirst($depFieldName) . '(): ' . $depClass, "\t{"];
		$code [] = "\t" . 'return $this->' . $depFieldName . ';';
		$code [] = "\t" . '}';

		return implode("\n", $code);
	}

	private function generateClassSetter($dependency, $serviceName): string
	{
		list($depNamespaced, $depClass, $depFieldName, $depArgumentName) = $this->getDependencyImplementationDetails($dependency, $serviceName);
		$code = ["\t", 'public function set' . ucfirst($depFieldName) . '(' . $depClass . ' $' . $depFieldName . '): void', "\t{"];
		$code [] = "\t" . '$this->' . $depFieldName . '= $' . $depFieldName . ';';
		$code [] = "\t" . '}';
		return implode("\n", $code);
	}

	/**
	 * @param $dependency
	 * @param $serviceName
	 * @return array
	 */
	private function getDependencyImplementationDetails($dependency, $serviceName): array
	{
		$depNamespaced = $this->getServiceNamespace() . get_class($dependency);
		$depClass = basename(str_replace('\\', '/', $depNamespaced));
		$depFieldName = lcfirst($this->camelize($serviceName));
		$depArgumentName = "$" . $depClass;
		return array($depNamespaced, $depClass, $depFieldName, $depArgumentName);
	}

	private function sanitizeServiceName(string $serviceName): string
	{
		$str = strtolower(preg_replace('/(?<!^)[A-Z]/', '.$0', $serviceName));
		$arr = explode('.', $str);
		$arr2 = array_map(function ($a) {
			return sanitize_key($a);
		}, $arr);
		$converted = array_map(function (string $item) {
			return ucfirst($item);
		}, $arr2);

		return implode("", $converted);
	}

	/**
	 * @param string $extensionName
	 * @param string $serviceName
	 * @param array|null $args
	 * @return void
	 * @throws Exception
	 */
	public function prepareData(array $args = []): void
	{
		extract($args);
		$this->setArgs($args);
		$this->setExtensionName($extensionName);
		$this->setParentClassBase($args['parentClassBase']);
		$extractNamespace = $this->extractNamespace($serviceName);

		if ($this->isDotizedName($serviceName)) {
			$rawServiceName = $this->extractRawServiceName($serviceName);
			$fromDotizedName = $this->getServiceClassNameFromDotizedName($serviceName);
			$str = str_replace('Gaad\Chameleon\Service', '', $fromDotizedName);
			$str = implode('\\', array_filter(explode('\\', str_replace($rawServiceName, '', $str))));

			$serviceName = $str . '\\' . $rawServiceName;
		}
		$extractNamespace = $this->extractNamespace($serviceName);
		$this->setServiceNamespace($extractNamespace);
		$this->setServiceName($serviceName);
		$this->setExtDir();
		$this->setServicesPath();
		$this->setServiceClassDir();
		$this->setupDependencies();
	}

	private function removeServiceClass(): void
	{
		unlink($this->getServiceClassPath());
	}

	private function removeServiceDefinition(): bool
	{
		$serviceName = $this->extensionName . '.' . $this->dotize($this->serviceName);
		$serviceName = $this->getServiceReferenceDotizedName();
		$servicesDefinitions = Yaml::parse(file_get_contents($this->servicesPath));
		foreach ($servicesDefinitions['services'] as $serviceName_ => $definition) {
			if ($serviceName === $serviceName_)
				$unset = $serviceName_;

			if ($this->getServiceReferencePrvDotizedName() === $serviceName_)
				$unset = $serviceName_;

			if ($definition === '@' . $serviceName)
				$unset = $serviceName_;

			if (isset($unset))
				unset($servicesDefinitions['services'][$unset]);

			unset($unset);
		}

		$dump = "---\n\n" . Yaml::dump($servicesDefinitions, 50);

		return (bool)file_put_contents($this->servicesPath, $dump);
	}

	/**
	 * @throws Exception
	 */
	private function extractNamespace(string $serviceName): string
	{
		$serviceName = $this->getServiceClassNameFromDotizedName($serviceName);

		$serviceNameParts = explode('\\', $serviceName);
		array_pop($serviceNameParts);
		return implode('\\', array_filter($serviceNameParts));
	}

	/**
	 * @return string
	 */
	public function getServiceNamespace(): string
	{
		return implode('\\', array_filter(explode("\\\\", $this->serviceNamespace)));
	}

	/**
	 * @param string $serviceNamespace
	 * @throws Exception
	 */
	public function setServiceNamespace(string $serviceNamespace): void
	{
		$this->validateNamespace($serviceNamespace);
		$this->serviceNamespace = $serviceNamespace;
	}

	/**
	 * @return string
	 */
	private function getServiceClassDir(): string
	{
		return __CEBOARD_CORE_DIR__ . '/../' . $this->extensionName . '/assets/class/Service' . '/' . $this->getServiceNamespaceDir();
	}

	/**
	 */
	private function setServiceClassDir(?string $serviceClassDir = null): void
	{
		$this->serviceClassDir = $serviceClassDir ?? __CEBOARD_CORE_DIR__ . '/../' . $this->extensionName . '/assets/class/Service' . '/' . $this->getServiceNamespaceDir();
	}

	private function getServiceNamespaceDir()
	{
		return str_replace('\\', '/', $this->getServiceNamespace());
	}

	/**
	 * @return string
	 */
	private function getServiceReferenceDotizedName(): string
	{
		return $this->isServiceNameOverridden() ? $this->getServiceNameOverride() : $this->generateServiceDotizedName();
	}

	/**
	 * @return array
	 */
	public function getServiceDefinition(): array
	{
		return $this->serviceDefinition;
	}

	/**
	 * @param array $serviceDefinition
	 */
	public function setServiceDefinition(array $serviceDefinition): void
	{
		$this->serviceDefinition = $serviceDefinition;
	}

	/**
	 * @throws Exception
	 */
	private function validateNamespace(string $serviceNamespace)
	{
		$serviceNameParts = explode('\\', strtolower($serviceNamespace));
		$namespaceBlacklist = $this->getNamespaceBlacklist();
		$diff = array_diff($namespaceBlacklist, $serviceNameParts);

		//if some namespace parts will not be different, then some restricted words were used
		if (count($namespaceBlacklist) !== count($diff))
			throw new Exception('Namespace `' . $serviceNamespace . '`is invalid. Restricted word was used. Restricted words: ' . implode(', ', $namespaceBlacklist));
	}

	/**
	 * @return array|string[]
	 */
	public function getNamespaceBlacklist(): array
	{
		return $this->namespaceBlacklist;
	}

	/**
	 * @param array|string[] $namespaceBlacklist
	 */
	public function setNamespaceBlacklist(array $namespaceBlacklist): void
	{
		$this->namespaceBlacklist = $namespaceBlacklist;
	}

	/**
	 * @param string $serviceName
	 * @return string
	 */
	private function extractRawServiceName(string $serviceName): string
	{
		if ($this->isDotizedName($serviceName))
			$serviceName = $this->getServiceClassNameFromDotizedName($serviceName);

		$serviceNameParts = explode('\\', $serviceName);
		return array_pop($serviceNameParts);
	}

	/**
	 * @param string $extDir
	 */
	public function setExtDir(?string $extDir = null): void
	{
		$this->extDir = $extDir ?? __CEBOARD_CORE_DIR__ . '/../' . $this->extensionName;
	}

	/**
	 * @return string
	 */
	public function getServicesPath(): string
	{
		return $this->servicesPath;
	}

	/**
	 * @param string|null $servicePath
	 */
	public function setServicesPath(?string $servicesPath = null): void
	{
		$this->servicesPath = $servicesPath ?? __CEBOARD_CORE_DIR__ . '/../' . $this->extensionName . '/config/services.yaml';
	}

	/**
	 * @return array|null
	 */
	public function getArgs(): ?array
	{
		return $this->args;
	}

	/**
	 * @param array|null $args
	 */
	public function setArgs(?array $args): void
	{
		$this->args = $args;

		$this->setServiceNameOverride((string)$this->args['serviceNameOverride']);
	}

	/**
	 * @return array
	 */
	public function getDependencies(): array
	{
		return $this->dependencies;
	}

	/**
	 * @param array $dependencies
	 */
	public function setDependencies(array $dependencies): void
	{
		$this->dependencies = $dependencies;
	}

	/**
	 * @return void
	 * @throws \Exception
	 */
	private function createServiceValidation(): void
	{
		if (!$this->extensionExists())
			throw new \Exception('The extension `' . $this->extensionName . '` do not exists.');

		if ($this->serviceExists())
			throw new \Exception('The `' . $this->serviceName . '` service already exists under `' . $this->extensionName . '` extension.');
	}

	/**
	 * @return bool
	 */
	public function isServiceNameOverridden(): bool
	{
		return $this->serviceNameOverridden;
	}

	/**
	 * @param bool $serviceNameOverridden
	 */
	public function setServiceNameOverridden(bool $serviceNameOverridden): void
	{
		$this->serviceNameOverridden = $serviceNameOverridden;
	}

	/**
	 * @return string
	 */
	public function getServiceNameOverride(): string
	{
		return $this->serviceNameOverride;
	}

	/**
	 * @param string $serviceNameToOverrideWith
	 * @throws \Exception
	 */
	public function setServiceNameOverride(string $serviceNameToOverrideWith): void
	{
		global $AppKernel;

		if (empty($serviceNameToOverrideWith)) {
			$this->serviceNameOverridden = false;
			$this->serviceNameOverride = '';
			return;
		}

		if ($AppKernel->serviceExists($serviceNameToOverrideWith))
			throw new \Exception('Service `' . $serviceNameToOverrideWith . '` already exists. Use different `serviceNameOverride` parameter value.');

		$this->serviceNameOverridden = true;
		$this->serviceNameOverride = $serviceNameToOverrideWith;
	}

	private function getServiceReferencePrvDotizedName()
	{
		return $this->generateServiceDotizedName(null, 'prv');
	}

	/**
	 * @return array|string|string[]
	 */
	private function generateServiceDotizedName(?string $prefix = '', ?string $suffix = '')
	{
		$serviceNamespace = !empty($this->getServiceNamespace()) ? $this->getServiceNamespace() . '\\' : '';
		$serviceReferenceNameNamespacePart = !empty($this->dotize($serviceNamespace)) ? '.' . $this->dotize($serviceNamespace) . '.' : '.';

		$prefixStr = !empty((string)$prefix) ? (string)$prefix . '.' : '';
		$suffixStr = !empty((string)$suffix) ? '.' . (string)$suffix : '';
		$serviceBasename = $prefixStr . $this->dotize($this->serviceName) . $suffixStr;
		$str = $serviceReferenceNameNamespacePart . $serviceBasename;
		$serviceName = strtolower($this->getExtensionName()) . str_replace(strtolower($this->getExtensionName()), '', $str);

		return str_replace('..', '.', $serviceName);
	}

	/**
	 * @param string $serviceNamespace
	 * @return array|string|string[]
	 */
	private function getPhpNamespaceString(string $serviceNamespace)
	{
		return str_replace('\\\\', '\\', 'Gaad\Chameleon\Service\\' . $serviceNamespace . '\\' . $this->getServiceName());
	}

	/**
	 * @param string $serviceName
	 * @param AppKernel $AppKernel
	 * @return string
	 */
	private function getServiceClassNameFromDotizedName(string $serviceName): string
	{
		global $AppKernel;

		if ($this->isDotizedName($serviceName)) {
			if ($AppKernel->serviceExists($serviceName)) {
				$service = $AppKernel->getService($serviceName);
				$serviceName = get_class($service);
			}
		}
		return $serviceName;
	}

	/**
	 * @param string $serviceName
	 * @return bool
	 */
	private function isDotizedName(string $serviceName): bool
	{
		return !empty(explode('.', $serviceName));
	}

	private function fetchParentClassDetails(array $argsDefaults): array
	{
		$dependencies = $this->getDependencies();
		$use = [];
		$constructorArgs = [];
		$classFields = [];
		$classFieldsInit = [];
		$classFieldsGetters = [];
		$classFieldsSetters = [];

		if (!empty($dependencies)) {
			foreach ($dependencies as $serviceName => $dependency) {
				list($depNamespaced, $depClass, $depFieldName, $depArgumentName) = $this->getDependencyImplementationDetails($dependency, $serviceName);

				$use[] = "use " . $depNamespaced . ";";
				$use[] = "use Gaad\Chameleon\Annotation\ChameleonServiceReference;";
				$constructorArgs[] = $depClass . " " . $depArgumentName;

				$classFields[] = implode("\n",
					[
						"/**",
						"* @ChameleonServiceReference {$serviceName}",
						"*/",
						"private " . $depClass . " $" . $depFieldName . ";"
					]
				);
				$classFieldsInit[] = "\t" . '$this->' . $depFieldName . " = " . $depArgumentName . ';';
				$classFieldsGetters[] = $this->generateClassGetter($dependency, $serviceName);
				$classFieldsSetters[] = $this->generateClassSetter($dependency, $serviceName);
			}
		}

		$parentClassBase = $this->getParentClassBase();
		if('AbstractParent' !== $parentClassBase)
			$use [] = 'use ' . $parentClassBase . ';';

		return array_merge($argsDefaults, [
			'useSection' => implode("\n", $use,),
			'constructorArgsSection' => implode(", ", $constructorArgs),
			'classFields' => implode("\n ", $classFields),
			'classFieldsInit' => implode("\n ", $classFieldsInit),
			'classFieldsGetters' => implode("\n ", $classFieldsGetters),
			//'classFieldsSetters' => implode("\n ", $classFieldsSetters),
		]);
	}

	/**
	 * @return string
	 */
	public function getParentClassBase(): string
	{
		return $this->parentClassBase;
	}

	/**
	 * @param string $parentClassBase
	 * @throws ClassNotFoundException
	 * @throws ClassException
	 */
	public function setParentClassBase(?string $parentClassBase): void
	{
		if (empty($parentClassBase)) return;

		if (!class_exists($parentClassBase)) {
			$parentClassStdNamespace = 'Gaad\Chameleon\Service\Extensions\Parent\\' . $parentClassBase;
			if (!class_exists($parentClassStdNamespace))
				throw ClassException::notExists($parentClassBase);

			$parentClassBase = $parentClassStdNamespace;
		}

		$this->parentClassBase = (string)$parentClassBase;
	}

	private function getParentClassBaseClassName()
	{
		return array_reverse(explode("\\", $this->getParentClassBase()))[0];
	}
}
