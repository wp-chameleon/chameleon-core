<?php

namespace Gaad\Chameleon\Service\Extensions;

use Exception;
use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionServiceList
{
	private AppKernel $kernel;
	private Filesystem $filesystem;
	private MustacheManager $mustache;
	private ConfigManager $configManager;

	public function __construct(Filesystem $filesystem, MustacheManager $mustache, ConfigManager $configManager)
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;

		$this->kernel = $AppKernel;
		$this->filesystem = $filesystem;
		$this->mustache = $mustache;
		$this->configManager = $configManager;
	}

	public function getAllServices(): array
	{

		$services = [];
		if (!$this->kernel instanceof AppKernel) return $services;
		$serviceIdsList = $this->kernel->getServiceIdsList();

		//building services list from extensions
		$extensionList = $this->kernel->getExtensionList();

		/** @var ChameleonExtension $extension */
		foreach ($extensionList as $extension) {
			$extName = $extension->getName();
			$extensionServicesDefinitions = $extension->getServicesDefinitions();

			$services[$extName] = $extensionServicesDefinitions;
		}

		foreach ($services as $extName => $services_) {
			foreach ($services_ as $name => $item) {
				if (!in_array($name, $serviceIdsList)) continue;
				$services_[$name]['name'] = $name;
				try {
					$services_[$name]['class'] = get_class($this->kernel->getService($name));
				} catch( Exception $e ) {
					//avoiding Wordpress 500 error handler output
					if ('cli' === php_sapi_name()) {
						echo $e->getMessage() . "\n";
						die();
					}
				}

				$services [$extName] = $services_;
			}
		}

		return $services;
	}

	public function getFilesystem(): Filesystem
	{
		return $this->filesystem;
	}

	public function getMustache(): MustacheManager
	{
		return $this->mustache;
	}

	public function getConfigManager(): ConfigManager
	{
		return $this->configManager;
	}

	/**
	 * @return AppKernel
	 */
	public function getKernel(): AppKernel
	{
		return $this->kernel;
	}

	/**
	 * @param AppKernel $kernel
	 */
	public function setKernel(AppKernel $kernel): void
	{
		$this->kernel = $kernel;
	}

}
