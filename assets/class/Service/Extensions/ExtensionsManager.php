<?php

namespace Gaad\Chameleon\Service\Extensions;

use Exception;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Gaad\Chameleon\Traits\InitAware;
use Gaad\Chameleon\Traits\PluginsDirectoryAware;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionsManager
{
	const description = 'Chameleon extensions generator';
	use InitAware;
	use PluginsDirectoryAware;

	private Filesystem $filesystem;
	private string $extDir = '';
	private string $extName = '';
	private MustacheManager $mustache;
	private ConfigManager $configManager;
	private array $extDefinitionDefaults = ['version' => '1.0.0'];

	public function __construct(Filesystem $filesystem, MustacheManager $mustache, ConfigManager $configManager)
	{
		$this->filesystem = $filesystem;
		$this->mustache = $mustache;
		$this->configManager = $configManager;
	}

	private function removeSubmodule(?string $extName = ''): void
	{
		if (!$this->isInitialized()) $this->prepareData($extName);


		$gitModulesFilePath = '/var/www/html/.gitmodules';
		if (is_readable($gitModulesFilePath))

			$gitModulesFileArrayClean = array_filter(file($gitModulesFilePath), function (string $item) {
				$a = !strstr($item, '/' . $this->extName . '.git');
				$b = !strstr($item, '/' . $this->extName . '"]');
				$c = !strstr($item, 'path = wp-content/plugins/' . $this->extName);

				return $a && $b && $c;
			});

		$gitmodules = implode("", (array)$gitModulesFileArrayClean);
		file_put_contents($gitModulesFilePath, $gitmodules);
	}


	function createExtension(string $name, ?bool $force)
	{
		$name = $this->sanitizeName($name);

		if (!$this->isInitialized()) $this->prepareData($name);

		if ($force) $this->removeExtension($name);

		if (!$this->extensionExists())
			shell_exec('wp plugin scaffold ' . $name);

		if ($this->extensionExists()) {
			$this->createAssets();
			$this->createTemplates();
			$this->createConfig();
			$this->createMain();
			$this->composer();
			$this->setupTesting();
			$this->createChameleonFile();

			shell_exec('wp plugin activate ' . $name);
		}
	}

	public function extensionExists(): bool
	{
		$extDir = !empty($this->getPluginsDirectory()) ? $this->getPluginsDirectory() .'/' .basename($this->extDir) : $this->extDir;
		return $this->filesystem->exists($extDir);
	}

	private function createAssets()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/';
		$fs = $this->filesystem;

		$fs->mkdir($this->extDir . '/assets');

		$tplPath = $extDir . '/bootstrap.php.tpl';
		$targetFilePath = $this->extDir . '/assets/bootstrap.php';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . '/.gitignore.tpl';
		$targetFilePath = $this->extDir . '/.gitignore';
		$this->createExtensionFile($tplPath, $targetFilePath);
	}

	private function createMain()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/';

		$tplPath = $extDir . '/extension.php.tpl';
		$targetFilePath = $this->extDir . '/' . $this->extName . '.php';
		$this->createExtensionFile($tplPath, $targetFilePath);
	}

	private function createTemplates()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/';
		$fs = $this->filesystem;

		$fs->mkdir($this->extDir . '/templates');

		$tplPath = $extDir . 'template-example.html.twig.tpl';
		$targetFilePath = $this->extDir . '/templates/template.html.twig';
		$this->createExtensionFile($tplPath, $targetFilePath);
	}

	private function createConfig()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/';
		$fs = $this->filesystem;
		$fs->mkdir($this->extDir . '/assets/class/Config');
		$fs->mkdir($this->extDir . '/config');

		$tplPath = $extDir . 'Configuration.php.tpl';
		$targetFilePath = $this->extDir . '/assets/class/Config/' . ucfirst($this->extName) . 'Configuration.php';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . 'config.yaml.tpl';
		$targetFilePath = $this->extDir . '/config/config.yaml';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . 'parameters.yaml.tpl';
		$targetFilePath = $this->extDir . '/config/parameters.yaml';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . 'services.yaml.tpl';
		$targetFilePath = $this->extDir . '/config/services.yaml';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . 'wp-modifications.yaml.tpl';
		$targetFilePath = $this->extDir . '/config/wp-modifications.yaml';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . 'routes.yaml.tpl';
		$targetFilePath = $this->extDir . '/config/routes.yaml';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . 'controllers.yaml.tpl';
		$targetFilePath = $this->extDir . '/config/controllers.yaml';
		$this->createExtensionFile($tplPath, $targetFilePath);

		$tplPath = $extDir . '/composer.json.tpl';
		$targetFilePath = $this->extDir . '/composer.json';
		$this->createExtensionFile($tplPath, $targetFilePath);
	}

	function removeExtension(string $name)
	{
		if (!$this->isInitialized()) $this->prepareData($name);
		$pathToSubmodule = 'wp-content/plugins/' . $this->extName;
		shell_exec('wp plugin deactivate ' . $name);
		shell_exec('cd /var/www/html && git rm --cached ' . $pathToSubmodule);
		$this->filesystem->remove('/var/www/html/.git/modules/' . $this->extName);
		$this->filesystem->remove($this->extDir);
		shell_exec('composer dump-autoload -o --working-dir=/var/www/html/wp-content/plugins/chameleon-core');
		$this->removeSubmodule();
	}

	public function getAllExtensions(): array
	{
		return array_map(function (array $extDetails) use (&$extensions) {
			return $extDetails['definition'];
		}, $this->getExtensionsDetails());
	}

	public function isExtensionInstalled(string $name, ?string $version = '*'): bool
	{
		$extensionsDetails = $this->getExtensionsDetails($name);
		if (empty($extensionsDetails)) return false;

		return '*' !== $version ? $this->isExtensionVersionSatisfiable($extensionsDetails['definition'], $version) : !empty($extensionsDetails);
	}

	public function getExtensionsDetails(?string $name = null): array
	{
		$extensions = [];
		array_map(function (string $pluginPath) use (&$extensions) {
			$chameleonDefinition = glob($pluginPath . '/chameleon.json');
			if (!empty($chameleonDefinition))
				try {
					$name = basename(dirname($chameleonDefinition[0]));
					$chameleonFileContents = file_get_contents($chameleonDefinition[0]);
					$extensions[$name] = [
						'name' => $name,
						'path' => $chameleonDefinition[0],
						'definition' => $this->parseExtensionDefinition(json_decode($chameleonFileContents, true))
					];
				} catch (Exception $e) {
				}
		}, glob(WP_PLUGIN_DIR . '/*'));

		if (!empty($name))
			$filteredByName = array_filter($extensions, function ($item) use ($name) {
				return $name === $item['name'];
			});

		return (array)(empty($name) ? $extensions : array_shift($filteredByName));
	}

	/**
	 * @return void
	 */
	private function createExtensionFile(string $tplPath, string $targetFilePath, ?array $args = []): void
	{
		$template = file_get_contents($tplPath);
		$content = $this->mustache->render($template, array_merge(['name' => $this->extName], $args));
		$this->filesystem->dumpFile($targetFilePath, $content);
	}

	private function composer()
	{
		ob_start();
		system('composer install -o --working-dir=' . $this->extDir, $res);
		ob_clean();
	}

	private function setupTesting()
	{
		$this->composerPhpUnit();
		$this->updatePhpUnitBoostrap();
		$this->updateGitignore();
		$this->createPhpUnitXmlFile();
		$this->createBeforeTestingScriptsShellFile();
		$this->createSamplePhpUnitTests();
	}

	/**
	 * @param string $name
	 * @return void
	 */
	public function prepareData(string $name): void
	{
		$this->setExtName($name);
		$this->extDir = __CEBOARD_CORE_DIR__ . '/../' . $this->extName;

		$this->setInitialized(true);
	}

	private function createChameleonFile()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/root/';
		$tplPath = $extDir . 'chameleon.json.tpl';
		$targetFilePath = $this->extDir . '/chameleon.json';
		$this->createExtensionFile($tplPath, $targetFilePath, ['version' => '1.0.0']);
	}

	private function createPhpUnitXmlFile()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/tests/';
		$tplPath = $extDir . 'phpunit.xml.tpl';
		$targetFilePath = $this->extDir . '/phpunit.xml';
		$this->createExtensionFile($tplPath, $targetFilePath, []);
	}

	private function createBeforeTestingScriptsShellFile()
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/tests/';
		$tplPath = $extDir . 'copy-elements.sh.tpl';
		$targetFilePath = $this->extDir . '/bin/copy-elements.sh';
		$this->createExtensionFile($tplPath, $targetFilePath, []);
	}

	private function parseExtensionDefinition(array $definition = []): array
	{
		return wp_parse_args($definition, $this->getExtensionDefinitionDefaults());
	}

	private function getExtensionDefinitionDefaults()
	{
		return $this->extDefinitionDefaults;
	}

	public function isExtensionVersionSatisfiable(array $extensionsDetails, ?string $requiredVersion = '*'): bool
	{
		if ('*' === $requiredVersion) return true;

		$currentVersion = $extensionsDetails['version'];
		return 0 <= version_compare($currentVersion, $requiredVersion);
	}

	public function runExtensionHealthyCheck(string $name, ?string $version = '*'): array
	{
		$isExtensionInstalled = $this->isExtensionInstalled($name);
		$isExtensionVersionSatisfiable = $isExtensionInstalled
			&& $this->isExtensionVersionSatisfiable($this->getExtensionsDetails($name)['definition'], $version);

		return [
			'isExtensionInstalled' => $isExtensionInstalled,
			'isExtensionVersionSatisfiable' => $isExtensionVersionSatisfiable
		];
	}

	static function isAllTrue(array $checks): bool
	{
		foreach ($checks as $test)
			if (false === $test) return false;

		return true;
	}

	public function isExtensionHealthy(string $name, ?string $version = '*'): bool
	{
		return self::isAllTrue($this->runExtensionHealthyCheck($name, $version));
	}

	/**
	 * @return string
	 */
	public function getExtName(): string
	{
		return $this->extName;
	}

	/**
	 * @param string $extName
	 */
	public function setExtName(string $extName): void
	{
		$this->extName = $extName;
	}

	function camelize($input, $separator = '_')
	{
		return str_replace($separator, '', ucwords($input, $separator));
	}

	/**
	 * @param string $extName
	 */
	public function sanitizeName(string $extName): string
	{
		return $this->camelize($extName);
	}

	private function composerPhpUnit()
	{
		ob_start();
		system('composer require --dev yoast/phpunit-polyfills --working-dir=' . $this->extDir, $res);
		ob_clean();
	}

	private function updatePhpUnitBoostrap(): void
	{
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/tests/';
		$tplPath = $extDir . 'bootstrap.php.tpl';
		$targetFilePath = $this->extDir . '/tests/bootstrap.php';
		$this->createExtensionFile($tplPath, $targetFilePath, []);
	}

	private function updateGitignore(): void
	{
		$this->appendToFile(["!phpunit.xml"], $this->extDir . '/.gitignore');
	}

	private function appendToFile(array $content = [], string $path = ''): void
	{
		if (empty($content) || empty($path)) return;

		$currentContent = file_get_contents($path);
		$currentContent .= implode("\n", $content);
		file_put_contents($path, $currentContent);
	}

	private function createSamplePhpUnitTests()
	{
		$name = ucfirst( $this->getExtName());
		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/tests/';
		$tplPath = $extDir . 'SmokeTest.php.tpl';
		$targetFilePath = $this->extDir . '/tests/'.$name.'SmokeTest.php';

		$this->createExtensionFile($tplPath, $targetFilePath, []);
		@unlink($this->extDir . '/tests/test-sample.php');
	}


}
