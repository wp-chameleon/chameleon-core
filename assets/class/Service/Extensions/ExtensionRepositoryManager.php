<?php

namespace Gaad\Chameleon\Service\Extensions;

use Doctrine\DBAL\Exception;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Gaad\Chameleon\Traits\InitAware;
use Gitlab\Client;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionRepositoryManager
{
	use InitAware;

	private string $repositoryNamespace = 'wp-chameleon';
	private string $gitToken = '';

	private Client $client;
	private ConfigManager $configManager;

	private array $gitlabGroupDetails = [];
	private array $gitlabProjectDetails = [];
	private string $extName = '';
	private MustacheManager $mustache;
	private Filesystem $filesystem;
	/**
	 * @var mixed
	 */
	private ?array $gitlabKey;
	private ChameleonExtension $chameleonExtension;

	public function __construct(Client $gitlabClient, ConfigManager $configManager, MustacheManager $mustache, Filesystem $filesystem)
	{
		$this->repositoryNamespace = !empty($_ENV["GITLAB_CEBOARD_GROUP"]) ? $_ENV["GITLAB_CEBOARD_GROUP"] : $this->repositoryNamespace;
		$this->client = $gitlabClient;
		$this->configManager = $configManager;
		$this->mustache = $mustache;
		$this->filesystem = $filesystem;
	}


	private function gitlabGroupCreate(): bool
	{
		$created = false;
		$groups = $this->client->groups();
		$groups->create($this->getRepositoryNamespace(), $this->getRepositoryNamespace(), 'Chameleon extensions', 'public');

		return $created;
	}


	private function gitlabGroupExists(): bool
	{
		$groups = $this->client->groups()->all(['search' => $this->getRepositoryNamespace()]);
		$exists = !empty($groups) && $groups[0]['name'] === $this->getRepositoryNamespace();
		if ($exists)
			$this->setGitlabGroupDetails($groups[0]);

		return $exists;
	}

	private function gitlabProjectExists(): bool
	{
		$gitlabGroupDetails = $this->getGitlabGroupDetails();
		$projects = $this->client->groups()->projects($gitlabGroupDetails['id'], ['search' => $this->getExtName()]);
		$exists = !empty($projects) && $projects[0]['name'] === $this->getExtName();

		if ($exists)
			$this->setGitlabProjectDetails(isset($projects[0]) ? $projects[0] : $projects);
		return $exists;
	}

	/**
	 * @throws \Exception
	 */
	function create(string $extName): void
	{
		if (!$this->isInitialized()) $this->prepareData($extName);

		if ($this->gitlabProjectExists())
			throw new \Exception("Remote project already exists.");

		if (!$this->gitlabProjectExists()) $this->createRemoteRepository();
		$this->pushLocalRepository();
		$this->addSubmodule();

		$this->gitlabRemovePublicKey();
	}

	private function addSubmodule(): void
	{
		$extName = $this->getExtName();
		$repoHttpsUrl = 'https://gitlab.com/' . $this->getRepositoryNamespace() . '/' . $extName . '.git';
		shell_exec("cd /var/www/html/ && git submodule add {$repoHttpsUrl} wp-content/plugins/{$extName}");
	}

	/**
	 * @throws \Exception
	 */
	private function localRepositoryExists(): bool
	{
		$chameleonExtension = new ChameleonExtension($this->getExtName());
		return is_dir($chameleonExtension->getAbsDir() . '/.git');
	}

	/**
	 * @throws \Exception
	 */
	function pushLocalRepository()
	{
		if ($this->localRepositoryExists())
			throw new Exception('Local repository in ' . $this->getExtName() . ' already exits. This process cant continue automatically.');

		$argsDefaults = ['ssh_url_to_repo' => 'git@gitlab.com:' . $this->getRepositoryNamespace() . '/' . $this->getExtName() . '.git'];
		$template = file_get_contents(__CEBOARD_CORE_DIR__ . '/assets/tpl/git/' . 'pushNewExtensionRepo.sh.tpl');
		$content = $this->mustache->render($template, $argsDefaults);
		$installRepoScript = 'install-repo.sh';
		$this->filesystem->dumpFile($this->chameleonExtension->getAbsDir() . '/' . $installRepoScript, $content);

		shell_exec(' cd ' . $this->chameleonExtension->getAbsDir() . ' && bash ' . $installRepoScript);

		$this->filesystem->remove($this->chameleonExtension->getAbsDir() . '/' . $installRepoScript);
	}


	/**
	 * @throws Exception
	 */
	private function createRemoteRepository()
	{
		if ($this->gitlabProjectExists())
			throw new Exception('Gitlab project exists.');

		$this->client->projects()->create($this->getExtName(), [
			'namespace_id' => $this->getGitlabNamespaceId()
		]);
	}

	/**
	 * @return void
	 */
	private function authenticate(): void
	{
		$gitToken = !empty($this->gitToken) ? $this->gitToken : $_ENV['GITLAB_TOKEN'];
		$this->client->authenticate($gitToken, \Gitlab\Client::AUTH_HTTP_TOKEN);
		$this->gitlabUploadPublicKey();
	}

	/**
	 * @throws \Exception
	 */
	public function prepareData(string $extName): void
	{
		$this->setExtName($extName);
		try {
			$this->chameleonExtension = new ChameleonExtension($this->getExtName());
		} catch (\Exception $e) {
		}
		$this->authenticate();
		if (!$this->gitlabGroupExists()) {
			throw new \Exception("No repository namespace found. Create `{$this->getRepositoryNamespace()}` namespace in your remote repository.");
			//$this->gitlabGroupCreate(); //@TODO it is supported in self hosted APIs only
		}

		$this->setInitialized(true);
	}

	/**
	 * @return string
	 */
	public function getRepositoryNamespace(): string
	{
		return $this->repositoryNamespace;
	}

	/**
	 * @param string $repositoryNamespace
	 */
	public function setRepositoryNamespace(string $repositoryNamespace): void
	{
		$this->repositoryNamespace = $repositoryNamespace;
	}

	/**
	 * @return mixed
	 */
	public function getGitlabGroupDetails()
	{
		return $this->gitlabGroupDetails;
	}

	/**
	 * @param mixed $gitlabGroupDetails
	 */
	public function setGitlabGroupDetails($gitlabGroupDetails): void
	{
		$this->gitlabGroupDetails = $gitlabGroupDetails;
	}

	/**
	 * @return string
	 */
	public function getExtName(): string
	{
		return $this->extName;
	}

	/**
	 * @param string $extName
	 */
	public function setExtName(string $extName): void
	{
		$this->extName = $extName;
	}

	/**
	 * @return array
	 */
	public function getGitlabProjectDetails(): array
	{
		return $this->gitlabProjectDetails;
	}

	/**
	 * @param array $gitlabProjectDetails
	 */
	public function setGitlabProjectDetails(array $gitlabProjectDetails): void
	{
		$this->gitlabProjectDetails = $gitlabProjectDetails;
	}

	/**
	 * @return array|mixed
	 */
	private function getGitlabNamespaceId()
	{
		$gitlabGroupDetails = $this->getGitlabGroupDetails();
		return $gitlabGroupDetails['id'] ?? "";
	}

	public function removeRemoteRepository()
	{
		$gitlabProjectDetails = $this->getGitlabProjectDetails();
		$this->client->projects()->remove($gitlabProjectDetails['id']);
	}

	private function getPublicKey(): ?array
	{
		$publicKey = '/home/' . $_ENV['USER'] . '/.ssh/id_rsa.pub';
		if (is_readable($publicKey)) $keyContents = file_get_contents($publicKey);
		else return null;

		return [
			'key' => $keyContents,
			'name' => $this->getExtName(),
			'fingerprint' => str_replace("MD5:", "", explode(" ", shell_exec("ssh-keygen -E md5 -l -f /home/gaad/.ssh/id_rsa.pub"))[1])
		];
	}

	private function gitlabUploadPublicKey()
	{
		$publicKey = $this->getPublicKey();
		if ($publicKey && !$this->publicKeyNotUploaded())
			$this->gitlabKey = $this->client->users()->createKey($publicKey['name'], $publicKey['key']);
	}

	private function publicKeyNotUploaded(): bool
	{
		return !empty(array_filter($this->client->users()->keys(), function (array $key) {
			$publicKey = $this->getPublicKey();
			return explode(" ", $publicKey['key'])[1] === explode(" ", $key['key'])[1];
		}));
	}

	private function gitlabRemovePublicKey()
	{
		$key = null;
		if (!isset($this->gitlabKey['id'])) return;

		try {
			$key = $this->client->users()->key($this->gitlabKey['id']);
		} catch (Exception $e) {
		}

		if ($key) $this->client->users()->removeKey($this->gitlabKey['id']);
	}

}
