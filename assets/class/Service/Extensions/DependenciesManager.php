<?php

namespace Gaad\Chameleon\Service\Extensions;


use Exception;
use Gaad\Chameleon\Event\KernelExtensionEvent;
use Gaad\Chameleon\Service\Extensions\Parent\DependenciesManagerParent;
use Gaad\Chameleon\Service\Version\ChameleonFileManager;
use ReflectionClass;
use ReflectionParameter;

class DependenciesManager extends DependenciesManagerParent
{
	const description = 'Manage service dependencies';
	protected ReflectionClass $serviceClass;
	protected array $dependencies = [];
	protected string $classPartsTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/service/classParts';
	protected string $classTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/service';
	protected string $name = '';
	protected string $type = '';

	/**
	 * @throws \Exception
	 */
	public function loadService(string $serviceReferenceDotizedName)
	{
		global $AppKernel;
		$serviceReferenceDotizedName = trim($serviceReferenceDotizedName);
		if (!$AppKernel->serviceExists($serviceReferenceDotizedName))
			throw new \Exception('Service do not exists: `' . $serviceReferenceDotizedName . '`');
		$service = $AppKernel->getService($serviceReferenceDotizedName);
		$reflectionClass = new ReflectionClass(get_class($service));
		$this->serviceClass = $reflectionClass->getParentClass() ? $reflectionClass->getParentClass() : $reflectionClass;

		$this->extractDependencies();
	}

	public function hasDependency(string $name, ?string $type = null): bool
	{
		return !empty(array_filter($this->getDependencies(), function (array $dependency) use ($name, $type) {
			$refName = $dependency['referenceName'] ? $dependency['referenceName'] : $name;
			$dependencyName = $dependency['name'];

			return $dependencyName === $name || $name === $refName || $dependency['type'] === $type;
		}));
	}

	function camelize(string $input, string $separator = '_')
	{
		$input = str_replace('.', '', ucwords($input, '.'));
		$input = str_replace('_', '', ucwords($input, '_'));
		$input = str_replace('-', '', ucwords($input, '-'));

		return lcfirst($input);
	}

	private function generateClassCode(): string
	{
		$dependencies = $this->getDependencies();

		$useSection = $this->getClassUseSections();

		$getters = implode("\n\n", array_map(function (array $dependency) {
			$type = basename(str_replace('\\', '/', $dependency['type']));
			$fieldName = $this->camelize($dependency['name'], '.');

			if (!empty($dependency['referenceName']) && $dependency['referenceName'] !== $dependency['name'])
				$fieldName = $this->camelize($type, '.');

			$name = strlen(strstr($fieldName, $type)) ? strstr($fieldName, $type) : $fieldName;
			return $this->generatePart('Getter', [
				'fieldName' => $name,
				'returnType' => $type
			]);
		}, $dependencies));

		$template = file_get_contents($this->getClassTplDir() . '/ServiceParent.php.tpl');

		return $this->getMustache()->render($template, [
			'name' => str_replace([$this->getServiceClass()->getNamespaceName() . '\\', 'Parent'], '', $this->getServiceClass()->getName()),
			'serviceNamespace' => str_replace('\Parent', '', $this->getServiceClass()->getNamespaceName()),
			'useSection' => $useSection,
			'constructorArgsSection' => $this->generateConstructorArgsSection(),
			'classFieldsInit' => $this->generateConstructorFieldsInitSection(),
			'classFields' => $this->generateClassFieldsSection(),
			'classFieldsGetters' => $getters
		]);
	}

	/**
	 * @throws Exception
	 */
	public function addDependency(string $name, ?string $type = null)
	{
		global $AppKernel;

		$serviceExists = $AppKernel->serviceExists($name);
		if (empty($type) && !$serviceExists)
			throw new \Exception('Service class do not exists for service `' . $name . '`');

		if (empty($type) && $serviceExists)
			$type = get_class($AppKernel->getService($name));

		if ($this->hasDependency($name, $type)) return;

		$this->name = $name;
		$this->type = $type;

		$this->setDependencies(array_merge($this->getDependencies(), [[
			'name' => $name,
			'referenceName' => $name,
			'type' => $type
		]]));
	}

	public function getChameleonFileManager(): ChameleonFileManager
	{
		$array = explode('/', str_replace(WP_PLUGIN_DIR . '/', '', $this->getServiceClass()->getFileName()));
		$extName = array_shift($array);

		$chameleonFileManager = parent::getChameleonFileManager();
		if(!$chameleonFileManager->isInitialized())
			$chameleonFileManager->init([
				'chExtensionName' => $extName
			]);

		return $chameleonFileManager;
	}

	/**
	 * @throws Exception
	 */
	public function removeDependency(string $name)
	{
		global $AppKernel;

		$serviceExists = $AppKernel->serviceExists($name);
		if (!$serviceExists)
			throw new \Exception('Dependency service do not exists: `' . $name . '`');

		if (!$this->hasDependency($name))
			return;

		$dependenciesFiltered = array_filter($this->getDependencies(), function (array $dependency) use ($name) {
			return $dependency['name'] !== $name && $dependency['referenceName'] !== $name;
		});
		$this->setDependencies($dependenciesFiltered);
	}

	function parseAnnotations($doc)
	{
		preg_match_all('#@(.*?)\n#s', $doc, $annotations);
		return $annotations[1];
	}

	private function extractDependencies()
	{
		$serviceDependencies = [];
		array_map(function (ReflectionParameter $parameter) use (&$serviceDependencies) {
			$name = $parameter->getName();
			$reflectionProperty = $this->serviceClass->getProperty($name);

			$serviceDependencies[] = [
				'name' => $name,
				'referenceName' => $this->getDependencyReferenceTypeFromAnnotation($reflectionProperty),
				'type' => $parameter->getType()->getName()
			];
		}, $this->serviceClass->getConstructor()->getParameters());

		$this->setDependencies($serviceDependencies);
	}

	private function generatePart(string $partName, ?array $args = []): string
	{
		$template = file_get_contents($this->getClassPartsTplDir() . '/' . $partName . '.php.tpl');
		return $this->getMustache()->render($template, $args);
	}

	/**
	 * @return ReflectionClass
	 */
	public function getServiceClass(): ReflectionClass
	{
		return $this->serviceClass;
	}

	/**
	 * @param ReflectionClass $serviceClass
	 */
	public function setServiceClass(ReflectionClass $serviceClass): void
	{
		$this->serviceClass = $serviceClass;
	}

	/**
	 * @return array
	 */
	public function getDependencies(): array
	{
		return $this->dependencies;
	}

	/**
	 * @param array $dependencies
	 */
	public function setDependencies(array $dependencies): void
	{
		$this->dependencies = $dependencies;
	}

	/**
	 * @return array|string[]
	 */
	public function getRegexp(): array
	{
		return $this->regexp;
	}

	/**
	 * @param array|string[] $regexp
	 */
	public function setRegexp(array $regexp): void
	{
		$this->regexp = $regexp;
	}

	/**
	 * @return array|array[]
	 */
	public function getClassCode(): array
	{
		return $this->classCode;
	}

	/**
	 * @param array|array[] $classCode
	 */
	public function setClassCode(array $classCode): void
	{
		$this->classCode = $classCode;
	}

	/**
	 * @return array|string|string[]
	 */
	public function getClassPartsTplDir()
	{
		return $this->classPartsTplDir;
	}

	/**
	 * @param array|string|string[] $classPartsTplDir
	 */
	public function setClassPartsTplDir($classPartsTplDir): void
	{
		$this->classPartsTplDir = $classPartsTplDir;
	}

	private function generateConstructorArgsSection(): string
	{
		$output = [];
		array_map(function (array $dependency) use (&$output) {
			$type = basename(str_replace('\\', '/', $dependency['type']));
			$name = strlen(strstr($dependency['name'], $type)) > 0 ? strstr($dependency['name'], $type) : $dependency['name'];

			if (!empty($dependency['referenceName']) && $dependency['referenceName'] !== $dependency['name'])
				$name = $this->camelize($type, '.');

			if (strlen(strstr($this->camelize($dependency['name']), $type)) > 0)
				$name = $this->camelize(strstr($this->camelize($dependency['name']), $type), '.');

			$str = $type . ' $' . $this->camelize($name);
			$output[] = $str;
		}, $this->getDependencies());

		return "\n\t\t" . implode(', ' . "\n\t\t", $output);
	}

	public function saveServiceParentClass()
	{
		file_put_contents($this->serviceClass->getFileName(), $this->generateClassCode());

		$event = new KernelExtensionEvent(new \Gaad\Chameleon\Extension\ChameleonExtension(explode('/',explode('/plugins/',$this->getServiceClass()->getFileName())[1])[0]));
		$this->getEventDispatcher()->dispatch($event, 'kernel.service.dependency.modification');
	}

	private function generateUseSection(): string
	{
		$output = [];
		array_map(function (array $dependency) use (&$output) {
			$output[] = 'use ' . $dependency['type'];
		}, $this->getDependencies());

		return "\n\t\t\t" . implode(', ' . "\n\t\t\t", $output);
	}

	private function generateClassFieldsSection()
	{
		$output = [];
		array_map(function (array $dependency) use (&$output) {
			$type = basename(str_replace('\\', '/', $dependency['type']));
			$name = $this->camelize($dependency['name'], '.');
			$fieldName = !empty($dependency['referenceName']) ? $dependency['referenceName'] : $name;

			if (!empty($dependency['referenceName']) && $dependency['referenceName'] !== $dependency['name'])
				$fieldName = $this->camelize($type, '.');

			$name = strlen(strstr($name, $type)) ? strstr($name, $type) : $name;

			$output[] = "\n";
			$output[] = '/**';
			$output[] = '* @ChameleonServiceReference ' . $fieldName;
			$output[] = '*/';

			$output[] = 'private $' . $this->camelize($name) . ';';
		}, $this->getDependencies());

		return "\n\t" . implode("\n\t", $output) . "\n\t";
	}

	private function generateConstructorFieldsInitSection()
	{
		$output = [];
		array_map(function (array $dependency) use (&$output) {
			$name = $this->camelize($dependency['name'], '.');
			$type = basename(str_replace('\\', '/', $dependency['type']));
			if (!empty($dependency['referenceName']) && $dependency['referenceName'] !== $dependency['name'])
				$name = $this->camelize(strlen(strstr($name, $type)) ? strstr($name, $type) : $name);

			if (strlen(strstr($this->camelize($dependency['name']), $type)) > 0)
				$name = $this->camelize(strstr($this->camelize($dependency['name']), $type), '.');

			$str = '$this->' . $name . ' = $' . $name . ';';
			$output[] = $str;
		}, $this->getDependencies());

		return "\n\t\t\t" . implode("\n\t\t\t", $output) . "\n\t\t\t";
	}

	/**
	 * @return string
	 */
	public function getClassTplDir(): string
	{
		return $this->classTplDir;
	}

	/**
	 * @param string $classTplDir
	 */
	public function setClassTplDir(string $classTplDir): void
	{
		$this->classTplDir = $classTplDir;
	}

	/**
	 * @return string
	 */
	private function getClassUseSections(): string
	{
		$detectedDependecies = array_map(function (array $dependency) {
			return 'use ' . $dependency['type'] . ';';
		}, $this->getDependencies());

		$annotationDependencies = [
			'use Gaad\Chameleon\Annotation\ChameleonServiceReference;'
		];

		return implode("\n",
			array_merge($detectedDependecies, $annotationDependencies));
	}

	private function getDependencyReferenceTypeFromAnnotation(\ReflectionProperty $reflectionProperty)
	{
		$annotations = $this->parseAnnotations($reflectionProperty->getDocComment());
		$annotationHandler = null;
		array_map(function (string $item) use (&$annotationHandler) {
			$annotationParts = explode(" ", $item);
			$annotationHandlerClass = 'Gaad\Chameleon\Annotation\\' . $annotationParts[0];
			if (class_exists($annotationHandlerClass))
				$annotationHandler = new $annotationHandlerClass($annotationParts[1]);
		}, $annotations);

		if ($annotationHandler) return $annotationHandler->getReferenceName();
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		$this->type = $type;
	}

}
