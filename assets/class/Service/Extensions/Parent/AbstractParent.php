<?php

namespace Gaad\Chameleon\Service\Extensions\Parent;
use Gaad\Chameleon\Traits\HydrateObject;
use Gaad\Chameleon\Traits\InitAware;

abstract class AbstractParent
{
	use InitAware;

	public function __construct()
	{
	}
}
