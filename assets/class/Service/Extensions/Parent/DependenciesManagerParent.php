<?php

namespace Gaad\Chameleon\Service\Extensions\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Symfony\Component\Filesystem\Filesystem;
use Gaad\Chameleon\Service\Templating\MustacheManager;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Gaad\Chameleon\Service\Version\ChameleonFileManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class DependenciesManagerParent extends AbstractParent
{
	const description = '';

	
	

	/**
	* @ChameleonServiceReference filesystem
	*/
	private $filesystem;
	

	/**
	* @ChameleonServiceReference mustache
	*/
	private $mustache;
	

	/**
	* @ChameleonServiceReference configManager
	*/
	private $configManager;
	

	/**
	* @ChameleonServiceReference chameleonFileManager
	*/
	private $chameleonFileManager;
	

	/**
	* @ChameleonServiceReference event_dispatcher
	*/
	private $eventDispatcher;
	

	public function __construct(
		Filesystem $filesystem, 
		MustacheManager $mustache, 
		ConfigManager $configManager, 
		ChameleonFileManager $chameleonFileManager, 
		EventDispatcher $eventDispatcher)
	{
		
			$this->filesystem = $filesystem;
			$this->mustache = $mustache;
			$this->configManager = $configManager;
			$this->chameleonFileManager = $chameleonFileManager;
			$this->eventDispatcher = $eventDispatcher;
			
	}

	
	public function getFilesystem(): Filesystem
	{
		return $this->filesystem;
	}



	public function getMustache(): MustacheManager
	{
		return $this->mustache;
	}



	public function getConfigManager(): ConfigManager
	{
		return $this->configManager;
	}



	public function getChameleonFileManager(): ChameleonFileManager
	{
		return $this->chameleonFileManager;
	}



	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}


	
}
