<?php

namespace Gaad\Chameleon\Service\Extensions\Parent;

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Interfaces\ConfigResourcesInterface;
use Gaad\Chameleon\Traits\ConfigResourcesAware;

class ResourceAwareParent extends AbstractParent
{
	use ConfigResourcesAware;

	protected string $cacheFormat = 'json';

	/**
	 * @throws ClassException
	 */
	public function __construct()
	{
		parent::__construct();

		$this->handleResource();
	}
}
