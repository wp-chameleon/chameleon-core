<?php

namespace Gaad\Chameleon\Service\Extensions\Parent;

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Traits\InitAware;

class InitAwareParent extends AbstractParent
{
	use InitAware;

	protected string $cacheFormat = 'json';

	/**
	 * @throws ClassException
	 */
	public function __construct()
	{
		parent::__construct();

		$this->setInitialized(true);
	}
}
