<?php

namespace Gaad\Chameleon\Service\Config;

use Gaad\Chameleon\Service\Config\Parent\ConfigManagerParent;
use Gaad\Chameleon\Traits\ConfigResourcesAware;


class ConfigManager extends ConfigManagerParent
{

	use ConfigResourcesAware {
		getCacheFormat as getCacheFormat;
		getSearchSeparator as getSearchSeparator;
	}

	const description = 'System config storage service';

	protected string $configField = 'config';
	protected string $resourcesTag = 'Config';
	protected string $resourceFileName = 'config.yaml';
	protected string $cacheFormat = 'xml';
	protected string $searchSeparator = '/';


	public function onInitComplete(): array
	{
		$this->processConfigurations();
		$this->addKernelParameters();
		return $this->getConfigFieldValue();
	}


	private function addKernelParameters()
	{
		(new ConfigParametersManager($this))->addKernelParameters();
	}


	public function getConfigField(): string
	{
		return $this->configField;
	}


	public function setConfigField(string $configField): void
	{
		$this->configField = $configField;
	}

	public function getResourcesTag(): string
	{
		return $this->resourcesTag;
	}


	public function setResourcesTag(string $resourcesTag): void
	{
		$this->resourcesTag = $resourcesTag;
	}


	public function getResourceFileName(): string
	{
		return $this->resourceFileName;
	}


	public function setResourceFileName(string $resourceFileName): void
	{
		$this->resourceFileName = $resourceFileName;
	}

	protected function processConfigurations(): void
	{
		$configField = $this->getConfigField();
		$configuration = $this->getConfigurationsManager()->getConfiguration();
		$this->$configField = $this->getProcessor()->processConfiguration(
			$configuration,
			$this->$configField
		);
	}

	/**
	 * @return string
	 */
	public function getCacheFormat(): string
	{
		return $this->cacheFormat;
	}

	/**
	 * @param string $cacheFormat
	 */
	public function setCacheFormat(string $cacheFormat): void
	{
		$this->cacheFormat = $cacheFormat;
	}

	/**
	 * @return string
	 */
	public function getSearchSeparator(): string
	{
		return $this->searchSeparator;
	}

}
