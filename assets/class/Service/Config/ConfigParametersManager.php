<?php

namespace Gaad\Chameleon\Service\Config;

class ConfigParametersManager
{

    private ConfigManager $configManager;

    public function __construct(ConfigManager $configManager)
    {
        $this->configManager = $configManager;
    }

    public function addKernelParameters()
    {
        $kernelParameters = [
            'kernel.env' => $this->configManager->getEnv(),
            'kernel.cache.basedir' => __CEBOARD_CORE_DIR__,
            'kernel.db.host' => DB_HOST,
            'kernel.db.name' => DB_NAME,
            'kernel.db.user' => DB_USER,
            'kernel.db.pass' => DB_PASSWORD,
            'kernel.db.collate' => DB_COLLATE,
            'wp.root.dir' => ABSPATH,
            'wp.stylesheet.dir' => __CEBOARD_CORE_DIR__,
            'wp.stylesheet.uri' => get_stylesheet_directory_uri(),
        ];
        $parameters = array_merge($kernelParameters, $this->configManager->getParameters());
        $this->configManager->updateParameters($parameters);
    }
}
