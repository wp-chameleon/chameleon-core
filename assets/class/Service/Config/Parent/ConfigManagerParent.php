<?php

namespace Gaad\Chameleon\Service\Config\Parent;

use Doctrine\DBAL\Exception;
use Gaad\Chameleon\Config\GeneralConfigurationsManager;
use Gaad\Chameleon\Service\Extensions\Parent\ResourceAwareParent;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Yaml\Yaml;


class ConfigManagerParent extends ResourceAwareParent
{
	const description = 'System config storage service';
	private Processor $processor;
	private GeneralConfigurationsManager $configurationsManager;


	public function __construct(Processor $Processor, GeneralConfigurationsManager $configurationsManager)
	{
		$this->processor = $Processor;
		$this->configurationsManager = $configurationsManager;

		parent::__construct();
	}

	/**
	 * @return Processor
	 */
	public function getProcessor(): Processor
	{
		return $this->processor;
	}

	/**
	 * @param Processor $processor
	 */
	public function setProcessor(Processor $processor): void
	{
		$this->processor = $processor;
	}

	/**
	 * @return GeneralConfigurationsManager
	 */
	public function getConfigurationsManager(): GeneralConfigurationsManager
	{
		return $this->configurationsManager;
	}

	/**
	 * @param GeneralConfigurationsManager $configurationsManager
	 */
	public function setConfigurationsManager(GeneralConfigurationsManager $configurationsManager): void
	{
		$this->configurationsManager = $configurationsManager;
	}

}
