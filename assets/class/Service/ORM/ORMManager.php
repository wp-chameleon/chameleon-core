<?php

namespace Gaad\Chameleon\Service\ORM;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Gaad\Chameleon\Traits\EnvironmentAware;
use Gaad\Chameleon\Traits\InitAware;

class ORMManager
{
	use EnvironmentAware;

	const description = 'Doctrine Entity Manager';

	private string $env = 'dev';
	private ?EntityManager $em = null;
	private ConfigManager $config;

	use initAware;

	public function __construct(ConfigManager $config)
	{
		$this->config = $config;
	}

	/**
	 * @throws Exception|ORMException
	 */
	public function getEntityManager(): EntityManager
	{
		if (!$this->isInitialized())
			$this->prepareData($this->config);

		return $this->em;
	}

	/**
	 * @throws ORMException|Exception
	 */
	private function prepareData(ConfigManager $config): void
	{
		$dbConfig = $config->getResource('database.connections.mysql');
		$ormConfig = $config->getResource('doctrine.orm');
		$directories = apply_filters('Chameleon.ORM.Directories.Filter', []);
		$isDevMode = 'prod' !== $this->getEnv();

		$metadataConfiguration = Setup::createAnnotationMetadataConfiguration($directories, $isDevMode, $ormConfig['proxy_dir'], null, false);
		$this->em = EntityManager::create([
			'driver' => $dbConfig['driver'],
			'host' => $dbConfig['host'],
			'user' => $dbConfig['user'],
			'password' => $dbConfig['password'],
			'dbname' => $dbConfig['dbname']
		], $metadataConfiguration);

		$this->em
			->getConnection()
			->getDatabasePlatform()
			->registerDoctrineTypeMapping('enum', 'string');

		$this->setInitialized(true);
	}
}
