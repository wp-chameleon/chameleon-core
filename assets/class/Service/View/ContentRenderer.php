<?php

namespace Gaad\Chameleon\Service\View;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Gaad\Chameleon\Service\View\Parent\ContentRendererParent;

use ReflectionClass;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class ContentRenderer extends ContentRendererParent
{

	private string $controller = '';
	private string $actionName ='';

	public function renderView($controller, $action): void
	{
		echo $this->getRenderView($controller, $action);
	}

	/**
	 * @throws \Doctrine\Common\Annotations\AnnotationException
	 */
	public function getRenderView($controller, $action): string
	{
		$templateAnnotation = $this->getTemplateAnnotation($controller, $action);
		if(empty($templateAnnotation)) return '';

		$controller = $this->controller = implode('\\', array_filter(explode('\\', (string)$controller)));
		$actionName = $this->actionName = $action . 'Action';
		//$actionArgumentsTypes = $this->getActionArgumentsTypes($controller, $action);
		$request = new Request(
			$_GET,
			$_POST,
			[],
			$_COOKIE,
			$_FILES,
			$_SERVER
		);
		$controllerObj = new $controller();

		ob_start();
		$this->_render($controllerObj->$actionName($request));

		return ob_get_clean();
	}

	/**
	 * @throws \Doctrine\Common\Annotations\AnnotationException
	 */
	private function getTemplatePath(string $controller, string $action): string
	{
		$templateAnnotation = $this->getTemplateAnnotation($controller, $action);
		$defaultTemplateOverride = $templateAnnotation instanceof Template && $templateAnnotation->getTemplate();

		return $defaultTemplateOverride ? $templateAnnotation->getTemplate() : $this->getDefaultTemplatePath($controller, $action);
	}

	private function getDefaultTemplatePath(string $controller, string $action, ?string $fallback = '404.html.twig'): string
	{
		$reflection = new ReflectionClass($controller);
		$fileName = dirname($reflection->getFileName());
		$extensionName = explode('/', explode('/plugins/', $fileName)[1])[0];
		$pathPart = dirname(str_replace('\\', '/', explode('\controller\\', strtolower($controller)))[1]);
		$templateRelPath = $pathPart . '/' . $action . '.html.twig';
		$defaultPath = WP_PLUGIN_DIR . '/' . $extensionName . '/templates/' . $templateRelPath;

		return is_readable($defaultPath) ? $templateRelPath : $fallback;
	}

	private function getTemplateAnnotation(string $controller, string $action): ?Template
	{
		$controller = implode('\\', array_filter(explode('\\', (string)$controller)));
		$reflection = new ReflectionClass($controller);
		$annotationReader = new AnnotationReader();
		$actionMethod = $reflection->getMethod($action . 'Action');
		return $annotationReader->getMethodAnnotation($actionMethod, Template::class);
	}

	/**
	 * Generates and echoes output from various response types
	 *
	 * @param $response
	 * @return void
	 */
	private function _render($response): void
	{
		if($response instanceof JsonResponse){
			$response->send();
		}

		try {
			$templatePath = $this->getTemplatePath($this->controller, $this->actionName);
			echo $this
				->getTwig()
				->renderTwigTemplate($templatePath, $response->getContent());
		} catch (LoaderError|RuntimeError|SyntaxError|AnnotationException $e) {
		}
	}

	private function getActionArgumentsTypes(string $controller, $action): array
	{
		global $AppKernel;
		$controller = implode('\\', array_filter(explode('\\', (string)$controller)));
		$reflection = new ReflectionClass($controller);
		$actionMethod = $reflection->getMethod($action . 'Action');

		$parsedParameters = [];
		array_map(function($reflectionParameter) use (&$parsedParameters, $AppKernel){
			$name = $reflectionParameter->getType()->getName();
			$parsedParameters[] = $name;
		}, $actionMethod->getParameters());

		return $parsedParameters;
	}
}
