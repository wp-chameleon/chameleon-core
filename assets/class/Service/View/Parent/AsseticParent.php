<?php

namespace Gaad\Chameleon\Service\View\Parent;

use Assetic\AssetManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;
use Gaad\Chameleon\Service\Extensions\Parent\InitAwareParent;

class AsseticParent extends InitAwareParent
{
	const description = '';




	/**
	* @ChameleonServiceReference assetic.assets.manager
	*/
	private $asseticAssetsManager;


	public function __construct(
		AssetManager $asseticAssetsManager)
	{

			$this->asseticAssetsManager = $asseticAssetsManager;


		parent::__construct();
	}


	public function getAsseticAssetsManager(): AssetManager
	{
		return $this->asseticAssetsManager;
	}



}
