<?php

namespace Gaad\Chameleon\Service\View\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Templating\TwigManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ContentRendererParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference twig
	*/
	private $twig;


	public function __construct(
		TwigManager $twig)
	{

			$this->twig = $twig;


		parent::__construct();
	}


	public function getTwig(): TwigManager
	{
		return $this->twig;
	}



}
