<?php

namespace Gaad\Chameleon\Service\Version\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Gaad\Chameleon\Service\Version\ChangelogManager;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ChameleonFileManagerParent extends AbstractParent
{
	const description = '';




	/**
	* @ChameleonServiceReference changelogManager
	*/
	private $changelogManager;


	public function __construct(
		ChangelogManager $changelogManager)
	{

			$this->changelogManager = $changelogManager;

	}


	public function getChangelogManager(): ChangelogManager
	{
		return $this->changelogManager;
	}



}
