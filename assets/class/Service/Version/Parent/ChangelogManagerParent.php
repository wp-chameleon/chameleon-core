<?php

namespace Gaad\Chameleon\Service\Version\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Gaad\Chameleon\Annotation\ChameleonServiceReference;

class ChangelogManagerParent extends AbstractParent
{
	const description = '';

	
	

	/**
	* @ChameleonServiceReference eventDispatcher
	*/
	private $eventDispatcher;
	

	public function __construct(
		EventDispatcher $eventDispatcher)
	{
		
			$this->eventDispatcher = $eventDispatcher;
			
	}

	
	public function getEventDispatcher(): EventDispatcher
	{
		return $this->eventDispatcher;
	}


	
}
