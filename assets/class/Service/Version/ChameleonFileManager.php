<?php

namespace Gaad\Chameleon\Service\Version;

use Gaad\Chameleon\Exception\ServiceException;
use Gaad\Chameleon\Service\Version\Parent\ChameleonFileManagerParent;
use Gaad\Chameleon\Traits\ChameleonExtensionAware;
use ReflectionClass;
use Exception;

class ChameleonFileManager extends ChameleonFileManagerParent
{
	use ChameleonExtensionAware;

	/**
	 * @throws ServiceException
	 */
	public function serviceDependencyAdded(ReflectionClass $service, string $dependencyName, ?string $dependencyType = null)
	{
		global $AppKernel;

		if (!$AppKernel->serviceExists($dependencyName))
			throw ServiceException::notExistsError($dependencyName);

		$this->incrementVersion('minor');

		//get_class($service)

		$this->updateChangeLog('minor');

		$this->save();
	}

	public function serviceAdded(ReflectionClass $serviceClass, string $name, ?string $type)
	{
		global $AppKernel;

		if (!$AppKernel->serviceExists($serviceClass))
			throw ServiceException::notExistsError($serviceClass);

		$this->incrementVersion('minor');
		$this->save();
	}

	private function updateChangeLog(string $msg, ?string $method = 'addLog')
	{
		$changelogManager = $this->getChangelogManager();
		$methodExists = method_exists($changelogManager, $method);
		if ($methodExists)
			$changelogManager->$method($this->getVersion(), $msg);
	}

	public function load(string $path, string $extensionName)
	{
		$chameleonFile = [];
		$chameleonFileContents = file_get_contents($path);
		if ($chameleonFileContents)
			try {
				$chameleonFile = json_decode($chameleonFileContents, true);
			} catch (\Exception $e) {
			}

		$this->setChameleonFile($chameleonFile);
		$this->setChExtensionName($extensionName);
	}

	public function save()
	{
		file_put_contents(
			$this->getChExtensionChameleonFile(),
			json_encode($this->getChameleonFile(), JSON_PRETTY_PRINT));
	}

	public function getChangelogManager(): ChangelogManager
	{
		$changeLogManager = parent::getChangelogManager();
		if (!$changeLogManager->isInitialized())
			$changeLogManager->prepareData([
				'chExtensionName' => $this->getChExtensionName()
			]);

		return $changeLogManager;
	}

	public function incrementVersion(string $type = 'patch', ?int $val = 1)
	{
		$parsedVersion = $this->parseVersion();
		$parsedVersion[$type] += $val;
		$this->updateVersion($parsedVersion);
	}

	public function decrementVersion(string $type = 'patch', ?int $val = 1)
	{
		$parsedVersion = $this->parseVersion();
		$parsedVersion[$type] = $parsedVersion[$type] - $val > 0 ? $parsedVersion[$type] - $val : 0;
		$this->updateVersion($parsedVersion);
	}

	public function updateVersion(array $versionParts): void
	{
		$chameleonFile = $this->getChameleonFile();
		$chameleonFile['version'] = implode('.', $versionParts);
		$this->setChameleonFile($chameleonFile);
	}

	private function parseVersion(): array
	{
		$chameleonFile = $this->getChameleonFile();
		$versionParts = explode('.', $chameleonFile['version']);

		return [
			'major' => (int)$versionParts[0],
			'minor' => (int)$versionParts[1],
			'patch' => (int)$versionParts[2],
		];
	}

	public function getVersion(): string
	{
		return $this->isInitialized() ? $this->getChameleonFile()['version'] : '0.0.0';
	}

	public function hasRequirement(string $requirement): bool
	{
		return !empty($this->getRequirements()[$requirement]);
	}

	public function addRequirements(array $requirements): bool
	{
		$updated = false;
		array_map(function (string $requirement) use (&$updated) {
			if (!$this->hasRequirement($requirement)) {
				$requirements = $this->getRequirements();
				$requirements[] = $requirement;
				$this->setRequirements($requirements);
				$updated = true;
			}
		}, $requirements);

		return $updated;
	}

	public function removeRequirements(array $requirements): bool
	{
		$new = [];
		$currentRequirementsCount = count($this->getRequirements());
		$currentRequirements = $this->getRequirements();
		$wanted = array_diff(array_keys($this->getRequirements()), $requirements);
		foreach ($wanted as $name)
			if (!empty($currentRequirements[$name]))
				$new[$name] = $currentRequirements[$name];

		$this->setRequirements($new);

		return count($new) !== $currentRequirementsCount;
	}

	public function getRequirements(): array
	{
		return $this->isInitialized() ? $this->getChameleonFile()['require'] : [];
	}

	public function setRequirements(array $requirements)
	{
		$chameleonFile = $this->getChameleonFile();
		$chameleonFile['require'] = $requirements;
		$this->setChameleonFile($chameleonFile);
	}

}
