<?php


namespace Gaad\Chameleon\Traits;


trait HydrateObject
{

	function setData(?array $data = NULL)
	{
		if (empty( $data)) return;

		foreach ($data as $k => $v)
			if (property_exists($this, $k)) {
				$setterName = 'set' . ucfirst($k);
				if (method_exists($this, $setterName)) $this->{$setterName}($v);
					else $this->$k = $v;
			}

	}

}
