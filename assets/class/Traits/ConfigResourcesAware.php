<?php
/**
 * Add an external yaml based configuration source.
 * The yaml source can be combined from all extensions activated.
 */

namespace Gaad\Chameleon\Traits;


use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Exception\ConfigException;
use ReflectionClass;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Yaml\Yaml;


trait ConfigResourcesAware
{
	use initAware;
	use EnvironmentAware;

	/* move to class
		protected string $configField = '';
		protected string $resourcesTag = '';
		protected string $resourceFileName = '';
		protected string $cacheFormat = 'xml';
	*/

	/**
	 * Properties that needs to be present in host object
	 * @var array|string[]
	 */
	private array $configurationProperties = ['configField', 'resourcesTag', 'resourceFileName', 'cacheFormat'];
	protected string $parametersFileName = 'parameters.yaml';
	protected array $configs = [];
	protected array $config = [];
	protected $_get = null;
	protected array $resources = [];
	protected array $parametersResources;
	protected array $parameters = [];

	/**
	 * Search path for value
	 *
	 * @throws ConfigException
	 */
	function getResource(string $optionPath)
	{
		$value = null;
		$this->isInitialized() ?: $this->handleResource();
		$configFieldValue = $this->getConfigFieldValue();

		if (!$this->has($optionPath))
			throw ConfigException::unknownPath($optionPath);

		eval('$value=' . $this->getSearchArrayCode($optionPath) . ';');

		return $value;
	}

	/**
	 * Check if config address exists.
	 *
	 * @param string $optionPath
	 * @return false
	 */
	function has(string $optionPath)
	{
		$result = false;
		//this variable is essential here because of dynamic code evaluation
		$configFieldValue = $this->getConfigFieldValue();
		eval ($this->getCheckConfigAddressExists($optionPath));

		return $result;
	}

	/**
	 * Lads cached data into the object
	 *
	 * @return bool
	 */
	protected function loadCache(): bool
	{
		$this->hydrateFromCache();
		return !empty($this->getConfigFieldValue());
	}

	/**
	 * Store resource to cache file
	 *
	 * @param bool $force
	 * @return void
	 */
	protected function saveCache(bool $force = false)
	{
		$cache = new ConfigCache($this->getCacheFilePath(), true);
		$resources = [];

		if (!$cache->isFresh() || $force) {
			if (isset($this->resources))
				foreach ($this->resources as $resource) {
					if (!is_readable($resource)) continue;
					$resources[] = new FileResource($resource);
				}

			$cache->write($this->getCacheContentString(), $resources);
		}
	}

	/**
	 * Generate cache content that will be saved to a cache file of any supported type
	 *
	 * @return string
	 */
	protected function getCacheContentString(): string
	{
		$configField = $this->getConfigField();
		$configFieldValue = $this->getConfigFieldValue();
		$dataFilterExists = method_exists($this, 'getCachedCodeDataFilter');
		$dataToCache = $dataFilterExists ? $this->getCachedCodeDataFilter($configFieldValue) : $configFieldValue;

		return $this->castCacheToFormat([
			$configField => $dataToCache,
			'resources' => $this->getResources()
		]);
	}

	/**
	 * Gets whatever data is stored in cache and hydrate it to object or system
	 *
	 * @return void
	 */
	private function hydrateFromCache(): void
	{
		$cacheFilePath = $this->getCacheFilePath();
		if (!is_readable($cacheFilePath)) return;

		$configFieldName = $this->getConfigField();
		$cacheVarName = $configFieldName . $this->getCacheVarNameSuffix();
		$getCacheFormat = $this->getCacheFormat();
		$cachedDataPacket = null;

		ob_start();

		if ('json' === $getCacheFormat) {
			require_once $cacheFilePath;

			try {
				$cachedDataPacket = (array)json_decode($$cacheVarName, true);


			} catch (\Exception $e) {
			}
		}

		if ('xml' === $getCacheFormat) {
			$xml = simplexml_load_string(file_get_contents($cacheFilePath));
			try {
				$cachedDataPacket = json_decode(json_encode($xml), true);
			} catch (\Exception $e) {
			}
		}

		$this->$configFieldName = $cachedDataPacket[$configFieldName];
		$this->setResources((array)$cachedDataPacket['resources']);

		ob_clean();

		$this->setInitialized(true, true);
	}


	/**
	 * Defines cache content depending on cache output format
	 *
	 * @param array $cacheData
	 * @return string
	 */
	protected function castCacheToFormat(array $cacheData): string
	{
		$cacheFormat = $this->getCacheFormat();
		$cacheContent = $this->getSerializer()->encode($cacheData, $cacheFormat);
		ob_start();
		if ('json' === $cacheFormat) {
			echo "<?php \n";
			echo '$' . $this->configField . 'ConfigCache = \'' . $cacheContent . '\';';
		}

		if ('xml' === $cacheFormat) {
			echo $cacheContent;
		}

		return ob_get_clean() ?? '';
	}

	/**
	 * Cache format getter. It is used internally for cache IO operations.
	 * Should be overridden for cache format config in host object.
	 *
	 * @return string
	 */
	function getCacheFormat(): string
	{
		return property_exists($this, 'cacheFormat') ? $this->cacheFormat : 'json';
	}

	/**
	 * Makes sure that cache directory is available for write operation.
	 *
	 * @return string
	 */
	protected function getCacheFilePath(): string
	{
		$resourceName = $this->getResourcesTag();
		$dir = __CEBOARD_CORE_DIR__ . '/cache/' . $this->getEnv() . '/Config/' . $resourceName . 'ConfigCache.' . $this->getCacheFileExtension();
		@mkdir(dirname($dir), 0775, true);
		return $dir;
	}

	/**
	 * Replaces placeholder used in resource with the parameters values fetched from parameters yaml file
	 *
	 * @return void
	 */
	protected function applyPlaceholdersValues()
	{
		$serializer = $this->getSerializer();
		$encoded = $serializer->encode($this->getConfigFieldValue(), 'xml', ['xml_format_output' => true]);
		$placeholders = array_keys($this->getParameters());
		$placeholders = array_map(function (string $placeholder) {
			return '%' . $placeholder . '%';
		}, $placeholders);
		$replacements = array_values($this->getParameters());
		$replaced = str_replace($placeholders, $replacements, $encoded);
		$xml = simplexml_load_string($replaced);
		$cachedDataPacket = json_decode(json_encode($xml), true);

		$this->setConfigFieldValue($cachedDataPacket);
	}


	/**
	 * Checks if every essential property is available in the host object.
	 *
	 * @throws ClassException
	 */
	private function ConfigResourceAwareValidate(): void
	{
		array_map(function (string $essentialPropertyName) {
			if (!property_exists($this, $essentialPropertyName))
				throw ClassException::propertyNotExists(__CLASS__, $essentialPropertyName);

			if (empty($this->$essentialPropertyName))
				throw ClassException::essentialPropertyEmpty(__CLASS__, $essentialPropertyName);

		}, $this->getConfigurationProperties());
	}

	/**
	 * Main config resource handling method.
	 * Decides wheaten get resource from cache or reload cache from YAML files.
	 *
	 * @throws ClassException
	 */
	protected function handleResource(): void
	{
		$this->ConfigResourceAwareValidate();

		if ($this->loadCache()) return;

		$this->refreshCachedResource();
	}

	/**
	 * Read the data from config resource partials.
	 *
	 * @return void
	 */
	protected function fetchConfigFiles()
	{
		array_map(function ($resource) {
			$configField = $this->getConfigField();
			$extensionName = $this->fetchExtensionNameFromPath($resource);
			$this->$configField[$extensionName] = Yaml::parse(file_get_contents($resource));
		}, $this->resources);

		array_map(function ($resource) {
			$parameters = Yaml::parse(file_get_contents($resource));
			if (!empty($parameters))
				$this->parameters = array_merge($this->parameters, $parameters);
		}, $this->parametersResources);
	}

	/**
	 * Extracts Chameleon Extensions name from given filepath
	 *
	 * @param string $path
	 * @return string
	 */
	protected function fetchExtensionNameFromPath(string $path): string
	{
		return explode('/', explode('/plugins/', $path)[1])[0];
	}

	/**
	 * Config directories scan and appropriate fields setup.
	 *
	 * @return void
	 */
	protected function locateFiles()
	{
		$fileLocator = new FileLocator($this->getConfigResourceNameDirectories());

		$this->setResources($fileLocator->locate($this->getResourceFileName(), null, false));
		$this->setParametersResources($fileLocator->locate($this->getParametersFileName(), null, false));
	}

	/**
	 * Lunch cache rebuild procedures.
	 *
	 * @return void
	 */
	protected function refreshCachedResource(): void
	{
		$this->locateFiles();
		$this->fetchConfigFiles();
		$this->setInitialized(true);
		$this->applyPlaceholdersValues();
		$this->saveCache();
	}

	/**
	 * Gets resources tag. Resources tag is used in:
	 *    - Wordpress filter `Chameleon.[resourcesTag].Directories.Filter` that is a part of mechanism that connects
	 *      the chameleon-core and its extensions. This filter is added in extensions assets/class/boostrap.php file.
	 *
	 * @return string
	 */
	function getResourcesTag(): string
	{
		return !empty($this->resourcesTag) ? $this->resourcesTag : 'ResourcesTag';
	}

	/**
	 * Gets config resource host class field name.
	 *
	 * @return string
	 */
	function getConfigField(): string
	{
		return !empty($this->configField) ? $this->configField : 'config';
	}

	/**
	 * Getter for resources array. It is an array of absolute directories to look for config resource in.
	 *
	 * @return array
	 */
	protected function getResources(): array
	{
		return $this->resources;
	}

	/**
	 * Setter for list of partial resources files. Partial resources are used to build config resource.
	 *
	 * @param array $resources
	 * @return void
	 */
	protected function setResources(array $resources): void
	{
		$this->resources = $resources;
	}

	/**
	 * Setter for list of directories that were used to fetch parameters to replace in main config resource.
	 *
	 * @param array $parametersResources
	 * @return void
	 */
	protected function setParametersResources(array $parametersResources): void
	{
		$this->parametersResources = $parametersResources;
	}

	/**
	 * Getter for loaded from parameters file parameters array.
	 * Parameters will be used to replace placeholders in resource
	 */
	function getParameters(): array
	{
		return $this->parameters;
	}

	/**
	 * Setter for parameters array that will be used to replace placeholders in loaded resource
	 *
	 * @param array $parameters
	 */
	protected function setParameters(array $parameters): void
	{
		$this->parameters = $parameters;
	}

	/**
	 * Update parameters array, for public parameters array access
	 *
	 * @param array $newParameters
	 */
	function updateParameters(array $newParameters): void
	{
		$this->parameters = array_merge($newParameters, $this->parameters);
	}

	/**
	 * Getter for config resource file name.
	 * From this file all the config setting will be hydrated.
	 *
	 * @return string
	 */
	function getResourceFileName(): string
	{
		return !empty($this->resourceFileName) ? $this->resourceFileName : 'config.yaml';
	}

	/**
	 * Getter for parameters.yaml file
	 */
	protected function getParametersFileName(): string
	{
		$fileName = !empty($this->parametersFileName) ? $this->parametersFileName : 'parameters.yaml';
		if (!$this->isProductionEnv()) {
			$raw = explode('.yaml', $fileName)[0];
			$fileName = $raw . '-' . $this->getEnv() . '.yaml';
		}

		return $fileName;
	}

	/**
	 * Returns the loaded config resource array
	 *
	 * @return mixed
	 */
	protected function getConfigFieldValue()
	{
		$configField = $this->getConfigField();
		return $this->$configField;
	}

	/**
	 * Sets the config resource array
	 *
	 * @param array $config
	 * @return void
	 */
	protected function setConfigFieldValue(array $config)
	{
		$configField = $this->getConfigField();
		$this->$configField = $config;
	}

	/**
	 * Return serializer object.
	 * Symfony serializer component used.
	 *
	 * @return Serializer
	 */
	private function getSerializer(): Serializer
	{
		$normalizers = [new ObjectNormalizer()];
		$encoders = [new XmlEncoder(), new JsonEncoder()];
		$serializer = new Serializer($normalizers, $encoders);
		$serializer->supportsEncoding('xml');
		$serializer->supportsEncoding('json');

		return $serializer;
	}

	/**
	 * Return suffix part of cache filename.
	 *
	 * @return string
	 */
	private function getCacheVarNameSuffix(): string
	{
		return 'ConfigCache';
	}

	/**
	 * Return sile extension of cache file depending on used cache format
	 *
	 * @return string
	 */
	protected function getCacheFileExtension(): string
	{
		$types = [
			'javascript' => 'js',
			'js' => 'js',
			'php' => 'php',
			'json' => 'php',
			'serialized' => 'php',
			'xml' => 'xml',
			'yaml' => 'yaml',
		];

		return $types[$this->getCacheFormat()];
	}

	/**
	 * Getter for essential host class fields
	 *
	 * @return array
	 */
	protected function getConfigurationProperties(): array
	{
		return $this->configurationProperties;
	}

	/**
	 * Getter for path separator that is used when deep searching the resource.
	 *
	 * @return string
	 */
	protected function getSearchSeparator(): string
	{
		return '.';
	}

	/**
	 * Checks if resource of given path is available.
	 *
	 * @param string $optionPath
	 * @return string
	 */
	protected function getCheckConfigAddressExists(string $optionPath): string
	{
		return '$result=!empty(' . $this->getSearchArrayCode($optionPath) . ');';
	}

	/**
	 * Generates PHP code needed to fetch resource of given path.
	 *
	 * @param string $optionPath
	 * @return string
	 */
	protected function getSearchArrayCode(string $optionPath): string
	{
		$optionPath = explode($this->getSearchSeparator(), $optionPath);

		return '$configFieldValue[\'' . implode('\'][\'', $optionPath) . '\']';
	}

	/**
	 * Dynamically named resource getter handling
	 *
	 * @param $func
	 * @param $params
	 * @return mixed|void
	 */
	function __call($func, $params)
	{
		return $this->handleDynamicMethodsCall($func, $params);
	}

	/**
	 * Wrapper for dynamically called methods of host class.
	 * Used to cover getter calls for config resource field.
	 *
	 * @throws ClassException
	 */
	private function handleDynamicMethodsCall($func, $params)
	{
		$resourceFieldName = $this->getConfigField();
		$baseMethodName = ucfirst($resourceFieldName);
		$obj = $this;
		$closures = [

			'get' . $baseMethodName => $getterClosure = function () use ($obj) {
				$resourceFieldName = $obj->getConfigField();
				return $obj->$resourceFieldName;
			},
			'set' . $baseMethodName => $setterClosure = function (array $data) use ($obj) {
				$resourceFieldName = $obj->getConfigField();
				$obj->$resourceFieldName = $data;
			},
		];

		if (!in_array($func, array_keys($closures)))
			throw ClassException::dynamicMethodNotExists(__CLASS__, $func);

		return $closures[$func](...$params);
	}

	protected function getConfigResourceNameDirectories()
	{
		$extensionPathPart = explode('/plugins/', (new ReflectionClass($this))->getFileName());
		$extensionName = explode('/', $extensionPathPart[1])[0];
		$directoriesFetchFilterName = 'Chameleon.' . $this->getResourcesTag() . '.Directories.Filter';

		return apply_filters($directoriesFetchFilterName, [
			__CEBOARD_CORE_DIR__ . '/config',
			$extensionPathPart[0] . '/plugins/' . $extensionName . '/config',
			__CEBOARD_CORE_DIR__ . '/config'
		]);
	}
}
