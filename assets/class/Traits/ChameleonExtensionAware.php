<?php

namespace Gaad\Chameleon\Traits;


trait ChameleonExtensionAware
{
	private string $chExtensionName;
	private string $chExtensionDirectory;
	private string $chExtensionChameleonFile;
	private ?array $chameleonFile = null;


	use InitAware;
	use PluginsDirectoryAware;
	use ChangeLogAware;

	/**
	 * @throws \Exception
	 */
	public function onInitComplete(string $status)
	{
		if (!filter_var($status, FILTER_VALIDATE_BOOLEAN)) return;

		$extensionName = $this->getChExtensionName();

		$this->setChExtensionDirectory($this->getPluginsDirectory() . '/' . $extensionName);
		$this->setChExtensionChameleonFile($this->getChExtensionDirectory() . '/chameleon.json');

		if (!is_readable($this->getChExtensionChameleonFile()))
			throw new \Exception('Extension missing: ' . $extensionName);

		$this->setChameleonFile($this->getChameleonFileContents());
	}

	public function getExtensionsLocalisation(): string
	{
		global $AppKernel;
		return $AppKernel->getService('extensionsFinder');
	}

	public function getChameleonFileContents(): array
	{
		$chameleonFile = [];
		try {
			$chameleonFile = json_decode(file_get_contents($this->getChExtensionChameleonFile()), true);
		} catch (\Exception $e) {
		}

		return $chameleonFile;
	}


	/**
	 * @return string
	 */
	public function getChExtensionName(): string
	{
		return $this->chExtensionName;
	}

	/**
	 * @param string $chExtensionName
	 * @throws \Exception
	 */
	public function setChExtensionName(string $chExtensionName): void
	{
		$this->setInitialized(false);
		$this->chExtensionName = $chExtensionName;
		$this->onInitComplete(true);
		$this->setInitialized(true);
	}

	/**
	 * @return string
	 */
	public function getChExtensionDirectory(): string
	{
		return $this->chExtensionDirectory;
	}

	/**
	 * @param string $chExtensionDirectory
	 */
	public function setChExtensionDirectory(string $chExtensionDirectory): void
	{
		$this->chExtensionDirectory = $chExtensionDirectory;
	}

	/**
	 * @return string
	 */
	public function getChExtensionChameleonFile(): string
	{
		return $this->chExtensionChameleonFile;
	}

	/**
	 * @param string $chExtensionChameleonFile
	 */
	public function setChExtensionChameleonFile(string $chExtensionChameleonFile): void
	{
		$this->chExtensionChameleonFile = $chExtensionChameleonFile;
	}

	/**
	 * @return array|null
	 */
	public function getChameleonFile(): ?array
	{
		return $this->chameleonFile;
	}

	/**
	 * @param array|null $chameleonFile
	 */
	public function setChameleonFile(?array $chameleonFile): void
	{
		$this->chameleonFile = $chameleonFile;
	}

	/**
	 * @return array|null
	 */
	public function getChangelogFile(): ?array
	{
		return $this->changelogFile;
	}

	/**
	 * @param array|null $changelogFile
	 */
	public function setChangelogFile(?array $changelogFile): void
	{
		$this->changelogFile = $changelogFile;
	}

}
