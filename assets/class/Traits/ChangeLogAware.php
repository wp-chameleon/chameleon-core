<?php

namespace Gaad\Chameleon\Traits;


trait ChangeLogAware
{

	private ?array $changelogFile = null;

	/**
	 * @return array|null
	 */
	public function getChangelogFile(): ?array
	{
		return $this->changelogFile;
	}

	/**
	 * @param array|null $changelogFile
	 */
	public function setChangelogFile(?array $changelogFile): void
	{
		$this->changelogFile = $changelogFile;
	}


}
