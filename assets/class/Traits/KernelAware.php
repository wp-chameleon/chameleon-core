<?php

namespace Gaad\Chameleon\Traits;


trait KernelAware
{


	public function getKernel(): string
	{
		global $AppKernel;

		return $AppKernel;
	}
}
