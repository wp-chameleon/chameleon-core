<?php

namespace Gaad\Chameleon\Traits;


trait EnvironmentAware
{

	public function isProductionEnv(): string
	{
		return in_array($this->getEnv(), ['prod', 'production', 'live']);
	}

	public function isStageEnv(): string
	{
		return in_array($this->getEnv(), ['stage', 'test']);
	}

	public function isDevEnv(): string
	{
		return in_array($this->getEnv(), ['dev', 'develop', 'development', 'local', 'temp', 'docker']);
	}

	public function getEnv(): string
	{
		global $AppKernel;
		$PROJECT_ENVIRONMENT = !empty($_ENV['PROJECT_ENVIRONMENT']) ? $_ENV['PROJECT_ENVIRONMENT'] : 'dev';

		return $AppKernel ? $AppKernel->getEnv() : $PROJECT_ENVIRONMENT;
	}
}
