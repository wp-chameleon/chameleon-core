<?php

namespace Gaad\Chameleon\Traits;

use Gaad\Chameleon\AppKernel;

trait CHUnitTestKernel
{
	public static string $env = 'dev';
	public ?AppKernel $kernel;

	public static function setUpBeforeClass(): void
	{
		$GLOBALS['kernel'] =
			array_key_exists('AppKernel', $GLOBALS) && $GLOBALS['AppKernel'] instanceof AppKernel ? $GLOBALS['AppKernel']
				: new AppKernel('dev');
	}

	protected function setUp(): void
	{
		$this->kernel = $GLOBALS['kernel'];
	}

}
