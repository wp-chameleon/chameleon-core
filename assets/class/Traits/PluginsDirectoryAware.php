<?php

namespace Gaad\Chameleon\Traits;


trait PluginsDirectoryAware
{

	private string $pluginsDirectory = '';

	/**
	 * @return string
	 */
	public function getPluginsDirectory(): string
	{
		$pluginsDirectory = empty($this->pluginsDirectory) && defined('WP_PLUGIN_DIR') ? WP_PLUGIN_DIR : $this->pluginsDirectory;
		return str_replace('//','/',$pluginsDirectory);
	}

	/**
	 * @param string $pluginsDirectory
	 */
	public function setPluginsDirectory(string $pluginsDirectory): void
	{
		$this->pluginsDirectory = $pluginsDirectory;
	}




}
