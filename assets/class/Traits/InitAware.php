<?php

namespace Gaad\Chameleon\Traits;

trait InitAware
{
 private bool $initialized = false;

	use HydrateObject;

	public function isInitialized(): bool
	{
		return $this->initialized;
	}

	public function setInitialized(bool $initialized, ?bool $silent = false): void
	{
		if(!$silent && method_exists($this, 'beforeInitComplete')) $this->beforeInitComplete($initialized);

		$this->initialized = $initialized;

		if(!$silent && method_exists($this, 'onInitComplete')) $this->onInitComplete($initialized);
	}

	public function init(array $args = []): void
	{
		$initialized = false;
		try {
			$this->setData($args);
			$initialized = true;
		} catch (\Exception $e) {
		}

		$this->setInitialized($initialized);
	}

}
