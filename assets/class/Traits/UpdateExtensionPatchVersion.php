<?php

namespace Gaad\Chameleon\Traits;


use Gaad\Chameleon\Event\KernelExtensionEvent;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\Service\Version\ChameleonFileManager;

trait UpdateExtensionPatchVersion
{

	public function updateExtensionVersion(KernelExtensionEvent $event)
	{
		/** @var ChameleonFileManager $ChameleonFileManager */
		/** @var ChameleonExtension $extension */
		global $AppKernel;

		$ChameleonFileManager = $AppKernel->getService('chameleonFileManager');
		$extension = $event->getSubject();
		$ChameleonFileManager->load($extension->getAbsDir() . '/chameleon.json', $extension->getName());
		$ChameleonFileManager->incrementVersion();
		$ChameleonFileManager->save();
	}
}
