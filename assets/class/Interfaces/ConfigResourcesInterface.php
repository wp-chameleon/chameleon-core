<?php

namespace Gaad\Chameleon\Interfaces;

interface ConfigResourcesInterface
{

	public function getConfigField(): string;

	public function getResourcesTag(): string;

	public function getResourceFileName(): string;

	public function getCacheFormat(): string;

}
