<?php

namespace Gaad\Chameleon\Interfaces;

interface PhpClassPartInterface
{

	public function getType(): string;
	public function getName(): string;
	public function getNameFromCode(): string;
	public function getBegin(): int;
	public function getEnd(): ?int;
	function findEnd(): ?int;
	function generateCode(): array;
	function getCode(): array;
	function validate(): bool;
	function getValidateFields(): array;


}
