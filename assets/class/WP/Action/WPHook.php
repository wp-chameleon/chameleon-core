<?php

namespace Gaad\Chameleon\WP\Action;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationReader as AnnotationsReader;
use Gaad\Chameleon\Annotation\WPHookType;
use ReflectionClass;
use ReflectionMethod;

abstract class WPHook
{
	protected string $hook = '';
	protected string $applier = '';

	/**
	 * @var array|array[]
	 */
	protected array $methods = [];

	public function __construct()
	{
		$this->applyHooks();
	}

	protected function applyHooks()
	{
		$this->fetchHooksDetails();
	}

	/**
	 * @return string
	 */
	public function getApplier(): string
	{
		return $this->applier;
	}

	/**
	 * @param string $applier
	 */
	public function setApplier(string $applier): void
	{
		$this->applier = $applier;
	}

	/**
	 * @return string
	 */
	public function getHook(): string
	{
		return $this->hook;
	}

	/**
	 * @param string $hook
	 */
	public function setHook(string $hook): void
	{
		$this->hook = $hook;
	}

	protected function getAnnotationValue(string $annotationType, array $methodAnnotations)
	{
		$annotationValue = array_filter($methodAnnotations, function ($annotation) use ($annotationType) {
			return $annotation instanceof $annotationType ? $annotation->getValue() : false;
		});

		return !empty($annotationValue) ? (array_shift($annotationValue))->getValue() : [];
	}

	private function getHookedMethodAnnotations(ReflectionMethod $method): array
	{
		$methodAnnotations = [];

		array_map(function ($annotation) use (&$methodAnnotations) {
			$methodAnnotations[substr(strrchr(get_class($annotation), "\\"), 1)] = $annotation->getValue();
		}, (new AnnotationsReader())->getMethodAnnotations($method));

		return $methodAnnotations;
	}

	protected function fetchHooksDetails(): void
	{
		//fetch methods from reflection class
		$methods = array_filter((new ReflectionClass($this))->getMethods(),
			function (ReflectionMethod $method) use (&$methodAnnotations, &$methodArguments) {
			$hookTypeAnnotation = (new AnnotationReader())->getMethodAnnotation($method, WPHookType::class);
			return $hookTypeAnnotation instanceof WPHookType ? $hookTypeAnnotation->getValue() : false;
		});

		//prepare summary for every hook
		$this->setMethods(array_map(function (ReflectionMethod $method){
			$methodAnnotations = $this->getHookedMethodAnnotations($method);
			$applierClassNamespace = str_replace('\\', '\\\\', get_class($this));
			$details = [
				'applier' => 'add_'.$methodAnnotations['WPHookType'],
				'hook' => $methodAnnotations['WPHookTag'],
				'method' => [$applierClassNamespace, $method->getName()],
				'priority' => (int)$methodAnnotations['WPHookPriority'],
				'args' => count($method->getParameters())
			];
		//	$details['code'] = $details['applier'] . '(\"'.$details['hook'].'\", [new ' .$applierClassNamespace. '(), \"' .$details['method'][1]. '\"]' .$details['priority']. ', ' .$details['args'] .')';

			return $details;
		}, $methods));
	}

	/**
	 * @return array|array[]
	 */
	public function getMethods(): array
	{
		return $this->methods;
	}

	/**
	 * @param array|array[] $methods
	 */
	public function setMethods(array $methods): void
	{
		$this->methods = $methods;
	}


}
