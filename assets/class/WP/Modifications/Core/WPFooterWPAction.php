<?php

namespace Gaad\Chameleon\WP\Modifications\Core;

use Gaad\Chameleon\Annotation\WPHookPriority;
use Gaad\Chameleon\Annotation\WPHookTag;
use Gaad\Chameleon\Annotation\WPHookType;
use Gaad\Chameleon\WP\Action\WPActionHook;

/**
 */
class WPFooterWPAction extends WPActionHook
{

	/**
	 * @WPHookPriority(100)
	 * @WPHookTag("init")
	 * @WPHookType("action")
	 *
	 * @param $arg1
	 * @return mixed
	 */
	function SomePreparationsOnLateInit($arg1)
	{
		return $arg1;
	}

	/**
	 * @WPHookPriority(100)
	 * @WPHookTag("init")
	 * @WPHookType("filter")
	 *
	 * @param $arg1
	 * @return mixed
	 */
	function SomeFiltrationOnLateInit($arg1)
	{
		return $arg1;
	}

	/**
	 * @WPHookTag("group1")
	 * @WPHookPriority(101)
	 *
	 * @param $arg1
	 * @return mixed
	 */
	function methodBAction($arg1)
	{
		return $arg1;
	}

	/**
	 * @WPHookTag("group1")
	 * @WPHookPriority(102)
	 *
	 * @param $arg1
	 * @return mixed
	 */
	function methodCAction($arg1)
	{
		return $arg1;

	}


}

