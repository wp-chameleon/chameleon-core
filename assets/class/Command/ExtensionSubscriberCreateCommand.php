<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Event\EventSubscriberCreator;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceCreator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionSubscriberCreateCommand extends Command
{
	protected static $defaultName = 'subscriber:create|su:c';


	protected function configure()
	{
		$this
			->setDescription('Create Chameleon event subscriber.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('subscriberName', InputArgument::REQUIRED, 'Name of the subscriber'),
					new InputArgument('extensionName', InputArgument::REQUIRED, 'Name of the extension'),
					new InputOption('description', 'd', InputArgument::OPTIONAL)
				])
			);
	}

	/**
	 * @throws \Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;

		/** @var EventSubscriberCreator $EventSubscriberCreator */
		$EventSubscriberCreator = $AppKernel->getService('EventSubscriberCreator');
		$EventSubscriberCreator->createSubscriber(
			$input->getArgument('subscriberName'),
			$input->getArgument('extensionName'),
			true
		);

		$AppKernel->clearCache();
		$output->writeln('Subscriber created.');

		return 0;
	}
}
