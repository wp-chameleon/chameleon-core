<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionRemoveCommand extends BaseCommand
{
	protected static $defaultName = 'extension:remove|ext:r';


	protected function configure()
	{
		$this
			->setDescription('Remove Chameleon extension.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('name', InputArgument::REQUIRED, 'Name of the extension' ),
					new InputOption('remove-repo', null, InputOption::VALUE_OPTIONAL, 'Defines an environment.', false)
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$extName = $input->getArgument('name');
		$helper = $this->getHelper('question');
		$question = new ConfirmationQuestion('Really remove `'.$extName.'` extension?', false);

		if (!$helper->ask($input, $output, $question)) {

			$extensionsManager = $this->kernel->getService('extensions.manager');
			$extensionsManager->removeExtension($extName);
			$extensionsManager->clearCache();
			$output->writeln('Extension removed.');

			if (false !== $input->getOption('remove-repo')){
				$repoCreator = $this->kernel->getService('extension.repository.manager');
				$repoCreator->remove($extName);
			}

			return 0;
		}


		return 0;
	}
}
