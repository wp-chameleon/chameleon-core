<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\Service\Routing\RoutesList;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RoutesListCommand extends ChameleonCommandBaseCommand
{
	protected static $defaultName = 'route:list|r:l';
	protected ?RoutesList $list;
	private int $listColWidthMinimum = 80;
	private int $listColWidth = 0;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->list = $this->kernel->getService('RoutesList');
	}


	protected function configure()
	{
		$this
			->setDescription('Chameleon routes list.')
			->setDefinition(
				new InputDefinition([
					new InputOption('extension', 'ext', InputArgument::OPTIONAL, 'Specify an extension name.'),
					new InputOption('full', null, InputOption::VALUE_OPTIONAL, 'Show full information table.', false)
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->applyFormatters($output);
		$full = is_null($input->getOption('full')) && false !== $input->getOption('full');

		$routes = $this->list->getRoutes();
		foreach ($routes as $controllerHash => $controllerDetails) {
			$extensionName = explode('/', explode('/plugins/', $controllerDetails['controller'])[1])[0];
			$controllerName = explode($extensionName . '/assets/class/Controller', $controllerDetails['controller'])[1];
			$controllerName = '/' === $controllerName[0] ? substr($controllerName, 1) : $controllerName;
			$output->writeln('<extension-name>' . $extensionName . '</> <secondary-info>(' . $controllerName . ')</>');

			foreach ($controllerDetails['routes'] as $action => $routesDetails) {
				$path = $routesDetails['path'];
				$addSpaces = (int)($this->listColWidth - strlen($path));
				$addSpaces = 0 > $addSpaces ? 0 : $addSpaces;

				$output->write("  <php-class>" . $action . '</>');
				$output->write( str_repeat(" ", $addSpaces) . "  <service-name>" . $path . '</>');
				$output->writeln("");
			}

			$output->writeln("");

		}

		return 0;
	}

	/**
	 * @param $class
	 * @return mixed|string
	 */
	protected function getServiceDescription($class)
	{
		$descriptionConstantName = $class . "::description";
		$description = '';
		if (defined($descriptionConstantName))
			$description = constant($descriptionConstantName);
		return $description;
	}

}
