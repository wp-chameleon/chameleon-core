<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

abstract class BaseCommand extends Command
{

	protected ?AppKernel $kernel;
	protected array $formatters = [];

	public function __construct(string $name = null)
	{
		global $AppKernel;

		parent::__construct($name);

		$this->kernel = $AppKernel;

	}


	protected function configure()
	{
		$this
			->setDescription('Clear cache directory')
			->setDefinition(
				new InputDefinition([
					new InputOption('env', null, InputOption::VALUE_OPTIONAL, 'Defines an environment.', 'dev')
				])
			);
	}

	protected function applyFormatters(OutputInterface $output): void
	{
		foreach ($this->getFormatters() as $name => $formatDetails) {
			extract($formatDetails);
			$output->getFormatter()->setStyle($name, new OutputFormatterStyle($foreground, $background, $options));
		}
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;
		$filesystem = new Filesystem();
		$config = $AppKernel->getService('config')->getConfig();
		$optionEnv = $input->getOption('env');
		$env = !empty($optionEnv) ? $optionEnv : $AppKernel->getEnv();
		$dir = __CEBOARD_CORE_DIR__ . '/' . $config['cache']['directory'] . '/' . $env;

		$filesystem->remove($dir);

		$output->writeln('Cache cleared for ' . $env . '.');

		return 0;
	}

	protected function addFormatter(string $name, array $formatDetails)
	{
		$this->formatters[$name] = $formatDetails;
	}

	/**
	 * @return array
	 */
	public function getFormatters(): array
	{
		return $this->formatters;
	}

	/**
	 * @param array $formatters
	 */
	public function setFormatters(array $formatters): void
	{
		$this->formatters = $formatters;
	}


}
