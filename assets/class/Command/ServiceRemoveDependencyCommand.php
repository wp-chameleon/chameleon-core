<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\Service\Extensions\DependenciesManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ServiceRemoveDependencyCommand extends BaseCommand
{
	protected static $defaultName = 'service:dependency:remove|se:d:r';
	private ?DependenciesManager $dependenciesManager;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->dependenciesManager = $this->kernel->getService('extension.service.dependencies.manager');
	}


	protected function configure()
	{
		$this
			->setDescription('Remove dependency from Chameleon service.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('serviceName', InputArgument::REQUIRED, 'Name of the service'),
					new InputArgument('dependencyServiceName', InputArgument::REQUIRED, 'Name of the dependency service to add')
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		global $AppKernel;

		$serviceName = $input->getArgument('serviceName');
		$dependencyServiceName = $input->getArgument('dependencyServiceName');
		$dependenciesManager = $this->dependenciesManager;

		$helper = $this->getHelper('question');
		$question = new ConfirmationQuestion('Really remove `'.$dependencyServiceName.'` from `'.$serviceName , false);

		if (!$helper->ask($input, $output, $question)) {
			$dependenciesManager->loadService($dependencyServiceName);
			$dependenciesManager->removeDependency($dependencyServiceName);
			$dependenciesManager->saveServiceParentClass();
			$AppKernel->clearCache();
		}
		return 0;
	}


}
