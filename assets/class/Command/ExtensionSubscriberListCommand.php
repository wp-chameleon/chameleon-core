<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Event\EventSubscriberCreator;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceCreator;
use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionSubscriberListCommand extends ChameleonCommandBaseCommand
{
	protected static $defaultName = 'subscriber:list|su:l';


	protected function configure()
	{
		$this
			->setDescription('List Chameleon subscribers.')
			->setDefinition(
				new InputDefinition([
				])
			);
	}

	/**
	 * @throws \Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;
		$this->applyFormatters($output);

		$serviceLine = function (string $item) use ($input, $output) {
			$output->writeln("    <secondary-info>- " . $item .'</>' );
		};

		/** @var EventSubscriberCreator $EventSubscriberCreator */
		$EventSubscribersManager = $AppKernel->getService('EventSubscribersManager');
		$extensionNameLine = '';
		foreach ($EventSubscribersManager->getSubscribers() as $i => $subscribersGroup) {
			if (empty($subscribersGroup)) continue;

			foreach ($subscribersGroup as $subscriberReferenceName => $item) {
				$extensionName = $item['extensionName'];
				if($extensionName !== $extensionNameLine) {
					$output->writeln("<extension-name>" . $extensionName .'</>');
					$extensionNameLine = $extensionName;
				}

				try{
					$className = implode('\\', array_filter(explode('\\', (string)$item['class'])));
					$var = new $className();
				} catch (\Exception $e){
					echo $e->getMessage();
					die();
				}
				$subscribedEvents = array_keys($var->getSubscribedEvents());

				$name = str_replace('/Subscriber$/g','',$item['name']);
				$output->writeln("  <subscriber-name>" . $name .'</>');
				$output->writeln("    <php-class>" .$item['class'] .'</>' );
				array_map($serviceLine, $subscribedEvents);
				$output->writeln("");
			}
		}

		return 0;
	}
}
