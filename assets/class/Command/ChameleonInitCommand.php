<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ChameleonInitCommand extends BaseCommand
{
	protected static $defaultName = 'chameleon:init|ch:i';


	protected function configure()
	{
		$this
			->setDescription('Create chameleon.json file in stylesheet directory.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('name', InputArgument::REQUIRED, 'Projects name' )
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{

		$extDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/extension/';
		$fs = $this->filesystem;
		$tplPath = $extDir . '/bootstrap.php.tpl';
		$targetFilePath = $this->extDir . '/assets/bootstrap.php';
		$this->createExtensionFile($tplPath, $targetFilePath);


		$output->writeln('TODO 2135');

		return 0;
	}
}
