<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionServiceRemoveCommand extends Command
{
	protected static $defaultName = 'service:remove|se:r';


	protected function configure()
	{
		$this
			->setDescription('Remove Chameleon extensions service.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('extensionName', InputArgument::REQUIRED, 'Name of the extension'),
					new InputArgument('serviceName', InputArgument::REQUIRED, 'Name of the service')
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;

		$service = $AppKernel->getService('extension.service.creator');

		$serviceName = $input->getArgument('serviceName');
		$extensionName = $input->getArgument('extensionName');

		$helper = $this->getHelper('question');
		$question = new ConfirmationQuestion('Really remove `'.$serviceName.'` service?', false);

		if (!$helper->ask($input, $output, $question)) {

			$service->removeService(
				$serviceName,
				$extensionName
			);
			$service->clearCache();
			$output->writeln('Service '.$serviceName.' successfully removed from '.$extensionName.'.');

			return 0;
		}

	}
}
