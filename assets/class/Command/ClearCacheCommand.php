<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ClearCacheCommand extends Command
{
	protected static $defaultName = 'core:clear:cache|c:c';


	protected function configure()
	{
		$this
			->setDescription('Clear cache directory')
			->setDefinition(
				new InputDefinition([
					new InputOption('env', null, InputOption::VALUE_OPTIONAL, 'Defines an environment.', 'dev')
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;
		$filesystem = new Filesystem();
		$config = $AppKernel->getService('config')->getConfig();
		$optionEnv = $input->getOption('env');
		$env = !empty($optionEnv) ? $optionEnv : $AppKernel->getEnv();
		$dir = __CEBOARD_CORE_DIR__ . '/' . $config['cache']['directory'] . '/' . $env;

		$filesystem->remove($dir);

		$output->writeln('Cache cleared for ' . $env . '.');

		return 0;
	}
}
//
