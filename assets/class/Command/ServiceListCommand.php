<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\Service\Extensions\DependenciesManager;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceList;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ServiceListCommand extends ChameleonCommandBaseCommand
{
	protected static $defaultName = 'service:list|se:l';
	protected ?ExtensionServiceList $list;
	private int $listColWidthMinimum = 80;
	private int $listColWidth = 0;
	private ?DependenciesManager $dependenciesManager;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->dependenciesManager = $this->kernel->getService('extension.service.dependencies.manager');
		$this->list = $this->kernel->getService('extension.service.list');
		$this->listColWidth = $this->calculateMinListColumnWidth();
	}


	protected function configure()
	{
		$this
			->setDescription('List Chameleon system commands.')
			->setDefinition(
				new InputDefinition([
					new InputOption('extension', 'ext', InputArgument::OPTIONAL, 'Specify an extension name.'),
					new InputOption('full', null, InputOption::VALUE_OPTIONAL, 'Show full information table.', false)
				])
			);

	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->applyFormatters($output);
		$full = is_null($input->getOption('full')) && false !== $input->getOption('full');

		$serviceLine = function (array $item) use ($input, $output, $full) {
			$extName = $item['name'];
			$addSpaces = (int)($this->listColWidth - strlen($extName));
			$addSpaces = 0 > $addSpaces ? 0 : $addSpaces;
			$description = $this->getServiceDescription($item['class']);

			$dependencies = [];
			if ($full) {
				$dependencies = $this->getServiceDependencies($item['name']);
				$dependenciesAvailable = strlen($dependencies);
			}

			$output->writeln("  <php-class>" .$item['class'] .'</>' );
			$output->write("  <service-name>" . $extName .'</>' . str_repeat(" ", $addSpaces));
			$output->writeln($description);

			if ($dependenciesAvailable) {
				$output->writeln($dependencies);
				$output->writeln("");
			} elseif($full) $output->writeln("");

			if (!$full) $output->writeln("");
		};

		foreach ($this->list->getAllServices() as $extName => $extServices) {
			$output->writeln("<extension-name>" . $extName.'</>');
			array_map($serviceLine, $extServices);
			$output->writeln("");
		}

		return 0;
	}

	private function calculateMinListColumnWidth()
	{
		$services = $this->list->getAllServices();
		$length = 0;
		foreach ($services as $extName => $services_) {
			$length = $length < strlen($extName) ? strlen($extName) : $length;
			foreach ($services_ as $name => $item) {
				$length = $length < strlen($extName) ? strlen($extName) : $length;
			}
		}

		$length_ = $length + 2;

		return $length_ >= $this->listColWidthMinimum ? $length_ : $this->listColWidthMinimum;
	}

	/**
	 * @param $class
	 * @return mixed|string
	 */
	protected function getServiceDescription($class)
	{
		$descriptionConstantName = $class . "::description";
		$description = '';
		if (defined($descriptionConstantName))
			$description = constant($descriptionConstantName);
		return $description;
	}

	private function getServiceDependencies(string $serviceName): string
	{
		global $AppKernel;

		$serviceDefinition = $AppKernel->getServiceDefinition($serviceName);
		$arguments = $serviceDefinition->getArguments();
		$dependencyServices = [];

		foreach ($arguments as $definition) {
			if (method_exists($definition, 'getClass')) {
				$registeredService = $AppKernel->getServiceDefinitionByType($definition->getClass());
				$dependencyServices[] = !empty($registeredService) ? $registeredService : $definition->getClass();
			}

			if (property_exists($definition, 'id')) {
				$array = (array)$definition;
				$serviceName = array_shift($array);
				$dependencyServices[] = '<service-name>' . $AppKernel->serviceExists($serviceName) ? $serviceName : 'unknown.service</>';
			}
		}

		$dependencyServices = array_map(function (string $dependency) {
			return "    <service-dependency>- " . $dependency .'</>';
		}, $dependencyServices);

		$implode = implode("\n", $dependencyServices);

		return $implode;
	}
}
