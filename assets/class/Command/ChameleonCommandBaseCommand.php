<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceList;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

abstract class ChameleonCommandBaseCommand extends BaseCommand
{
	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->configureFormatters();
	}

	/**
	 * CLI formatters for Chameleon related command context.
	 * The idea is to render elements of Chameleon env  using same formatting
	 *
	 * @return void
	 */
	protected function configureFormatters()
	{

		$this->addFormatter('subscriber-name',
			[
				'foreground' => 'cyan',
				'background' => 'default',
				'options' => ['bold']

			]);

		$this->addFormatter('extension-name',
			[
				'foreground' => 'green',
				'background' => 'default',
				'options' => ['bold']

			]);

		$this->addFormatter('controller',
			[
				'foreground' => 'cyan',
				'background' => 'default',
				'options' => ['bold']

			]);

		$this->addFormatter('secondary-info',
			[
				'foreground' => 'gray',
				'background' => 'default',
				'options' => []
			]);

		$this->addFormatter('service-name',
			[
				'foreground' => 'cyan',
				'background' => 'default',
				'options' => ['bold']
			]);

		$this->addFormatter('service-dependency',
			[
				'foreground' => 'gray',
				'background' => 'default',
				'options' => []
			]);

		$this->addFormatter('php-class',
			[
				'foreground' => 'yellow',
				'background' => 'default',
				'options' => []
			]);
	}


}
