<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\Service\Extensions\ExtensionsManager;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class ExtensionsRepositoryAddCommand extends BaseCommand
{
	protected static $defaultName = 'extensions-repository:add|er:a';
	private ?ExtensionsManager $extManager;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->extManager = $this->kernel->getService('extensions.manager');
	}

	protected function configure()
	{
		$this
			->setDescription('Add Chameleon extensions repository.')
			->setDefinition(
				new InputDefinition([
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$serviceLine = function (array $item) use ($input, $output) {
			$require = $item['require'];
			$description = $item['description'];

			empty($description) ?: $output->writeln("  " . $description);
			if (!empty ($require)) {
				$output->writeln("  " . "require:");
				foreach ($require as $requiredExtName => $version) {
					$output->write("  " . "  " . $requiredExtName . ": " . $version);

					$extensionsDetails = $this->extManager->getExtensionsDetails($requiredExtName);
					$isExtensionInstalled = !empty($extensionsDetails);
					$isExtensionHealthy = $this->extManager->isExtensionHealthy($requiredExtName, $version);

					$notInstalled = !$isExtensionInstalled || $isExtensionHealthy;
					$isExtensionHealthyStr = $notInstalled ? '' : 'needs update';
					$isExtensionInstalledStr = ($isExtensionInstalled ? '' : 'not ') . 'installed';
					$isExtensionInstalledStr .= !$isExtensionHealthy && !$notInstalled ? ' ' . $extensionsDetails['definition']['version'] : '';
					$isExtensionInstalledStr = $isExtensionInstalledStr . ($notInstalled ? '' : ', ');

					$output->writeln(' (' . $isExtensionInstalledStr . $isExtensionHealthyStr . ')');
				}
			}
		};

		foreach ($this->extManager->getAllExtensions() as $extName => $extDetails) {
			if (empty($extDetails)) continue;

			$output->write($extName);
			$version = !empty($extDetails['version']) ? $extDetails['version'] : '1.0.0';

			$output->writeln(' (version: ' . $version . ')');
			array_map($serviceLine, [$extDetails]);
			$output->writeln("");
		}

		return 0;
	}
}
