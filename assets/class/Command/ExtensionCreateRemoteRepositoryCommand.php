<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionCreateRemoteRepositoryCommand extends BaseCommand
{
	protected static $defaultName = 'extension:repository:create|ext:r:c';


	protected function configure()
	{
		$this
			->setDescription('Create Chameleon extensions repository on Gitlab.com.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('name', InputArgument::REQUIRED, 'Name of the extension' )
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{

		$repo = $this->kernel->getService('extension.repository.manager');
		$repo->create($input->getArgument('name'));

		$output->writeln('Repository created.');

		return 0;
	}
}
