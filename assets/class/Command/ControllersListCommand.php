<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\Controller\ControllerReflectionManager;
use Gaad\Chameleon\Service\Controller\ControllersList;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ControllersListCommand extends ChameleonCommandBaseCommand
{
	protected static $defaultName = 'controller:list|c:l';
	protected ?ControllersList $list;
	private int $listColWidthMinimum = 80;
	private int $listColWidth = 0;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->list = $this->kernel->getService('ControllersList');
	}


	protected function configure()
	{
		$this
			->setDescription('Chameleon  controllers list.')
			->setDefinition(
				new InputDefinition([
					new InputOption('extension', 'ext', InputArgument::OPTIONAL, 'Specify an extension name.'),
					new InputOption('full', null, InputOption::VALUE_OPTIONAL, 'Show full information table.', false)
				])
			);

	}

	/**
	 * @throws \ReflectionException
	 * @throws \Gaad\Chameleon\Exception\ClassException
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->applyFormatters($output);
		$full = is_null($input->getOption('full')) && false !== $input->getOption('full');

		$controllers = $this->list->getControllers();
		foreach ($controllers as $extName => $controllersList) {
			$output->writeln("<extension-name>" . $extName.'</>');

			/** @var ControllerReflectionManager $controllerReflectionManager */
			foreach ($controllersList as $controllerHash => $controllerReflectionManager) {
				$controllerClassPath = $controllerReflectionManager->getControllerClassPath();
				$controllerReflectionManager->getClassManager()->loadFromFile($controllerClassPath);
				$reflectionClass = $controllerReflectionManager->getClassManager()->getRef();
				$extensionName = explode('/', explode('/plugins/', $controllerClassPath)[1])[0];
				$controllerName = explode($extensionName . '/assets/class/Controller', $controllerClassPath)[1];
				$controllerName = '/' === $controllerName[0] ? substr($controllerName, 1) : $controllerName;
				$output->writeln('<controller>' . $controllerName . '</>');

				foreach($reflectionClass->getMethods() as $method) {
					$name = $method->getName();
					if(strstr($name, 'Action')){
						$addSpaces = (int)($this->listColWidth - strlen($name));
						$addSpaces = 0 > $addSpaces ? 0 : $addSpaces;
						$output->write("  <php-class>" . $name . '</>');
						$output->writeln("");
					}
				}
				$output->writeln("");
			}
			$output->writeln("");
		}

		return 0;
	}

	/**
	 * @param $class
	 * @return mixed|string
	 */
	protected function getServiceDescription($class)
	{
		$descriptionConstantName = $class . "::description";
		$description = '';
		if (defined($descriptionConstantName))
			$description = constant($descriptionConstantName);
		return $description;
	}

}
