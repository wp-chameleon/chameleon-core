<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceCreator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionServiceCreateCommand extends Command
{
	protected static $defaultName = 'service:create|se:c';


	protected function configure()
	{
		$this
			->setDescription('Create Chameleon extensions service.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('extensionName', InputArgument::REQUIRED, 'Name of the extension'),
					new InputArgument('serviceFullClassName', InputArgument::REQUIRED, 'Name of the service'),
					new InputOption('serviceNameOverride', 'sno', InputArgument::OPTIONAL),
					new InputOption('parentClassBase', 'pcb', InputArgument::OPTIONAL),
					new InputOption('description', 'd', InputArgument::OPTIONAL),
					new InputOption('dependencies', 'dep', InputArgument::OPTIONAL)
				])
			);
	}

	/**
	 * @throws \Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;

		/** @var ExtensionServiceCreator $service */
		$service = $AppKernel->getService('extension.service.creator');

		$service->createService(
			$input->getArgument('serviceFullClassName'),
			$input->getArgument('extensionName'),
			$this->prepareArgs($input)
		);
		$AppKernel->clearCache();
		$output->writeln('Service created.');

		return 0;
	}

	protected function prepareArgs(InputInterface $input): array
	{
		$args = [
			'serviceNameOverride' => $input->getOption('serviceNameOverride'),
			'description' => $input->getOption('description'),
			'dependencies' => $input->getOption('dependencies'),
		];

		if ($input->hasOption('parentClassBase'))
			$args['parentClassBase'] = $input->getOption('parentClassBase');

		return $args;
	}
}
