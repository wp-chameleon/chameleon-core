<?php

namespace Gaad\Chameleon\Command;

use Doctrine\Common\Annotations\AnnotationReader;
use Gaad\Chameleon\Annotation\WPHookPriority;
use Gaad\Chameleon\Annotation\WPHookTag;
use Gaad\Chameleon\Service\WP\Modifications\ModsList;
use ReflectionClass;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ModsListCommand extends ChameleonCommandBaseCommand
{
	protected static $defaultName = 'mods:list|m:l';
	protected ?ModsList $list;
	private int $listColWidthMinimum = 80;
	private int $listColWidth = 0;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->list = $this->kernel->getService('ModsList');
	}


	protected function configure()
	{
		$this
			->setDescription('List Wordpress modifications.')
			->setDefinition(
				new InputDefinition([
					new InputOption('extension', 'ext', InputArgument::OPTIONAL, 'Specify an extension name.'),
					new InputOption('full', null, InputOption::VALUE_OPTIONAL, 'Show full information table.', false)
				])
			);

	}

	/**
	 * @throws \ReflectionException
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		$this->applyFormatters($output);
		$full = is_null($input->getOption('full')) && false !== $input->getOption('full');
		$annotationReader = new AnnotationReader();
		foreach ($this->list->getMods() as $extName => $mods) {
			$output->writeln("<extension-name>" . $extName . '</>');
			foreach ($mods as $modDetails) {
				$output->writeln("  <service-name>" . $modDetails['name'] . '</>');
				$reflectionClass = new ReflectionClass($modDetails['class']);
				foreach ($reflectionClass->getMethods() as $method) {
					$WPHookTag = $annotationReader->getMethodAnnotation($method, WPHookTag::class);
					if (!empty($WPHookTag)) {
						$WPHookPriority = $annotationReader->getMethodAnnotation($method, WPHookPriority::class);
						$priority = $WPHookPriority ? $WPHookPriority->getValue() : 10;
						$output->writeln("    " . $method->getName() . " <secondary-info>hooked to " . $WPHookTag->getValue() . " at priority ".$priority."</>");
					}
				}
			}
			$output->writeln("");
		}

		return 0;
	}

	/**
	 * @param $class
	 * @return mixed|string
	 */
	protected function getServiceDescription($class)
	{
		$descriptionConstantName = $class . "::description";
		$description = '';
		if (defined($descriptionConstantName))
			$description = constant($descriptionConstantName);
		return $description;
	}

}
