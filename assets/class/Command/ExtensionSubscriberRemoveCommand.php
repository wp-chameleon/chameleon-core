<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Event\EventSubscriberCreator;
use Gaad\Chameleon\Service\Extensions\ExtensionServiceCreator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionSubscriberRemoveCommand extends Command
{
	protected static $defaultName = 'subscriber:remove|su:r';


	protected function configure()
	{
		$this
			->setDescription('Remove Chameleon extensions service.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('subscriberName', InputArgument::REQUIRED, 'Name of the subscriber'),
					new InputArgument('extensionName', InputArgument::REQUIRED, 'Name of the extension'),
					new InputOption('description', 'd', InputArgument::OPTIONAL)
				])
			);
	}

	/**
	 * @throws \Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** @var AppKernel $AppKernel */
		global $AppKernel;

		$subscriberName = $input->getArgument('subscriberName');
		$extensionName = $input->getArgument('extensionName');

		/** @var EventSubscriberCreator $EventSubscriberCreator */
		$EventSubscriberCreator = $AppKernel->getService('EventSubscriberCreator');

		$helper = $this->getHelper('question');
		$question = new ConfirmationQuestion('Really remove `' . $subscriberName . '` subscriber?', false);

		if (!$helper->ask($input, $output, $question))
			$EventSubscriberCreator->removeSubscriber($subscriberName, $extensionName, true);

		$AppKernel->clearCache();
		$output->writeln('Service created.');

		return 0;
	}
}
