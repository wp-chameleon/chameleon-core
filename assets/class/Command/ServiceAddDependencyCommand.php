<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\Service\Extensions\DependenciesManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServiceAddDependencyCommand extends BaseCommand
{
	protected static $defaultName = 'service:dependency:add|se:d:a';
	private ?DependenciesManager $dependenciesManager;

	public function __construct(string $name = null)
	{
		parent::__construct($name);

		$this->dependenciesManager = $this->kernel->getService('extension.service.dependencies.manager');
	}


	protected function configure()
	{
		$this
			->setDescription('Add dependency to Chameleon service.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('serviceName', InputArgument::REQUIRED, 'Name of the service'),
					new InputArgument('dependencyServiceName', InputArgument::REQUIRED, 'Name of the dependency service to add')
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		global $AppKernel;

		$serviceName = $input->getArgument('serviceName');
		$dependencyServiceName = $input->getArgument('dependencyServiceName');
		$dependenciesManager = $this->dependenciesManager;

		$dependenciesManager->loadService($serviceName);
		$dependenciesManager->addDependency($dependencyServiceName);
		$dependenciesManager->saveServiceParentClass();
		$AppKernel->clearCache();

		return 0;
	}


}
