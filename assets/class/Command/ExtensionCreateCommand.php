<?php

namespace Gaad\Chameleon\Command;

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Service\Extensions\ExtensionsManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ExtensionCreateCommand extends BaseCommand
{
	protected static $defaultName = 'extension:create|ext:c';


	protected function configure()
	{
		$this
			->setDescription('Create Chameleon extension as wordpress plugin.')
			->setDefinition(
				new InputDefinition([
					new InputArgument('name', InputArgument::REQUIRED, 'Name of the extension' ),
					new InputOption('env', null, InputOption::VALUE_OPTIONAL, 'Defines an environment.', 'dev'),
					new InputOption('create-repo', null, InputOption::VALUE_OPTIONAL, 'Defines an environment.', false)
				])
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		global $AppKernel;
		$extName = $input->getArgument('name');

		/** @var ExtensionsManager $extCreator */
		$extCreator = $this->kernel->getService('extensions.manager');
		$extCreator->createExtension($extName, false);
		$AppKernel->clearCache();
		$output->writeln('Extension created.');

		if (false !== $input->getOption('create-repo')){
			$repoCreator = $this->kernel->getService('extension.repository.manager');
			$repoCreator->create($extName);
		}

		return 0;
	}
}
