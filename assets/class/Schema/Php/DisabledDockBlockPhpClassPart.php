<?php

namespace Gaad\Chameleon\Schema\Php;

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Exception\ClassPartException;
use Gaad\Chameleon\Interfaces\PhpClassPartInterface;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Gaad\Chameleon\Traits\HydrateObject;
use Gaad\Chameleon\Traits\InitAware;

abstract class DisabledDockBlockPhpClassPart extends PhpClassPart
{
	/**
	 * @return null
	 */
	public function getDocBlock(): ?PhpClassPartInterface
	{
		return null;
	}

	/**
	 * @param PhpClassPartInterface|null $docBlock
	 */
	public function setDocBlock(?PhpClassPartInterface $docBlock = null): void
	{
		$this->docBlock = null;
	}
}
