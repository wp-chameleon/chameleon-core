<?php

namespace Gaad\Chameleon\Schema\Php\Annotations;

use Gaad\Chameleon\Schema\Php\DisabledDockBlockPhpClassPart;
use Symfony\Component\Routing\Annotation\Route;


class RouteAnnotationClassPart extends DisabledDockBlockPhpClassPart
{
	protected string $tplName = 'RouteAnnotation';
	protected string $classTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/annotations';
	protected string $getNameFromCodeRegExp = '/.*/';
	protected array $validateFields = ['name', 'path'];
	private string $path = '/';
	private Route $route;

	function generateCode(): array
	{
		return [$this->generateTemplate($this->getTplName(), $this->getValidateFieldsValues())];
	}

	function onInitComplete(bool $initialized)
	{
		$this->route = new Route($this->getPath(), $this->getRouteDefaults());
	}

	/**
	 * @return string
	 */
	public function getPath(): string
	{
		return $this->path;
	}

	/**
	 * @param string $path
	 */
	public function setPath(string $path): void
	{
		$this->path = $path;
	}

	private function getRouteDefaults(): array
	{
		$r = [];
		array_map(function (string $item) use (&$r) {
			$r[$item] = $this->$item;
		}, $this->getValidateFields());

		return $r;
	}


}
