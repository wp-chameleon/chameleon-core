<?php

namespace Gaad\Chameleon\Schema\Php;

class ConstantClassPart extends PhpClassPart
{
	protected string $getNameFromCodeRegExp = '/const ([\w]+)/';


	public function getGetNameFromCodeRegExp(): string
	{
		return $this->getNameFromCodeRegExp;
	}

	function generateCode(): array
	{
		$code = [];

		return $code;
	}
}
