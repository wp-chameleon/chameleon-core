<?php

namespace Gaad\Chameleon\Schema\Php;

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Exception\ClassPartException;
use Gaad\Chameleon\Interfaces\PhpClassPartInterface;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Gaad\Chameleon\Traits\HydrateObject;
use Gaad\Chameleon\Traits\InitAware;

abstract class PhpClassPart implements PhpClassPartInterface
{
	use HydrateObject;
	use InitAware;

	protected string $tplName = 'Class';
	protected string $classTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/phpclass';
	protected string $getNameFromCodeRegExp;
	public string $type;
	public bool $newEntity = true;
	public PhpClassManager $classManager;
	public string $name;
	public int $begin;
	public ?int $end = null;
	public array $code = [];
	protected array $validateFields = ['name'];

	/**
	 * @return array|string[]
	 */
	public function getValidateFields(): array
	{
		return $this->validateFields;
	}

	public ?PhpClassPartInterface $docBlock = null;

	/**
	 * @throws ClassException
	 */
	public function __construct(PhpClassManager $classManager, ?array $data = null)
	{
		$this->classManager = $classManager;
		$this->setData($data);
		if (!empty($this->begin)) $this->setNewEntity(false);

		if (!$this->isNewEntity() && empty($this->begin))
			throw (new ClassPartException())->classPartBeginNull();

		$this->findEnd();
		$this->findDocBlock();
		$this->setInitialized(true);

	}

	function generateCode(): array
	{
		return [$this->generateTemplate($this->getTplName(), $this->getValidateFieldsValues())];
	}

	/**
	 * @throws ClassPartException
	 */
	function onInitComplete(bool $initialized)
	{
		if (!$this->isNewEntity())
			$this->setCode($this->findCode());
	}

	/**
	 * Validate fields before code generation.
	 * This a basic validator only that check if property exists and not empty.
	 * More specific cases should be handled via method override in target class.
	 * There is no support for null value here.
	 *
	 * @return bool
	 */
	function validate(): bool
	{
		$valid = true;
		array_map(function (string $fieldName) use (&$valid) {
			if ($valid && !(property_exists($this, $fieldName) && !empty($fieldName))) $valid = false;
		}, $this->getValidateFields());

		return $valid;
	}

	/**
	 * @throws ClassPartException
	 */
	function findCode(): array
	{
		if ($this->isNewEntity())
			return $this->generateCode();

		if (!$this->isInitialized() || !$this->codeSearchValid())
			throw (new ClassPartException)->searchForCodeBeforeInit();

		$cm = $this->getClassManager();
		$code = $cm->getClassSlice($this->getBegin(), $this->getEnd());
		$docBlockCode = $this->findDocBlockCode();

		return array_merge($docBlockCode, $code);
	}

	/**
	 * Returns child DocBlocks code
	 *
	 * @return array
	 */
	private function findDocBlockCode(): array
	{
		/** @var PhpClassPartInterface $docBlock */
		$docBlock = $this->getDocBlock();
		$code = [];

		if ($docBlock && $this->codeSearchValid())
			$code = $this->getClassManager()->getClassSlice($docBlock->getBegin(), $docBlock->getEnd());

		return $code;
	}

	/**
	 * Finds the corresponding to given origin line DocBlock and saves its contents to docBlock field
	 *
	 * @return void
	 * @throws ClassException
	 */
	private function findDocBlock()
	{
		if ($this->isNewEntity()) return;

		$cm = $this->getClassManager();
		$beginLine = $this->getBegin();
		$nearestDocBlockPosition = $cm->findNearestDocBlockPosition($beginLine, false);

		if (!empty($nearestDocBlockPosition) && $nearestDocBlockPosition['end'] === $beginLine - 1) {
			$docBlockClassPart = new DocBlockClassPart($this->getClassManager(), $nearestDocBlockPosition);
			$this->setDocBlock($docBlockClassPart);
		}

	}

	/**
	 * Find the given origin line end by finding the nearest semicolon after that line;
	 *
	 * @return int|null
	 */
	function findEnd(): ?int
	{
		if (!empty($this->getEnd()) || $this->isNewEntity()) return null;

		$cm = $this->getClassManager();
		$this->setEnd($cm->findNearestSemiColon($this->getBegin()));

		return $this->getEnd();
	}

	public function getType(): string
	{
		return $this->type;
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}

	function getName(): string
	{
		return !$this->isNewEntity() && empty($this->name) ? $this->getNameFromCode() : $this->name;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	public function getBegin(): int
	{
		return $this->begin;
	}

	public function setBegin(int $begin): void
	{
		$this->begin = $begin;
	}

	public function getEnd(): ?int
	{
		return $this->end;
	}

	public function setEnd(int $end): void
	{
		$this->end = $end;
	}

	public function getDocBlock(): ?PhpClassPartInterface
	{
		return $this->docBlock;
	}

	public function setDocBlock(?PhpClassPartInterface $docBlock = null): void
	{
		$this->docBlock = $docBlock;
	}

	public function getCode(): array
	{
		return $this->code;
	}

	public function setCode(array $code): void
	{
		$this->code = $code;
	}

	public function getClassManager(): PhpClassManager
	{
		return $this->classManager;
	}

	public function setClassManager(PhpClassManager $classManager): void
	{
		$this->classManager = $classManager;
	}

	/**
	 * Check minimal requirements for code fetching.
	 *
	 * @return bool
	 */
	private function codeSearchValid(): bool
	{
		return null !== $this->getBegin() && null !== $this->getEnd();
	}

	/**
	 * @return bool
	 */
	public function isNewEntity(): bool
	{
		return $this->newEntity;
	}

	/**
	 * @param bool $newEntity
	 */
	private function setNewEntity(bool $newEntity): void
	{
		$this->newEntity = $newEntity;
	}

	/**
	 * @return string
	 */
	public function getGetNameFromCodeRegExp(): string
	{
		return $this->getNameFromCodeRegExp;
	}

	/**
	 * @param string $getNameFromCodeRegExp
	 */
	public function setGetNameFromCodeRegExp(string $getNameFromCodeRegExp): void
	{
		$this->getNameFromCodeRegExp = $getNameFromCodeRegExp;
	}

	function getNameFromCode(): string
	{
		$matches = [];
		try {
			$test = preg_match($this->getGetNameFromCodeRegExp(), implode("\n", $this->getCode()), $matches);
		} catch (\Exception $e) {
		}

		return $test ? trim($matches[1]) : '';
	}

	/**
	 * @return string
	 */
	public function getClassTplDir(): string
	{
		return $this->classTplDir;
	}

	function generateTemplate(string $template, array $args = []): string
	{
		global $AppKernel;

		$mustache = $AppKernel->getService('mustache');
		$template = file_get_contents($this->getClassTplDir() . '/' . $template . '.php.tpl');

		return $mustache->render($template, $args);
	}

	/**
	 * @return string
	 */
	public function getTplName(): string
	{
		return $this->tplName;
	}

	/**
	 * @param string $tplName
	 */
	public function setTplName(string $tplName): void
	{
		$this->tplName = $tplName;
	}

	protected function getValidateFieldsValues(): array
	{
		$values = [];

		array_map(function ($item) use (&$values) {
			$getter = 'get' . ucfirst($item);
			$values[$item] = $this->$getter();
		}, $this->getValidateFields());

		return $values;
	}


}
