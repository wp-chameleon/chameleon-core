<?php

namespace Gaad\Chameleon\Schema\Php;


use ReflectionProperty;

class PropertyClassPart extends PhpClassPart
{
	protected string $getNameFromCodeRegExp = '/\$([\w]+)/';


	function generateCode(): array
	{
		$code = [];

		return $code;
	}

	public function getGetNameFromCodeRegExp(): string
	{
		return $this->getNameFromCodeRegExp;
	}

	private function getPropertyDefaultValue(ReflectionProperty $property): ?string
	{
		$value = '';
		$file = $this->getClassManager()->getClassFileArray();
		$i = 0;
		$value = explode('=', $file[$i]);
		$isOneLiner = strpos($value[1], ';');
		if ($isOneLiner)
			$value = trim(str_replace(';', '', $value[1] ?? '')) . ';';
		else {
			$max = count($file);
			for ($j = $i; $j < $max; $j++)
				if (strpos($file[$j], ';')) break;

			$propertyDefaultValue = array_slice($file, $i + 1, $j - $i);
			array_unshift($propertyDefaultValue, trim($value[1]));
			$value = implode("\n", $propertyDefaultValue);
		}

		return !empty($value) ? $value : null;
	}

}
