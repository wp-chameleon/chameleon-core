<?php

namespace Gaad\Chameleon\Schema\Php;


use Gaad\Chameleon\Exception\ClassPartException;

class MethodArgumentClassPart extends PhpClassPart
{
	protected string $getNameFromCodeRegExp = '/.*/';
	protected array $validateFields = ['name', 'argumentType', 'defaultValue', 'isNull', 'canBeNull', 'methodName'];

	public string $name = '';
	public ?string $methodName = null;

	public string $argumentType = '';
	public ?string $defaultValue = null;
	public bool $isNull = false;
	public bool $canBeNull = false;
	private array $stdTypes = ['string', 'array', 'int', 'float'];


	/**
	 * @return string
	 */
	public function getGetNameFromCodeRegExp(): string
	{
		return $this->getNameFromCodeRegExp;
	}

	function generateCode(): array
	{
		$methodCodeParts = [
			'name' => $this->getName(),
			'argumentType' => $this->getArgumentType(),
			'defaultValue' => $this->defaultValueResolver(),
			'canBeNull' => $this->isCanBeNull() ? '?' : '',
		];
		$code = $this->generateTemplate('MethodArgument', $methodCodeParts);
		$code = $this->normalizeCode($code);

		$multilineCode = explode("\n", $code);

		//@Todo take care of this monstrosity
		return 1 === count($multilineCode) ?
			[$code]
			: array_map(function (string $line) {
				return $line . "\n";
			}, $multilineCode);
	}


	/**
	 * @inheritDoc
	 *
	 * @return bool
	 */
	function validate(): bool
	{
		$validate = parent::validate();
		if (!$validate) return false;

		$validate = empty($this->getMethodName()) ? false : $validate;

		return $validate;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getArgumentType(): string
	{
		return $this->argumentType;
	}

	/**
	 * @param string $argumentType
	 * @throws ClassPartException
	 */
	public function setArgumentType(string $argumentType): void
	{
		if (!class_exists($argumentType) && !in_array($argumentType, $this->getStdTypes()))
			throw (new ClassPartException())->classMethodDoNotExists($argumentType);

		$this->argumentType = $this->normalizeElementType($argumentType);
	}

	/**
	 * @return string
	 */
	public function getDefaultValue(): ?string
	{
		return $this->defaultValue;
	}

	public function setDefaultValue(?string $defaultValue): void
	{
		$this->defaultValue = $defaultValue;
	}

	public function isNull(): bool
	{
		return $this->isNull;
	}

	public function setIsNull(bool $isNull): void
	{
		$this->isNull = $isNull;
	}


	/**
	 * @return string|null
	 */
	public function getMethodName(): ?string
	{
		return $this->methodName;
	}

	/**
	 * @param string|null $methodName
	 */
	public function setMethodName(?string $methodName): void
	{
		$this->methodName = $methodName;
	}

	/**
	 * @return bool
	 */
	public function isCanBeNull(): bool
	{
		return $this->canBeNull;
	}

	/**
	 * @param bool $canBeNull
	 */
	public function setCanBeNull(bool $canBeNull): void
	{
		$this->canBeNull = $canBeNull;
	}

	function normalizeCode(string $code): string
	{
		//remove last carriage return mark
		//$carriageReturn = strrpos($code, "\n");
		//	$code = empty($carriageReturn) ? $code : substr($code, 0, -(strlen($code) - $carriageReturn + 1));
		$code = trim($code);

		return $code;
	}

	public function defaultValueResolver(): ?string
	{
		$defaultValue = $this->getDefaultValue();
		$isNull = $this->isNull();
		$resolvedValue = null === $defaultValue && !$isNull ?
			''
			: '= ' . $defaultValue;


		//string $rrr = 'gangster'
		//int $rrr = 100
		//?int $rrr = null

		if ($isNull)
			$resolvedValue .= 'null';

		return $resolvedValue;
	}

	/**
	 * @return array|string[]
	 */
	public function getStdTypes(): array
	{
		return $this->stdTypes;
	}

	/**
	 * Strip the namespace of custom type elements and add it to class manager use section so there
	 * can be uUnqualified name used as argument type.
	 *
	 * @param string $argumentType
	 */
	private function normalizeElementType(string $argumentType): string
	{
		if (in_array($argumentType, $this->getStdTypes())) return $argumentType;

		$namespaceParts = explode('\\', $argumentType);
		$unqualifiedName = array_pop($namespaceParts);
		$qualifiedName = implode('\\\\', $namespaceParts);

		$this->getClassManager()->addUseSectionElement($qualifiedName . "\\" . $unqualifiedName);

		return $unqualifiedName;
	}


}
