<?php

namespace Gaad\Chameleon\Schema\Php;


class MethodClassPart extends PhpClassPart
{
	protected string $getNameFromCodeRegExp = '/function ([\w]+)/';
	protected array $validateFields = ['name', 'modifier', 'returnType', 'static', 'arguments'];
	protected string $tplName = 'Method';
	public string $modifier = 'public';
	public string $returnType = 'void';
	public string $static = '';
	public array $arguments = [];


	/**
	 * @return string
	 */
	public function getGetNameFromCodeRegExp(): string
	{
		return $this->getNameFromCodeRegExp;
	}


	function generateDocBlockCode(): string
	{
		$docBlock = $this->getDocBlock();

		return !empty($docBlock) ? implode('', $docBlock->generateCode()) : '';
	}

	function generateCode(): array
	{
		$methodCodeParts = $this->getValidateFieldsValues();

		$methodCodeParts ['docBlock'] = $this->generateDocBlockCode();

		$methodCodeParts['arguments'] = $this->getArgumentsCode();

		$code = $this->generateTemplate($this->getTplName(), $methodCodeParts);

		return array_map(function (string $line) {
			return $line . "\n";
		}, explode("\n", $code));
	}


	/**
	 * @inheritDoc
	 *
	 * @return bool
	 */
	function validate(): bool
	{
		$validate = parent::validate();
		if (!$validate) return false;


		return $validate;
	}

	/**
	 * @return string
	 */
	public function getModifier(): string
	{
		return $this->modifier;
	}

	/**
	 * @param string $modifier
	 */
	public function setModifier(string $modifier): void
	{
		$this->modifier = $modifier;
	}

	/**
	 * @return string
	 */
	public function getReturnType(): string
	{
		return $this->returnType;
	}

	/**
	 * @param string $returnType
	 */
	public function setReturnType(string $returnType): void
	{
		$this->returnType = $returnType;
	}

	/**
	 * @return string
	 */
	public function getStatic(): string
	{
		return $this->static;
	}

	/**
	 * @param string $static
	 */
	public function setStatic(string $static): void
	{
		$this->static = $static;
	}

	/**
	 * @return array
	 */
	public function getArguments(): array
	{
		return $this->arguments;
	}

	/**
	 * @param array $arguments
	 */
	public function setArguments(array $arguments): void
	{
		$this->arguments = $arguments;
	}

	function getArgumentsCode(): string
	{
		$code = [];
		array_map(function (MethodArgumentClassPart $argument) use (&$code) {
			$code[] = $argument->generateCode();
		}, $this->getArguments());

		$code = array_map(function (array $line) {
			return $line[0];
		}, $code);

		return implode(', ', $code);
	}
}
