<?php

namespace Gaad\Chameleon\Schema\Php;

use Gaad\Chameleon\Interfaces\PhpClassPartInterface;


class DocBlockClassPart extends PhpClassPart
{
	protected string $tplName = 'DocBlock';
	protected string $classTplDir = __CEBOARD_CORE_DIR__ . '/assets/tpl/annotations';
	protected array $validateFields = ['annotations'];
	protected string $getNameFromCodeRegExp = '/.*/';
	public array $annotations = [];

	protected function getValidateFieldsValues(): array
	{
		return ['annotations' => $this->getAnnotationsCode()];
	}

	/**
	 * Overridden to remove last carriage return sign
	 *
	 * @return array
	 */
	function generateCode(): array
	{
		return array_map(function (string $line) {
			return "\n" === substr($line, -1) ?
				substr($line, 0, strlen($line) - 1)
				: $line;
		}, parent::generateCode());
	}

	/**
	 * Iterates trough annotations, generate code of each and return combined
	 */
	function getAnnotationsCode(): array
	{
		$code = [];
		array_map(function (PhpClassPartInterface $argument) use (&$code) {
			$code[] = $argument->generateCode();
		}, $this->getAnnotations());

		return array_map(function (array $line) {
			return $line[0];
		}, $code);
	}

	/**
	 * @return null
	 */
	public function getDocBlock(): ?PhpClassPartInterface
	{
		return null;
	}

	/**
	 * @param PhpClassPartInterface|null $docBlock
	 */
	public function setDocBlock(?PhpClassPartInterface $docBlock = null): void
	{
		$this->docBlock = null;
	}

	/**
	 * @return string
	 */
	public function getGetNameFromCodeRegExp(): string
	{
		return $this->getNameFromCodeRegExp;
	}

	/**
	 * @return array
	 */
	public function getAnnotations(): array
	{
		return $this->annotations;
	}

	/**
	 * @param array $annotations
	 */
	public function setAnnotations(array $annotations): void
	{
		$this->annotations = $annotations;
	}


}
