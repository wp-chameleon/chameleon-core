<?php

namespace Gaad\Chameleon\Controller;

use Gaad\Chameleon\Exception\ClassException;
use Gaad\Chameleon\Service\Schema\PhpClassManager;
use Symfony\Component\Routing\Annotation\Route;

class ControllerReflectionManager
{
	private string $name;
	private array $routes = [];
	private string $controllerClassPath;
	private PhpClassManager $classManager;

	public function __construct(string $controllerClassPath, PhpClassManager $classManager)
	{
		$this->name = 'c_'.md5($controllerClassPath);
		$this->controllerClassPath = $controllerClassPath;
		$this->classManager = $classManager;

	}

	/**
	 * @throws ClassException
	 */
	public function fetchRoutes(): void
	{
		$classManager = $this->getClassManager();
		$classManager->loadFromFile($this->getControllerClassPath());
		$extensionName = explode('/', explode('/plugins/', $classManager->getRef()->getFileName())[1])[0];
		$routesDetails = [
			'controller' => $this->getControllerClassPath(),
			'extensionName' => $extensionName,
			'routes' => $classManager->getMethodsAnnotations(Route::class)
		];

		$this->setRoutes($routesDetails);
	}

	public function getName(): string
	{
		return $this->name;
	}


	public function setName(string $name): void
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getControllerClassPath(): string
	{
		return $this->controllerClassPath;
	}

	/**
	 * @param string $controllerClassPath
	 */
	public function setControllerClassPath(string $controllerClassPath): void
	{
		$this->controllerClassPath = $controllerClassPath;
	}

	/**
	 * @return PhpClassManager
	 */
	public function getClassManager(): PhpClassManager
	{
		return $this->classManager;
	}

	/**
	 * @param PhpClassManager $classManager
	 */
	public function setClassManager(PhpClassManager $classManager): void
	{
		$this->classManager = $classManager;
	}

	/**
	 * @return array
	 */
	public function getRoutes(): array
	{
		return $this->routes;
	}

	/**
	 * @param array $routes
	 */
	public function setRoutes(array $routes): void
	{
		$this->routes = $routes;
	}

}
