<?php

namespace Gaad\Chameleon\Controller;

abstract class AbstractController
{

	private string $filepath;

	public function __construct(string $filepath)
	{
		$this->filepath = $filepath;
	}
}
