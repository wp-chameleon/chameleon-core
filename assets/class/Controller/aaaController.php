<?php

namespace Gaad\Chameleon\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Gaad\Chameleon\Annotation\ChController;
use Gaad\Chameleon\Annotation\WPHookPriority;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @ChController()
 */
class aaaController
{
	/**
	 * @Route("some1", name="some_place")
	 * @WPHookPriority(100)
	 *
	 * @Template
	 * @return JsonResponse
	 */
	function someAction(Request $request): JsonResponse
	{

		return JsonResponse::fromJsonString('{ "data": 123 }');
	}

}
