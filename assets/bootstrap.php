<?php

use Gaad\Chameleon\AppKernel;
use Gaad\Chameleon\Command\ChameleonInitCommand;
use Gaad\Chameleon\Command\ClearCacheCommand;
use Gaad\Chameleon\Command\ControllersListCommand;
use Gaad\Chameleon\Command\ExtensionCreateCommand;
use Gaad\Chameleon\Command\ExtensionCreateRemoteRepositoryCommand;
use Gaad\Chameleon\Command\ExtensionListCommand;
use Gaad\Chameleon\Command\ExtensionRemoveCommand;
use Gaad\Chameleon\Command\ExtensionsRepositorySearchCommand;
use Gaad\Chameleon\Command\ExtensionServiceCreateCommand;
use Gaad\Chameleon\Command\ExtensionServiceRemoveCommand;
use Gaad\Chameleon\Command\ExtensionsRepositoryAddCommand;
use Gaad\Chameleon\Command\ExtensionsRepositoryListCommand;
use Gaad\Chameleon\Command\ExtensionSubscriberCreateCommand;
use Gaad\Chameleon\Command\ExtensionSubscriberListCommand;
use Gaad\Chameleon\Command\ExtensionSubscriberRemoveCommand;
use Gaad\Chameleon\Command\ModsListCommand;
use Gaad\Chameleon\Command\RoutesListCommand;
use Gaad\Chameleon\Command\ServiceAddDependencyCommand;
use Gaad\Chameleon\Command\ServiceListCommand;
use Gaad\Chameleon\Command\ServiceRemoveDependencyCommand;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Gaad\Chameleon\WP\Action\WPFooterWPAction;
use Symfony\Component\Console\Application;

//new WPFooterWPAction();

if ('cli' === php_sapi_name()) {
	require_once(__DIR__ . "/../vendor/autoload.php");

	'phpunit' !== basename($_ENV['_']) ?
		require_once(__DIR__ . "/../../../../wp-load.php")
		: require_once("/tmp/wordpress/wp-load.php");
}

if(function_exists('add_action')) {

	add_action('plugins_loaded', function () {
		if (null === $GLOBALS['AppKernel'] && class_exists('Gaad\Chameleon\AppKernel'))
			new AppKernel($_ENV['PROJECT_ENVIRONMENT'] ?? 'dev');
	}, -1);

	add_filter('Chameleon.Extension.Register', function (array $extensions) {
		$extensions[] = new ChameleonExtension(basename(dirname(__DIR__)));
		return $extensions;
	});

	add_filter('Chameleon.Console.Commands.Add', function (Application $application) {

		$application->add(new ServiceAddDependencyCommand());
		$application->add(new ServiceRemoveDependencyCommand());
		$application->add(new ClearCacheCommand());
		$application->add(new ExtensionsRepositoryListCommand());
		$application->add(new ServiceListCommand());
		$application->add(new ExtensionListCommand());
		$application->add(new ExtensionCreateCommand());
		$application->add(new ExtensionRemoveCommand());
		$application->add(new ExtensionsRepositorySearchCommand());
		$application->add(new ExtensionServiceCreateCommand());
		$application->add(new ExtensionsRepositoryAddCommand());
		$application->add(new ExtensionServiceRemoveCommand());
		$application->add(new ExtensionCreateRemoteRepositoryCommand());
		$application->add(new ChameleonInitCommand());
		$application->add(new ExtensionSubscriberListCommand());
		$application->add(new ExtensionSubscriberCreateCommand());
		$application->add(new ExtensionSubscriberRemoveCommand());
		$application->add(new ModsListCommand());
		$application->add(new RoutesListCommand());
		$application->add(new ControllersListCommand());

		return $application;
	});

	add_filter('Chameleon.Twig.Directories.Filter', function (array $directories){
		$directories[] = __DIR__ . '/../templates';
		return 	$directories;
	});

	add_filter('Chameleon.ORM.Directories.Filter', function (array $directories) {
		$directories[] = __DIR__ . '/class/Entity';

		return $directories;
	});

	add_filter('Chameleon.EventSubscribers.Directories.Filter', function (array $directories){
		$directories[] = __CEBOARD_CORE_DIR__ . '/config';
		return 	$directories;
	});

	add_filter('Chameleon.WPModifications.Directories.Filter', function (array $directories){
		$directories[] = __CEBOARD_CORE_DIR__ . '/config';
		return 	$directories;
	});
}
