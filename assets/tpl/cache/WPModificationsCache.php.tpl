{{%FILTERS}}
<?php

{{ useSection }}

if(! class_exists('{{ name }}ConfigCache')){

	class {{ name }}ConfigCache
	{

		public function __construct()
		{
			{{{constructorContent}}}
		}

	}

	new {{ name }}ConfigCache();
}
