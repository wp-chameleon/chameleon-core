{{%FILTERS}}
#!/bin/bash

git config --global user.email "you@example.com"
git config --global user.name "Your Name"
git init --initial-branch=main
git remote add origin {{ ssh_url_to_repo }}
git add .
git commit -m "Initial commit"
git push -u origin main
