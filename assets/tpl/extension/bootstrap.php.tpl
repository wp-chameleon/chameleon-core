{{%FILTERS}}
<?php
use Gaad\Chameleon\Config\{{name | case.ucfirst }}Configuration;
use Gaad\Chameleon\Extension\ChameleonExtension;
use Symfony\Component\Config\Definition\ConfigurationInterface;

add_filter('Chameleon.Extension.Register', function (array $extensions){
	$extensions[] = new ChameleonExtension(basename(dirname(__DIR__)));
	return $extensions;
});

add_filter('Chameleon.Config.Configuration.Filter', function (ConfigurationInterface $configuration){
	return (new {{name | case.ucfirst }}Configuration())->appendTo($configuration);
});

add_filter('Chameleon.Config.Directories.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../config';
	return 	$directories;
});

add_filter('Chameleon.EventSubscribers.Directories.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../config';
	return 	$directories;
});

add_filter('Chameleon.WPModifications.Directories.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../config';
	return 	$directories;
});

add_filter('Chameleon.MVCRoutes.Directories.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../config';
	return 	$directories;
});

add_filter('Chameleon.MVCControllers.Directories.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../config';
	return 	$directories;
});

add_filter('Chameleon.Twig.Directories.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../templates';
	return 	$directories;
});

add_filter('Chameleon.Services.File.Filter', function (array $directories){
	$directories[] = __DIR__ . '/../config';
	return 	$directories;
});
