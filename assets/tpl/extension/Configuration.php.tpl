{{%FILTERS}}
<?php
namespace Gaad\Chameleon\Config;

use Symfony\Component\Config\Definition\ConfigurationInterface;

class {{name | case.ucfirst }}Configuration extends ExtensionConfigurationBase
{

	protected string $rootNodeName = '{{name | case.lower }}';

	public function appendTo(ConfigurationInterface $configuration)
	{
		$rootNode = $configuration->treeBuilder->getRootNode()->
		children()
			->arrayNode('{{name | case.lower }}');

		$this->applyExtensionManifestSupport($rootNode);

		return $configuration;
	}

	public function getConfigTreeBuilder()
	{
	}

	public function getRootNodeName(): string
	{
		return $this->rootNodeName;
	}

}
