{{%FILTERS}}
{
	"name": "gaad/chameleon-extension-{{name | case.lower }}",
	"description": "Chameleon Woocommerce theme {{name | case.lower }}",
	"autoload": {
		"psr-4": {
			"Gaad\\Chameleon\\": "assets/class"
		}
	}
}
