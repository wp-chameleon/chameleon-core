{{%FILTERS}}
<?php
/**
 * Plugin Name:     {{ name | case.ucfirst }} Chameleon Extension
 * Plugin URI:
 * Description:
 * Author:
 * Author URI:
 * Text Domain:     {{name | case.lower }}-chameleon-extension
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         {{name | case.ucfirst }}_Chameleon_Extension
 */

if (file_exists(__DIR__ . "/vendor/autoload.php")) require_once __DIR__ . "/vendor/autoload.php";
include_once __DIR__ . "/assets/bootstrap.php";
class {{ name }} {}
