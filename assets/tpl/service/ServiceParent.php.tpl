{{%FILTERS}}
<?php

namespace {{ serviceNamespace }}\Parent;

use Gaad\Chameleon\Service\Extensions\Parent\AbstractParent;
{{ useSection }}

class {{name | case.ucfirst }}Parent extends {{parentClassBaseName}}
{
	const description = '{{ description }}';

	{{classFields}}

	public function __construct({{ constructorArgsSection }})
	{
		{{{classFieldsInit}}}

		parent::__construct();
	}

	{{{classFieldsGetters}}}

	{{{classFieldsSetters}}}
}
