{{%FILTERS}}
<?php
namespace {{ serviceNamespace }};

use {{ serviceNamespace }}\Parent\{{name | case.ucfirst }}Parent;

{{ useSection }}

class {{name | case.ucfirst }} extends {{name | case.ucfirst }}Parent
{
	public function serviceMethod()
	{

	}
}
