{{%FILTERS}}

	public function get{{ fieldName | case.ucfirst }}(): {{ returnType }}
	{
		return $this->{{ fieldName | case.lcfirst }};
	}
