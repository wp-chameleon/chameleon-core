{{%FILTERS}}
<?php
namespace {{ namespace | sanitize.namespace }};

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
{{ useSection }}

class {{name | case.ucfirst }} implements EventSubscriberInterface
{

	public static function getSubscribedEvents()
	{
		return [
			'acme.foo.action' => 'fooAction',
		];
	}

	public function fooAction(Event $event)
	{
	}

}
