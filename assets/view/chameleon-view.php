<?php
global $AppKernel;

use Gaad\Chameleon\Service\View\ContentRenderer;

/** @var ContentRenderer $ContentRenderer */
$ContentRenderer = $AppKernel->getService('ContentRenderer');

$ContentRenderer->renderView(get_query_var('ch_controller'), get_query_var('ch_action'));

