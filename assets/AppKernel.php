<?php

namespace Gaad\Chameleon;


use DIContainer;
use Gaad\Chameleon\DependencyInjection\DependencyInjectionManager;
use Gaad\Chameleon\Service\Config\ConfigManager;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Filesystem\Filesystem;

class AppKernel
{
	private DependencyInjectionManager $dependencyInjection;
	private $env = 'dev';
	private ConfigManager $config;

	public function __construct($env)
	{
		$this->env = $env;
		$this->dependencyInjection = new DependencyInjectionManager($this);

		$GLOBALS['AppKernel'] = $this;
	}

	function getService(string $name)
	{
		return $this->dependencyInjection->getContainer()->get($name);
	}

	/**
	 * @return DependencyInjectionManager
	 */
	public function getDependencyInjection(): DependencyInjectionManager
	{
		return $this->dependencyInjection;
	}

	/**
	 * @return string
	 */
	public function getEnv(): string
	{
		return $this->env;
	}

	/**
	 * @param string $env
	 */
	public function setEnv(string $env): void
	{
		$this->env = $env;
	}

	public function serviceExists(string $serviceName): bool
	{
		return in_array($serviceName, $this->dependencyInjection->getContainer()->getServiceIds());
	}

	public function getServiceIdsList(): array
	{
		return $this->dependencyInjection->getContainer()->getServiceIds();
	}

	public function getExtensionList()
	{
		return apply_filters('Chameleon.Extension.Register', []);
	}

	/**
	 * Remove while environment cache directory
	 *
	 * @return void
	 * @throws Exception\ConfigException
	 */
	public function clearCache()
	{
		/**
		 * @var ConfigManager $config
		 * @var Filesystem $filesystem
		 */
		$config = $this->getService('config');
		$filesystem = $this->getService('filesystem');
		$cacheDir = $config->has('resources/cache/dir') ? $config->getResource('resources/cache/dir') : __CEBOARD_CORE_DIR__ . '/cache';
		$cacheDir .= '/' . $this->getEnv();

		try {
			$filesystem->remove($cacheDir);
		} catch (\Exception $e) {}
	}

	public function getServicesDefinitions(): array
	{
		$definitions = [];
		try {
			$definitions = (new DependencyInjectionManager($this, true))->getContainer()->getDefinitions();
		} catch (\Exception $e) {}

		return $definitions;
	}

	public function getServiceDefinition(string $serviceName): ?definition
	{
		$servicesDefinitions = $this->getServicesDefinitions();
		return empty($servicesDefinitions[$serviceName]) ? null : $servicesDefinitions[$serviceName];
	}

	public function getServiceDefinitionByType(string $serviceType): array
	{
		return array_filter($this->getServicesDefinitions(), function (Definition $definition) use ($serviceType) {
			return $serviceType === $definition->getClass();
		});
	}

}
