<?php
/**
 * Plugin Name:     CE Board Wp Plugin
 * Plugin URI:
 * Description:
 * Author:
 * Author URI:
 * Text Domain:
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         CEBoard
 */

use Dotenv\Dotenv;

define('__CEBOARD_CORE_DIR__', __DIR__);
if (!defined('__CEBOARD_CACHE_BASE_DIR__')) define('__CEBOARD_CACHE_BASE_DIR__', __DIR__); //this constant can be overridden in wp-config.php
include __CEBOARD_CORE_DIR__ .'/vendor/autoload.php';
(Dotenv::createImmutable(__CEBOARD_CORE_DIR__ . '/../../../'))->load();
