'use strict';

var gulp = require( 'gulp' );
var sass = require( 'gulp-sass' );
var gutil = require('gulp-util');
var poToMo = require('gulp-potomo-js');

sass.compiler = require( 'node-sass' );

var _ENV_TYPE = typeof gutil.env.env !== "undefined" ? gutil.env.env : 'dev';

var paths = {
    sass: {
		dev: {
			langSrc: '/var/www/html/wp-content/themes/chameleon/languages/cukiernie/*.po',
			langDestSrc: '/var/www/html/wp-content/themes/chameleon/languages/cukiernie/',

			src: '/var/www/html/wp-content/themes/chameleon/assets/scss/**/*.scss',
			dest: '/var/www/html/wp-content/themes/chameleon/'
		},
		stage: {
			langSrc: '/var/www/html/wp-content/themes/chameleon/languages/cukiernie/*.po',
			langDestSrc: '/var/www/html/wp-content/themes/chameleon/languages/cukiernie/',

			src: '/var/www/stage/current/wp-content/themes/chameleon/assets/scss/**/*.scss',
			dest: '/var/www/stage/current/wp-content/themes/chameleon/'
		},
		production:{
			langSrc: '/var/www/html/wp-content/themes/chameleon/languages/cukiernie/*.po',
			langDestSrc: '/var/www/html/wp-content/themes/chameleon/languages/cukiernie/',

			src: '/var/www/prod/current/wp-content/themes/chameleon/assets/scss/**/*.scss',
			dest: '/var/www/prod/current/wp-content/themes/chameleon/'
		}
	}
};




gulp.task( 'sass', function () {
    var ret =  gulp.src( paths.sass[ _ENV_TYPE ].src )
        .pipe( sass().on( 'error', sass.logError ) )
        .pipe( gulp.dest( paths.sass[ _ENV_TYPE ].dest ) );

    console.log(sass.logError );
    return ret;

} );

gulp.task('potomo', function() {
	return 	gulp.src(paths.sass[_ENV_TYPE].langSrc)
			.pipe(poToMo())
			.pipe(gulp.dest(paths.sass[_ENV_TYPE].langDestSrc))
	}
);

gulp.task( 'sass:watch', function () {
    gulp.watch( paths.sass[ _ENV_TYPE ].src, gulp.series( 'sass' ) );
    gulp.watch( paths.sass[ _ENV_TYPE ].langSrc, gulp.series( 'potomo' ) );
} );
