<?php
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Dotenv\Dotenv;

$autoloadFiles = array(
    __DIR__ . '/../vendor/autoload.php',
    __DIR__ . '/../../../autoload.php'
);

$autoloader = false;
foreach ($autoloadFiles as $autoloadFile) {
    if (file_exists($autoloadFile)) {
        require_once $autoloadFile;
        $autoloader = true;
    }
}

if (!$autoloader) die('vendor/autoload.php could not be found. Did you run `php composer.phar install`?');

(Dotenv::createImmutable(__DIR__ . '/../../../../'))->load();

require_once(__DIR__ . "/../../../../wp-load.php");

$PROJECT_ENVIRONMENT = !empty($_ENV['PROJECT_ENVIRONMENT']) ? $_ENV['PROJECT_ENVIRONMENT'] : 'dev';
$AppKernel = new \Gaad\Chameleon\AppKernel($PROJECT_ENVIRONMENT);
$config = $AppKernel->getService('config');

$directories = apply_filters('Chameleon.ORM.Directories.Filter', []);
$isDevMode = 'prod' !== (string)$PROJECT_ENVIRONMENT;
$ormConfig = $config->get('doctrine.orm');

$EntityManager = EntityManager::create([
    'driver' => 'pdo_mysql',
    'host' => $_ENV['MYSQL_CONTAINER'],
    'user' => $_ENV['MYSQL_USER'],
    'password' => $_ENV['MYSQL_ROOT_PASSWORD'],
    'dbname' => $_ENV['MYSQL_DATABASE'],
], Setup::createAnnotationMetadataConfiguration($directories, $isDevMode, $ormConfig['proxy_dir'], null, false));

$EntityManager
    ->getConnection()
    ->getDatabasePlatform()
    ->registerDoctrineTypeMapping('enum', 'string');


return ConsoleRunner::createHelperSet($EntityManager);
