#!/usr/bin/env bash

rm -fr /tmp/wordpress/wp-content/themes/*
rm -fr /tmp/wordpress/wp-content/plugins/*
cp -r /var/www/html/wp-content/themes /tmp/wordpress/wp-content/

#mv /tmp/wordpress/wp-content/themes/child /tmp/wordpress/wp-content/themes/default
